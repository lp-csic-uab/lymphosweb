/*
Created on 25/07/2012

@authors: LP CSIC/UAB (lp.csic@uab.cat)
*/

var info_id0 = {};
var gelimg_dct = {};
var gelzoom_dct = {};

function pick_spot(spot_id, twodgel_id, n_imgs) {
    var spot_img, pickr_img;
    var info_curr = 'info.' + spot_id;
    var info_prev;
    
    for (var img_id = 1; img_id <= n_imgs; img_id++){
        spot_img = document.getElementById(spot_id + '.' + img_id);
        pickr_img = document.getElementById('sel.' + twodgel_id + '.' + img_id);
        pickr_img.style.top = spot_img.style.top;
        pickr_img.style.left = spot_img.style.left;
        pickr_img.style.zIndex = 1001;
    }
    
    if (twodgel_id in info_id0) {
        info_prev = 'info.' + info_id0[twodgel_id];
    } else {
        info_prev = 'info.0.' + twodgel_id;
    }
    document.getElementById(info_curr).style.display = 'inline-block';
    document.getElementById(info_prev).style.display = 'none';
    
    info_id0[twodgel_id] = spot_id;
    
    return true;
}

function gelimg_change(gelimg_id, twodgel_id, img_num) {
    var gelimg_img = document.getElementById('img.' + twodgel_id + '.' + img_num);
    var gelimg_file = gelimg_dct[twodgel_id + '.' + gelimg_id];
    
    gelimg_img.src = '/gelimages/' + gelimg_file + '?zoom=' + gelzoom_dct[twodgel_id];
    
    return true;
}

function show_spots(show_type, twodgel_id, img_num) {
    var spotset_div = document.getElementById('spots.' + twodgel_id + '.' + img_num);
    
    if (show_type == true) {
        spotset_div.style.display = 'block';
    } else {
    	spotset_div.style.display = 'none';
    }
    
    return true;
}

function scroll_sync(viewport, twodgel_id, n_imgs) {
	var other_viewport;
	
    for (var img_id = 1; img_id <= n_imgs; img_id++){
    	other_viewport = document.getElementById('gel.' + twodgel_id + '.' + img_id);
        other_viewport.scrollTop = viewport.scrollTop;
        other_viewport.scrollLeft = viewport.scrollLeft;
    }
    
    return true;
}

function gelzoom_change(selzoom, twodgel_id, n_imgs) {
	var old_zoom, 
		new_zoom,
		over_zoom,
		scale,
		viewport,
		overflow_type,
		selimg,
		spots,
		spot_num,
		spot;
	
	old_zoom = gelzoom_dct[twodgel_id];
	new_zoom = parseFloat(selzoom.value); //Get new zoom level
	over_zoom = parseFloat(selzoom.options.item(0).value); //Get default zoom level
	scale = new_zoom / old_zoom;
	gelzoom_dct[twodgel_id] = new_zoom; //Update the gelzoom_dct dictionary for gel with id = twodgel_id
	if (new_zoom > over_zoom) {
		overflow_type = 'scroll'; //Show scroll-bars
	} else {
		overflow_type = 'hidden'; //Don't show scroll-bars
	}
	
    for (var img_id = 1; img_id <= n_imgs; img_id++){
    	spots = document.getElementById('spots.' + twodgel_id + '.' + img_id).children;
    	spot_num = spots.length;
    	for (var spot_index = 0; spot_index < spot_num; spot_index++){
    		spot = spots.item(spot_index);
    		spot.style.left = ((spot.offsetLeft + 5) * scale - 5).toString() + "px";
    		spot.style.top = ((spot.offsetTop + 5) * scale - 5).toString() + "px";
    	}
    	selimg = document.getElementById('selimg.' + twodgel_id + '.' + img_id); //Get the image selector object
    	gelimg_change(selimg.value, twodgel_id, img_id) //Change the image size (we send the current displayed image id, so image will not change)
    	viewport = document.getElementById('gel.' + twodgel_id + '.' + img_id); //Get the div object containing the image (the viewport)
    	viewport.style.overflow = overflow_type; //Set the overflow style to 'scroll' or 'hidden'
    }
    
    return true;
}

function focus_selspot(twodgel_id, img_num) {
	var selspot,
		viewport,
		view_posx,
		view_posy;
	
	selspot = document.getElementById('sel.' + twodgel_id + '.' + img_num);
	viewport = document.getElementById('gel.' + twodgel_id + '.' + img_num); //Get the div object containing the image (the viewport)
	if (viewport.style.overflow == 'scroll' && selspot.style.zIndex == 1001){
		view_posx = selspot.offsetLeft - viewport.offsetWidth / 2;
		view_posy = selspot.offsetTop - viewport.offsetHeight / 2;
	} else {
		view_posx = 0;
		view_posy = 0;
	}
	viewport.scrollTop = view_posy;
	viewport.scrollLeft = view_posx;
}