SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

SET default_storage_engine=INNODB;

DROP SCHEMA IF EXISTS `lymphos_lymphosJM2allpepd` ;
CREATE SCHEMA IF NOT EXISTS `lymphos_lymphosJM2allpepd` DEFAULT CHARACTER SET utf8 ;
USE `lymphos_lymphosJM2allpepd` ;

BEGIN;
CREATE TABLE `lymphos_peptidecache` (
    `phospho` bool NOT NULL,
    `n_prots` smallint UNSIGNED NOT NULL,
    `dbpsites` varchar(20) NOT NULL,
    `dbascores` varchar(85) NOT NULL,
    `peptide` varchar(80) BINARY NOT NULL PRIMARY KEY,
    `dbproteins` longtext NOT NULL,
    `quantitative` bool NOT NULL,
    `n_scans` smallint UNSIGNED NOT NULL,
    `gel` bool NOT NULL
)
;
CREATE TABLE `lymphos_expgroup` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `name` longtext NOT NULL,
    `description` longtext NOT NULL
)
;
CREATE TABLE `lymphos_miapes` (
    `id` varchar(3) NOT NULL PRIMARY KEY,
    `xml` longtext NOT NULL
)
;
CREATE TABLE `lymphos_experiments` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `expgroup_id` integer NOT NULL,
    `dbsub_exps` varchar(50) NOT NULL,
    `name` varchar(100) NOT NULL,
    `miape_id` varchar(50) NOT NULL,
    `description` varchar(180) NOT NULL,
    `instrument` varchar(150) NOT NULL,
    `label` varchar(10) NOT NULL,
    `quant_cond` longtext NOT NULL
)
;
ALTER TABLE `lymphos_experiments` ADD CONSTRAINT `expgroup_id_refs_id_8c68f678` FOREIGN KEY (`expgroup_id`) REFERENCES `lymphos_expgroup` (`id`);
CREATE TABLE `lymphos_quantpepexpcond` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `unique_pep` varchar(80) BINARY NOT NULL,
    `expgroup_id` integer NOT NULL,
    `dballquanttimes` longtext NOT NULL,
    `dbavgratios` longtext NOT NULL,
    `dbstdv` longtext NOT NULL,
    `dbavgregulation` varchar(200) NOT NULL,
    `dbttestpassed95` varchar(100) NOT NULL,
    `dbttestpassed99` varchar(100) NOT NULL,
    `dbsdgreatersdmeanx3` varchar(100) NOT NULL,
    `dbdanteravgratios` longtext NOT NULL,
    `dbdanterpvalues` longtext NOT NULL,
    `dbdanteradjpvalues` longtext NOT NULL
)
;
ALTER TABLE `lymphos_quantpepexpcond` ADD CONSTRAINT `expgroup_id_refs_id_cd0d276f` FOREIGN KEY (`expgroup_id`) REFERENCES `lymphos_expgroup` (`id`);
CREATE TABLE `lymphos_nophospho_stats` (
    `quanttime` smallint NOT NULL PRIMARY KEY,
    `mu` double precision NOT NULL,
    `sigma` double precision NOT NULL,
    `n_spectra` integer NOT NULL
)
;
CREATE TABLE `lymphos_proteins` (
    `ac` varchar(25) NOT NULL PRIMARY KEY,
    `phospho` bool NOT NULL,
    `is_decoy` bool NOT NULL,
    `up_id` varchar(25) NOT NULL,
    `up_ver` integer UNSIGNED NOT NULL,
    `name` varchar(300) NOT NULL,
    `chr` varchar(5) NOT NULL,
    `function` longtext NOT NULL,
    `location` varchar(300) NOT NULL,
    `seq` longtext NOT NULL,
    `up_psites` longtext NOT NULL,
    `score_lcms` double precision NOT NULL
)
;
CREATE TABLE `lymphos_gelimage` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `twodgel_id` integer NOT NULL,
    `name` varchar(100) NOT NULL,
    `hasjpg` bool NOT NULL,
    `dye` varchar(10) NOT NULL,
    `condition` varchar(50) NOT NULL,
    `slope_x` double precision NOT NULL,
    `intercept_x` double precision NOT NULL,
    `slope_y` double precision NOT NULL,
    `intercept_y` double precision NOT NULL
)
;
CREATE TABLE `lymphos_twodgel` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `experiment_id` integer NOT NULL UNIQUE,
    `dbconditions` longtext NOT NULL,
    `name` varchar(15) NOT NULL
)
;
ALTER TABLE `lymphos_twodgel` ADD CONSTRAINT `experiment_id_refs_id_8976c957` FOREIGN KEY (`experiment_id`) REFERENCES `lymphos_experiments` (`id`);
ALTER TABLE `lymphos_gelimage` ADD CONSTRAINT `twodgel_id_refs_id_99099428` FOREIGN KEY (`twodgel_id`) REFERENCES `lymphos_twodgel` (`id`);
CREATE TABLE `lymphos_spot` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `twodgel_id` integer NOT NULL,
    `x` integer NOT NULL,
    `y` integer NOT NULL,
    `area` integer NOT NULL,
    `pi` double precision NOT NULL,
    `mw` double precision NOT NULL,
    `ratio_ab` double precision NOT NULL,
    `anova` double precision NOT NULL,
    `fold` double precision NOT NULL,
    `max_cv` double precision NOT NULL,
    `ingel_id` integer NOT NULL
)
;
ALTER TABLE `lymphos_spot` ADD CONSTRAINT `twodgel_id_refs_id_5b1357e3` FOREIGN KEY (`twodgel_id`) REFERENCES `lymphos_twodgel` (`id`);
CREATE TABLE `lymphos_spotextra` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `spot_id` integer NOT NULL,
    `gelimage_id` integer NOT NULL,
    `volume` double precision NOT NULL,
    `norm_volume` double precision NOT NULL,
    `background` double precision NOT NULL,
    `peak_height` double precision NOT NULL
)
;
ALTER TABLE `lymphos_spotextra` ADD CONSTRAINT `gelimage_id_refs_id_90425a8e` FOREIGN KEY (`gelimage_id`) REFERENCES `lymphos_gelimage` (`id`);
ALTER TABLE `lymphos_spotextra` ADD CONSTRAINT `spot_id_refs_id_286d2278` FOREIGN KEY (`spot_id`) REFERENCES `lymphos_spot` (`id`);
CREATE TABLE `lymphos_spots2proteins` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `spot_id` integer NOT NULL,
    `protein_id` varchar(25) NOT NULL,
    `score_gelpmf` double precision NOT NULL
)
;
ALTER TABLE `lymphos_spots2proteins` ADD CONSTRAINT `protein_id_refs_ac_b42211ae` FOREIGN KEY (`protein_id`) REFERENCES `lymphos_proteins` (`ac`);
ALTER TABLE `lymphos_spots2proteins` ADD CONSTRAINT `spot_id_refs_id_1952d291` FOREIGN KEY (`spot_id`) REFERENCES `lymphos_spot` (`id`);
CREATE TABLE `lymphos_data_proteins` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `data_id` integer NOT NULL,
    `proteins_id` varchar(25) NOT NULL,
    UNIQUE (`data_id`, `proteins_id`)
)
;
ALTER TABLE `lymphos_data_proteins` ADD CONSTRAINT `proteins_id_refs_ac_2a803504` FOREIGN KEY (`proteins_id`) REFERENCES `lymphos_proteins` (`ac`);
CREATE TABLE `lymphos_data` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `phospho` bool NOT NULL,
    `n_prots` smallint UNSIGNED NOT NULL,
    `dbpsites` varchar(20) NOT NULL,
    `dbascores` varchar(85) NOT NULL,
    `peptide` varchar(80) BINARY NOT NULL,
    `experiment_id` integer NOT NULL,
    `out_file` varchar(100) NOT NULL,
    `raw_file` varchar(200) NOT NULL,
    `scan_i` integer UNSIGNED NOT NULL,
    `scan_f` integer UNSIGNED NOT NULL,
    `ms_n` smallint UNSIGNED NOT NULL,
    `rt` integer UNSIGNED NOT NULL,
    `mass` double precision NOT NULL,
    `deltamass` double precision NOT NULL,
    `ions` varchar(10) NOT NULL,
    `charge` smallint NOT NULL,
    `pep_orig` varchar(200) NOT NULL,
    `pep_score` double precision NOT NULL,
    `xcorr` double precision NOT NULL,
    `deltacn` double precision NOT NULL,
    `d` double precision NOT NULL,
    `omssa` double precision NOT NULL,
    `phenyx` double precision NOT NULL,
    `peaks` double precision NOT NULL,
    `easyprot` double precision NOT NULL,
    `qdata` longtext NOT NULL,
    `spot_id` integer NOT NULL
)
;
ALTER TABLE `lymphos_data` ADD CONSTRAINT `experiment_id_refs_id_dc8b5b63` FOREIGN KEY (`experiment_id`) REFERENCES `lymphos_experiments` (`id`);
ALTER TABLE `lymphos_data` ADD CONSTRAINT `spot_id_refs_id_0cf28bb8` FOREIGN KEY (`spot_id`) REFERENCES `lymphos_spot` (`id`);
ALTER TABLE `lymphos_data_proteins` ADD CONSTRAINT `data_id_refs_id_71fd3e1b` FOREIGN KEY (`data_id`) REFERENCES `lymphos_data` (`id`);
CREATE TABLE `lymphos_dataquant` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `dbquanttimes` varchar(100) NOT NULL,
    `dbratios` varchar(100) NOT NULL,
    `dbstdv` varchar(100) NOT NULL,
    `dbregulation` varchar(20) NOT NULL,
    `data_id` integer NOT NULL UNIQUE,
    `quantpepexpcond_id` integer NOT NULL
)
;
ALTER TABLE `lymphos_dataquant` ADD CONSTRAINT `quantpepexpcond_id_refs_id_afd1aeb5` FOREIGN KEY (`quantpepexpcond_id`) REFERENCES `lymphos_quantpepexpcond` (`id`);
ALTER TABLE `lymphos_dataquant` ADD CONSTRAINT `data_id_refs_id_240d86f1` FOREIGN KEY (`data_id`) REFERENCES `lymphos_data` (`id`);
CREATE TABLE `lymphos_spectra` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `data_id` integer NOT NULL,
    `ms_n` smallint UNSIGNED NOT NULL,
    `mass` longtext NOT NULL,
    `intensity` longtext NOT NULL
)
;
ALTER TABLE `lymphos_spectra` ADD CONSTRAINT `data_id_refs_id_035612a9` FOREIGN KEY (`data_id`) REFERENCES `lymphos_data` (`id`);
CREATE TABLE `lymphos_users` (
    `mail` varchar(240) NOT NULL PRIMARY KEY,
    `passwd` varchar(24) NOT NULL,
    `permissions` smallint UNSIGNED NOT NULL,
    `singup_date` datetime NOT NULL,
    `last_logging` datetime NOT NULL
)
;
CREATE TABLE `lymphos_eventtype` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `name` varchar(15) NOT NULL
)
;
CREATE TABLE `lymphos_events_subevents` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `from_events_id` integer NOT NULL,
    `to_events_id` integer NOT NULL,
    UNIQUE (`from_events_id`, `to_events_id`)
)
;
CREATE TABLE `lymphos_events` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `eventtype_id` integer NOT NULL,
    `date` datetime NOT NULL,
    `value` longtext NOT NULL
)
;
ALTER TABLE `lymphos_events` ADD CONSTRAINT `eventtype_id_refs_id_0b5982ea` FOREIGN KEY (`eventtype_id`) REFERENCES `lymphos_eventtype` (`id`);
ALTER TABLE `lymphos_events_subevents` ADD CONSTRAINT `from_events_id_refs_id_55dfd6bd` FOREIGN KEY (`from_events_id`) REFERENCES `lymphos_events` (`id`);
ALTER TABLE `lymphos_events_subevents` ADD CONSTRAINT `to_events_id_refs_id_55dfd6bd` FOREIGN KEY (`to_events_id`) REFERENCES `lymphos_events` (`id`);
CREATE TABLE `lymphos_version` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `db_version` VARCHAR(50) NOT NULL,
  `version_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`));
CREATE INDEX `lymphos_experiments_c63dd22a` ON `lymphos_experiments` (`expgroup_id`);
CREATE INDEX `lymphos_quantpepexpcond_c63dd22a` ON `lymphos_quantpepexpcond` (`expgroup_id`);
CREATE INDEX `lymphos_gelimage_179d20fb` ON `lymphos_gelimage` (`twodgel_id`);
CREATE INDEX `lymphos_spot_179d20fb` ON `lymphos_spot` (`twodgel_id`);
CREATE INDEX `lymphos_spotextra_0bd42832` ON `lymphos_spotextra` (`spot_id`);
CREATE INDEX `lymphos_spotextra_d7b309fa` ON `lymphos_spotextra` (`gelimage_id`);
CREATE INDEX `lymphos_spots2proteins_0bd42832` ON `lymphos_spots2proteins` (`spot_id`);
CREATE INDEX `lymphos_spots2proteins_e3372c3e` ON `lymphos_spots2proteins` (`protein_id`);
CREATE INDEX `lymphos_data_3e8130cb` ON `lymphos_data` (`experiment_id`);
CREATE INDEX `lymphos_data_0bd42832` ON `lymphos_data` (`spot_id`);
CREATE INDEX `lymphos_dataquant_e30ffe10` ON `lymphos_dataquant` (`quantpepexpcond_id`);
CREATE INDEX `lymphos_spectra_3d40d808` ON `lymphos_spectra` (`data_id`);
CREATE INDEX `lymphos_events_7a4f8c6c` ON `lymphos_events` (`eventtype_id`);

COMMIT;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
