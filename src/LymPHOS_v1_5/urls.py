# -*- coding: utf-8 -*-
"""
:synopsis:   Django URL settings file for LymPHOS_v1_5 project

:created:    2011-01-14

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2011-2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '2.0'
__UPDATED__ = '2015-10-21'


#===============================================================================
# Imports
#===============================================================================
# Import basic Django urls and configuration handlers:
#from django.conf.urls.defaults import * #X-NOTE: not compatible with Django 1.6
from django.conf.urls import *
# Imports the predefined view so the Django wsgi-webserver could serve
# static local files:
import django.views.static

import settings

# Import the LymPHOS own views:
import views

# Uncomment the next two lines to enable the Django admin:
#from django.contrib import admin
#admin.autodiscover()


#===============================================================================
# URL patterns variable
#===============================================================================
urlpatterns = patterns('')

if not settings.DEPLOY:                         #We are in Development Mode
    # ``urlpatterns`` to serve the static files with Django wsgi-webserver:
    urlpatterns += patterns('',
                        (r'^files/(?P<path>.*)$', django.views.static.serve,
                         {'document_root': settings.STATIC_DOC_ROOT}),
    )

urlpatterns += patterns('',
                        # Home (/):
                        (r'^$',
                         views.FormsHtmlPage.as_view(template_name='home_section.html'),
                         {'extra_context': {'section_title': 'Home Page',
                                            'sidebar': {'template': 'side_bar.html'},
                                            'class_home': r'current'}}
                         ),
                        # Main Standard sections:
                        (r'^experimental/$',
                         views.BasicHtmlPage.as_view(template_name='experimental_section.html'),
                         {'extra_context': {'section_title': 'Experimental Procedures',
                                            'css_sheet': 'default_nosidebar.css',
                                            'class_experimental': r'current'}}
                         ),
                        (r'^search/$',
                         views.FormsHtmlPage.as_view(template_name='search_section.html'),
                         {'extra_context': {'section_title': 'Search LymPHOS DataBase',
                                            'css_sheet': 'default_nosidebar.css',
                                            'class_search': r'current'}}
                         ),
                        (r'^contribute/$',
                         views.FormsHtmlPage.as_view(template_name='contribute_section.html'),
                         {'extra_context': {'section_title': 'Contribute',
                                            'sidebar': {'template': 'side_bar.html'},
                                            'class_contribute': r'current'}}
                         ),
                        (r'^about/$',
                         views.FormsHtmlPage.as_view(template_name='about_section.html'),
                         {'extra_context': {'section_title': 'About',
                                            'sidebar': {'template': 'side_bar.html'},
                                            'class_about': r'current'}}
                         ),
                        (r'^help/$',
                         views.FormsHtmlPage.as_view(template_name='help_section.html'),
                         {'extra_context': {'section_title': 'Help',
                                            'sidebar': {'template': 'help_toc_side_bar.html'},
                                            'class_help': r'current'}}
                         ),
#                         (r'^todo/$',
#                          views.FormsHtmlPage.as_view(template_name='todo_section.html'),
#                          {'extra_context': {'section_title': 'To Do',
#                                             'sidebar': {'template': 'side_bar.html'},
#                                             'class_todo': r'current'}}
#                           ),
                        # Search section Results:
                        (r'^peptide_search_results/$',
                         views.PeptideResultPage.as_view()),
                        (r'^protein_search_results/$',
                         views.ProtResultPage.as_view()),
                        (r'^quick_search_results/$',
                         views.QuickResultPage.as_view()),
                        (r'^(peptide)_view/(.*)$',
                         views.PeptideViewPage.as_view()),
                        (r'^(protein)_view/(.*)$',
                         views.ProtViewPage.as_view()),
                        (r'^spectrum_image/(.*\.png)$',
                         views.Spectra2File.as_view(file_type='PNG')),
                        (r'^download_mgf/(spectrum-.*\.mgf)$',
                         views.Spectra2File.as_view(file_type='MGF')),
                        (r'^(quant_summary)/(.*)$',
                         views.QuantSummaryPage.as_view()),
                        (r'^(quant_view)/(\w*)\-(\d*)$',
                         views.QuantViewPage.as_view()),
# No more 2D Gel information
#                         (r'^twodgels_peptide_search_results/$',
#                          views.PeptideResultPage2D.as_view()),
#                         (r'^twodgels_protein_search_results/$',
#                          views.ProtResultPage2D.as_view()),
#                         (r'^twodgels_all_search_results/$',
#                          views.TwoDViewPage.as_view()),
#                         (r'^(2d_gels/viewer)\&(.*)$',
#                          views.TwoDViewPage.as_view()),
#                         (r'^(2d_gels)/(.*)$',
#                          views.TwoDDataPage.as_view()),
#                         (r'^gelimages/(.*\.jpg)$',
#                          views.TwoDGelImage.as_view()),
# No more 2D Gel information
                        # Sections that need Authentication:
                        (r'^upload/$',
                         views.UploadPage.as_view(),
                         {'extra_context': {'css_sheet': 'default_nosidebar.css',
                                            'admin_view': 'upload_view.html',
                                            'login_view': 'login_view.html',
                                            'class_upload': r'current'
                                            }}
                         ),
                        (r'^download/$',
                         views.DownloadPage.as_view(),
                         {'extra_context': {'css_sheet': 'default.css',
                                            'sidebar': {'template': 'side_bar.html'},
                                            'download_view': 'download_view.html',
                                            'login_view': 'login_view.html',
                                            'class_download': r'current'
                                            }}
                         ),
                        (r'^download_all_mgfs/(.*-spectra\.mgf)$',
                         views.PeptideSpectra2MGF.as_view(),
                         {'extra_context': {'css_sheet': 'default_nosidebar.css',
                                            'login_view': 'login_view.html',
                                            'class_search': r'current'
                                            }}
                         ),
                        # Other:
                        (r'^google855e43fe6c499f15.html$',
                         django.views.static.serve,
                         {'path': 'google855e43fe6c499f15.html',
                          'document_root': settings.TEMPLATE_DIRS[0]}
                         ),
                        (r'^file/images/padlock.*\.png$',
                         django.views.static.serve,
                         {'path': 'padlock.png',
                          'document_root': settings.SPECIAL_DOC_ROOT}
                         ),
)
