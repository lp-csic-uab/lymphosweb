# -*- coding: utf-8 -*-
"""
:synopsis:   Dispatcher classes for LymPHOS_v1_5 project views

:created:    2011-01-14

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '2.0'
__UPDATED__ = '2015-10-08'


#===============================================================================
# Imports
#===============================================================================
#DJango imports:
from django.http import HttpResponseRedirect

#Application imports:
from basic_classes import BasicHtmlPage, FormsHtmlPage
from ms_classes import PeptideResultPage, PeptideResultPage2D, PeptideViewPage
from ms_classes import ProtResultPage, ProtResultPage2D, ProtViewPage
from ms_classes import QuickResultPage, Spectra2File, PeptideSpectra2MGF
from ms_classes import QuantSummaryPage, QuantViewPage
from twodgels_classes import TwoDViewPage, TwoDDataPage, TwoDGelImage
from support_classes import UploadPage, DownloadPage


#===============================================================================
# Dispatcher classes definitions
#===============================================================================
class SearchDispatcher(object):
    '''
    Decides what search system to use depending on the user actions in
    search forms.
    '''
    def __init__(self, request = None):
        """
        Constructor
        """

        #Pseudo-constant for result view selection from a post request:
        self._fSELECT = {'peptide_search':      {'field': 'peptide_sequence',
                                                 'PageCls': PeptideResultPage,
                                                 'title': 'Search Results : Phospho-peptides found in LymPHOS DB'},
                         'all_peptides_search': {'default': '*',
                                                 'PageCls': PeptideResultPage,
                                                 'title': 'Browsing All Phospho-peptides found in LymPHOS DB'},
                         'all_proteins_search': {'default': '*',
                                                 'PageCls': ProtResultPage,
                                                 'title': 'Browsing All Phosphorylated Proteins found in LymPHOS DB'},
                         'protein_search':      {'field': 'protein_text',
                                                 'PageCls': ProtResultPage,
                                                 'title': 'Search Results : Phosphorylated Proteins found in LymPHOS DB'},
                         'quick_search':        {'field': 'quick_text',
                                                 'PageCls': QuickResultPage,
                                                 'title': 'Quick Search Results :'},
                         'all_2d_gels_search':  {'default': '',
                                                 'PageCls': TwoDViewPage,
                                                 'title': 'Browsing All 2D Gels found in LymPHOS DB'},
                         'gel_peptide_search':  {'field': 'gel_peptide_sequence',
                                                 'PageCls': PeptideResultPage2D,
                                                 'title': 'Search Results : Peptides from 2D Gels found in LymPHOS DB'},
                         'gel_protein_search':  {'field': 'gel_protein_text',
                                                 'PageCls': ProtResultPage2D,
                                                 'title': 'Search Results : Proteins found in LymPHOS DB identified in some 2D Spot'},
                         }

        #Pseudo-constant for result view selection from a get request:
        self._uSELECT = {'peptide':        {'PageCls': PeptideViewPage,
                                            'title': 'Search Results : Peptide View'},
#                         'image':          {'PageCls': Spectra2File,
#                                            'title': 'png',
#                                            'method': self._o_show},
#                         'download':       {'PageCls': Spectra2File,
#                                            'title': 'mgf',
#                                            'method': self._o_show},
                         'protein':        {'PageCls': ProtViewPage,
                                            'title': 'Search Results : Protein View'},
                         'quant_summary':  {'PageCls': QuantSummaryPage,
                                            'title': 'Results : Quantitative Summary'},
                         'quant_view':     {'PageCls': QuantViewPage,
                                            'title': 'Results : Detailed Quantitative Data',
                                            'kwargs': (('expgroup_id', int, 2),)},
                         '2d_gels/viewer': {'PageCls': TwoDViewPage,
                                            'title': 'Results : 2DE Image Viewer'},
                         '2d_gels':        {'PageCls': TwoDDataPage,
                                            'title': 'Results : 2DE Data'},
                         }

        #Attributes and attribute initialization:
        self.request = request
        self.section_title = ''
        self.extra_context = {'css_sheet': 'default_nosidebar.css',
                              'class_search': r'current'
                              }
        self.s_search = '*'

        self._search = dict.fromkeys( ('type', 'input', 'value', 'order_by'),  #Re-enter variables needed for paginated and sortable views
                                      None )                                   #
        self._p_result = None #Page results (html)
        self._o_result = None #Object results (files: spectra, mgf)

    def _p_show(self): #OPTIMIZE
        #Update _p_results page object:
        self.extra_context['section_title'] = self.section_title
        if self._search['type']:
            self.extra_context['search_type'] = self._search['type']
            self._search['type'] = None
        if self._search['input']:
            self.extra_context['search_input'] = self._search['input']
            self.extra_context['search_value'] = self._search['value']
            self._search['input'] = self._search['value'] = None
        if self._search['order_by']:
            self.extra_context['search_order_by'] = self._search['order_by']
            self._p_result.order_fields = self._search['order_by'].split(',')
            self._search['order_by'] = None
        self._p_result.context_dict.update(self.extra_context)
        self._p_result.request = self.request
        #Triggers search and context_format methods in _p_result page object:
        self._p_result.s_search = self.s_search
        #Returns the html HttpResponse of _p_result page object:
        return self._p_result.show()

#    def _o_show(self):
#        self._p_result.s_search = self.s_search
#        return self._p_result.show()

    #self.page_result is a read_only property:
    def get_p_result(self):
        return(self._p_result)
    page_result = property(get_p_result)

    #self.object_result is a read_only property:
    def get_o_result(self):
        return(self._o_result)
    object_result = property(get_o_result)

    def from_forms(self, request):
        self.request = request
        if request.method == 'POST':
            for search_type, params in self._fSELECT.iteritems():
                if search_type in request.POST:
                    self._search['type'] = search_type
                    if not params.get('field'):
                        self.s_search = params['default']
                    else:
                        self._search['input'] = params['field']
                        self.s_search = request.POST.get( params['field'],
                                                          '*' )
                        self._search['value'] = self.s_search
                    self._search['order_by'] = request.POST.get('order_by',
                                                                None)
                    self.section_title = params['title']
                    self._p_result = params['PageCls']()
                    return self._p_show()
        #
        return HttpResponseRedirect('/search/')

    def from_url(self, request, *args, **kwargs):
        self.request = request
        params = self._uSELECT[ args[0] ]
        self.section_title = params['title']
        self.s_search = args[1]
        if params.get('kwargs', None):
            for key, function, index in params['kwargs']:
                kwargs[key] = function(args[index])
        self._p_result = params['PageCls'](**kwargs)
        return_method = params.get('method', self._p_show)
        return return_method()

