# -*- coding: utf-8 -*-

"""
New LymPHOS App-database models and database logic developed using the Django framework.

LymPHOS is a DataBase containing peptidic sequences and spectrometric information 
on the phosphoproteome of human T-Lymphocytes. And it's also a Web-Application 
that allows everyone to freely access all this information from the Internet.

Authors: Montserrat Carrascal, Marina Gay, Oscar Gallardo, Joaquin Abian (LP CSIC/UAB) 
e-mail: lp.csic@uab.cat

Current State (Feb. 2011): Development

See:
models.py: for the database models (classes)
logic.py : for the database logic classes.
pseudo_templates/ : for the small templates used in logic.py.

License:
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA 02110-1301, USA.
"""
