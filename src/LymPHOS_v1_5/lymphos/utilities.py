# -*- coding: utf-8 -*-
"""
:synopsis:   Utility classes and functions for LymPHOS_v1_5 project

:created:    2011-01-14

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2011-2016 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '2.0'
__UPDATED__ = '2016-07-14'


#===============================================================================
# Imports
#===============================================================================
#DJango imports:

#Application imports:

#Python core imports:
import os
from hashlib import sha256
from collections import defaultdict


#==============================================================================
# Accessory Classes
#==============================================================================
class NoPaginator(object):
    """
    This class simulates a Django :class:``Paginator``, so a page view can't be
    paginated but still exposes the same interface as if it was (useful for
    changing a view from paginated to no paginated without a lot of code and
    template changes).
    """
    def __init__(self, iterable, *args, **kwargs):
        self.object_list = iterable
        self.has_previous = False
        self.has_next = False
        self.paginator = self
        self.number = 1
        self.num_pages = 1
        self.start_index = 1
        self.end_index = len(self.object_list)
        self.count = self.end_index
        self.next_page_number = 1
        self.previous_page_number = 1

    def page(self, *args):
        return self


class LoggerCls(object):
    def __init__(self, key_func=None, *func_args, **func_kwargs):
        self._log = defaultdict(list)
        self._crono_keys = list() #List of keys in self._log, in chronological order

        if not key_func:
            self.key_func = self.crono_index
        else:
            self.key_func = key_func

        self.func_args = func_args
        self.func_kwargs = func_kwargs

    def __call__(self, key=None):
        if key:
            return list( self._log.get(key, []) )
        else:
            return self.log

    def get_log(self):
        return self._log.copy()
    def set_log(self, value):
        self.put(value)
    def del_log(self):
        self._log.clear()
        self._crono_keys = list()
    log = property(get_log, set_log, del_log)

    def put(self, value):
        key = self.key_func(*self.func_args, **self.func_kwargs)
        self.put_with_key(key, value)
        return key

    def put_with_key(self, key, value):
        self._log[key].append(value)
        self._crono_keys.append(key)

    def keys(self):
        return list(self._crono_keys)

    def crono_index(self, start=0):
        return len(self._crono_keys) + start


class UserLog(LoggerCls): #TO_DO: Implement class UserLog??
    def __init__(self):
        #__init__ from superclass:
        LoggerCls.__init__(self)

        self._request = None
        self.session_id = None

    def get_request(self):
        return self._request
    def set_request(self, request):
        self._request = request
        self.session_id = hash(self._request)
        return
    request = property(get_request, set_request)


#==============================================================================
# Accessory Functions
#==============================================================================
def get_random_data(length):
    """
    Generate a random key of specified byte-length. It uses an OS source of
    randomness, or, alternatively the Python pseudo-random number generator.

    :param int length: byte-length of the desired random key.

    :return str : a random key of specified byte-length as a string.
    """
    try:
        random_key = os.urandom(length)
    except NotImplementedError as _:
        import random
        random.seed()
        random_bytes = bytearray( random.randint(0, 255) for _ in xrange(length) )
        random_key = str(random_bytes)
    return random_key


def xor_encrypt(clear_string, random_key=None):
    """
    Encrypts a string with a random key using the XOR cipher algorithm:
    https://en.wikipedia.org/wiki/XOR_cipher
    https://en.wikipedia.org/wiki/One-time_pad

    :param str clear_string: a string of random bytes.
    :param obj random_key: a string of random bytes, preferably of the same
    length as `clear_string`, or None to generate a random one.

    :return tuple :
    """
    clear_bytes = bytearray(clear_string)
    # Prepare or get a `random_key` of the same length as `clear_string`:
    clearbytes_len = len(clear_bytes)
    if random_key:
        random_bytes = bytearray(random_key)
        randombytes_len = len(random_bytes)
        diffbytes_len = clearbytes_len - randombytes_len
        if diffbytes_len > 0:
            extra_mult = int(randombytes_len / diffbytes_len) + 1
            extra_random_bytes = random_bytes * extra_mult
            random_bytes.extend( extra_random_bytes[:diffbytes_len] )
    else:
        random_key = get_random_data(clearbytes_len)
        random_bytes = bytearray(random_key)
    # Do a byte-XOR operation between bytes in `clear_bytes` and `random_bytes`:
    xor_bytes = bytearray( a ^ b for a, b in zip(clear_bytes, random_bytes) )
    xor_string = str(xor_bytes)
    return xor_string, random_key


def xor_decrypt(xor_string, random_key):
    """
    Decrypts a encrypted string obtained using the XOR cipher algorithm with a
    the supplied random key.

    :param str xor_string: the encrypted string of bytes to decrypt.
    :param str random_key: the random key used to obtain `xor_string`.

    :return str : the original clear text that was encrypted with the
    `random_key` using the XOR cipher algorithm.
    """
    return xor_encrypt(xor_string, random_key)[0]


def kdf_sha256(string, iterations=1):
    """
    A hash key derivation function using the sha256 algorithm.
    """
    prev = string
    string = sha256(string).hexdigest()
    #
    for i in xrange(iterations):
        prev = sha256(string + str(i) + prev).hexdigest()
    return prev


