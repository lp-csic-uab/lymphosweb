# -*- coding: utf-8 -*-
"""
:synopsis:   Test classes for LymPHOS_v1_5 project

:created:    2012-03-16

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    1.5 dev
:updated:    2014-04-24
"""

"""
This file demonstrates two different styles of tests (one doctest and one
unittest). These will both pass when you run "manage.py test".

Replace these with more appropriate tests for your application.
"""
#class SimpleTest(TestCase):
#    def test_basic_addition(self):
#        """
#        Tests that 1 + 1 always equals 2.
#        """
#        self.failUnlessEqual(1 + 1, 2)
#
#__test__ = {"doctest": """
#Another way to test that 1 + 1 is equal to 2.
#
#>>> 1 + 1 == 2
#True
#"""}


#===============================================================================
# Imports
#===============================================================================
from django.test import TestCase

from LymPHOS_v1_5.lymphos import logic, filters, models


#===============================================================================
# TestCase class definitions
#===============================================================================
class SameSpotsConverterTest(TestCase):
    def __init__(self, methodName='runTest', i_file=None):
        self.file = i_file
        #
        super(SameSpotsConverterTest, self).__init__(methodName)
    
    def setUp(self):
        self.filter = filters.TwoDGelZIPConverter()
        self.assertIsInstance(self.filter, filters.TwoDGelZIPConverter, 
                              'The filter is not an instance of '\
                              'filters.SameSpotsConverter')
        if not self.file:
            self.file = r'D:\Oscar\Mis documentos\Baixades\Gels\testjsonvisor.db'
        self._load_csv()
    
    def _load_csv(self):
        with open(self.file, 'r') as io_file:
            self.filter.file = self.file
            self.filter.load_data(io_file)
    
    def test_load_csv(self):
        self.assertIsInstance(self.filter.data[0], list, 
                              'Converter loaded data is not a list of lists')
        self.assertGreaterEqual(len(self.filter.data), 1000)
    
    def test_import_csv(self):
        self.filter.import_data()

class JsonIntegratorConverterTest(TestCase):
    def __init__(self, methodName='runTest', i_file=None):
        self.file = i_file
        #
        super(JsonIntegratorConverterTest, self).__init__(methodName)
    
    def setUp(self):
        self.filter = filters.JsonIntegratorConverter()
        self.assertIsInstance(self.filter, filters.JsonIntegratorConverter, 
                              'The filter is not an instance of '\
                              'filters.SameSpotsConverter')
        if not self.file:
            self.file = r'D:\Oscar\Mis documentos\Baixades\Gels\testdige2.csv'
        self._load_json()
    
    def _load_json(self):
        with open(self.file, 'r') as io_file:
            self.filter.file = self.file
            self.filter.load_data(io_file)
    
    def test_load_json(self):
        self.assertIsInstance(self.filter.data.values()[0], dict, 
                              'Converter loaded data is not a list of dictionaries')
        self.assertGreaterEqual(len(self.filter.data), 1000)
        self.assertIn('LT11_PS_070705.350.350', self.filter.data.keys(), 
                      'Data was not fully loaded from file {0}'.format(self.file))
    
    def test_import_json(self):
        self.filter.import_data()

