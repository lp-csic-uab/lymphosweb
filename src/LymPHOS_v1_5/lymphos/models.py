# -*- coding: utf-8 -*-
"""
:synopsis: New Django Models for the new LymPHOS Database Schema (see
Documentation folder for more information).

:created:    2011-03-14

:authors:    Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2011-2016 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '2.0'
__UPDATED__ = '2016-12-20'


#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django.db import models
from django.conf import settings

# Application imports:
from utilities import get_random_data, xor_encrypt, xor_decrypt, kdf_sha256

# Python core imports:
import re
import random
from collections import defaultdict, OrderedDict
from uuid import uuid4
try:
    from hashlib import pbkdf2_hmac
except ImportError as _:
    from backports.pbkdf2 import pbkdf2_hmac


#===============================================================================
# Models definitions
#===============================================================================
class AbstractData(models.Model):
    phospho = models.BooleanField(db_index=True, default=True)
    n_prots = models.PositiveSmallIntegerField(default=0)
    dbpsites = models.CharField(max_length=20, default='')
    dbascores = models.CharField(max_length=85, default='')

    _PEP_THRESHOLD = 19
    _CSS_P = 'secP'
    _CSS_PD = 'secPdoubt'

    def get_psites(self):
        if self.dbpsites:
            return map(int, self.dbpsites.split('|'))
        else:
            return list()
    #
    def set_psites(self, psites_list):
        if psites_list:
            self.dbpsites = '|'.join(map(str, psites_list))
            return self
        else:
            return None
    #
    psites = property(get_psites, set_psites)

    def get_ascores(self):
        if self.dbascores:
            return map(float, self.dbascores.split('|'))
        else:
            return list()
    #
    def set_ascores(self, ascores_list):
        if ascores_list:
            self.dbascores = '|'.join(map(str, ascores_list))
            return self
        else:
            return None
    #
    ascores = property(get_ascores, set_ascores)

    def get_htmlpeptide(self):
        """
        Formating function. Format a peptide string as partial html, where
        phosphorilation sites (p-sites) are marked acordignly to the maximum
        ascore values for those p-sites.
        """
        pep_aas = list(self.peptide)
        for psite, ascore in zip(self.psites, self.ascores):
            if ascore > self._PEP_THRESHOLD:
                psite_class = self._CSS_P
            else:
                psite_class = self._CSS_PD
            pep_aas[psite] = '<span class=\"{0}\">{1}</span>'.format(psite_class, pep_aas[psite])
        return ''.join(pep_aas)
    #
    htmlpeptide = property(get_htmlpeptide)

    def get_n_prots(self):
        return len(self.proteins)

    def get_n_decoys(self):
        return len(self.decoys)
    #
    n_decoys = property(get_n_decoys)

    def get_n_prots_w_decoys(self):
        return self.get_n_prots() + self.get_n_decoys()
    #
    n_prots_w_decoys = property(get_n_prots_w_decoys)

    class Meta:
        abstract = True


class PeptideCache(AbstractData):
    peptide = models.CharField(primary_key=True, max_length=80)
    dbproteins = models.TextField(default='')
    quantitative = models.BooleanField(default=False)
    n_scans = models.PositiveSmallIntegerField(default=1)
    gel = models.BooleanField(default=False)

    def get_proteins_ac_name(self):
        if self.dbproteins:
            return [ tuple( t_prot.split('^') ) for t_prot in
                     self.dbproteins.split('|') ]
        else:
            return None

    def set_proteins_ac_name(self, iter_t_proteins):
        if iter_t_proteins:
            iter_t_proteins = set(iter_t_proteins)
            self.dbproteins = '|'.join( ['^'.join(prot) for prot in
                                         iter_t_proteins] )
            return self
        else:
            return None

    l_prots = property(get_proteins_ac_name, set_proteins_ac_name)

    def get_prots_ac(self, is_decoy=False):
        if self.dbproteins:
            return [t_prot.split('^')[0] for t_prot in
                    self.dbproteins.split('|') if
                    (not is_decoy and 'decoy' not in t_prot) or
                    (is_decoy and 'decoy' in t_prot)]
        else:
            return None

    proteins = property(get_prots_ac)

    def get_decoys(self):
        return self.get_prots_ac(is_decoy=True)

    decoys = property(get_decoys)

    def get_prots_ac_w_decoys(self):
        return self.proteins + self.decoys

    proteins_w_decoys = property(get_prots_ac_w_decoys)

    def get_grpprots(self):
        """
        Formating function. Returns a dictionary from self.l_prots:
        [(P1,name1), (P1-3,name1), (P1-2,name1),        {(P1,name1):    [2, 3],
         (P2,name2),                              ==>    (P2,name2):    [],
         (P3-2,name3), (P3-3,name3)]                     (P3-2,name3):  [3]}
        """
        protsfrompepsgrpd = defaultdict(list)
        for prot_ac, prot_name in self.l_prots:
            #[(P1,name1), (P1-2,name1), (P1-3,name1),     {(P1,name1): [0, 3, 2],
            # (P2,name2),                             ==>  (P2,name2): [0],
            # (P3-2,name3), (P3-3,name3)]                  (P3,name3): [2, 3]}
            rootprot_ac, _, isofnum = prot_ac.partition('-')
            protroot = (rootprot_ac, prot_name)
            protsfrompepsgrpd[protroot].append(int(isofnum) if isofnum else 0)
        protsfrompepsgrpd2 = dict()
        for protroot, isofnums in protsfrompepsgrpd.items():
            #{(P1,name1): [0, 3, 2],         {(P1,name1):    [2, 3],
            # (P2,name2): [0],         ==>    (P2,name2):    [],
            # (P3,name3): [2, 3]}             (P3-2,name3):  [3]}
            rootprot_ac, prot_name = protroot
            isofnums.sort()
            isofnum = isofnums.pop(0)
            prot_isof = (rootprot_ac, prot_name, isofnum)
            protsfrompepsgrpd2[prot_isof] = isofnums
        return protsfrompepsgrpd2.copy()

    grpprots = property(get_grpprots)


class ExpGroup(models.Model):
    name = models.TextField(default='')
    description = models.TextField(default='')

    def __unicode__(self):
        info = list()
        if self.pk: info.append('ID: {0}'.format(self.pk))
        if self.name: info.append('Name: {0}'.format(self.name))
        return ', '.join(info)


class Miapes(models.Model):
    id = models.CharField(primary_key=True, max_length=3)
    xml = models.TextField(default='')


class Experiments(models.Model):
    expgroup = models.ForeignKey(ExpGroup, default=-1)
    dbsub_exps = models.CharField(max_length=50, default='')
    name = models.CharField(max_length=100, default='')
    dbmiape_ids = models.CharField(max_length=50, default='')
    description = models.CharField(max_length=180, default='')
    instrument = models.CharField(max_length=150, default='')
    label = models.CharField(max_length=10, default='')
    quant_cond = models.TextField(default='')
    miapes = models.ManyToManyField(Miapes, db_table='lymphos_experiments_miapes')

    def __unicode__(self):
        info = list()
        if self.pk: info.append('ID: {0}'.format(self.pk))
        if self.expgroup_id: info.append('ExpGroup: {0}'.format(self.expgroup_id))
        l_miapes = self.get_miape_ids()
        if l_miapes: info.append('MIAPEs: {0}'.format(l_miapes))
        if self.description: info.append('Desc.: {0}'.format(self.description))
        return ', '.join(info)

    def get_sub_exps(self):
        if self.dbsub_exps:
            return self.dbsub_exps.split('|')
        else:
            return None
    def set_sub_exps(self, sub_exps_list):
        if sub_exps_list:
            self.dbsub_exps = '|'.join(sub_exps_list)
            return self
        else:
            return None
    sub_exps = property(get_sub_exps, set_sub_exps)

    def get_miape_ids(self):
        if self.dbmiape_ids:
            return self.dbmiape_ids.split('|')
        else:
            return None
    def set_miape_ids(self, miape_ids_list):
        if miape_ids_list:
            self.dbmiape_ids = '|'.join(miape_ids_list)
            return self
        else:
            return None
    miape_ids = property(get_miape_ids, set_miape_ids)

    def get_quant_cond(self):
        if self.quant_cond:
            return map(int, self.quant_cond.split('|'))
        else:
            return None
    def set_quant_cond(self, quant_cond_list):
        if quant_cond_list:
            self.quant_cond = '|'.join(map(str, quant_cond_list))
            return self
        else:
            return None
    quanttimes = property(get_quant_cond, set_quant_cond)

    def iscontrol(self):
        """
        Indicates when experimental conditions are control conditions
        """
        t_quanttimes = self.quanttimes
        if t_quanttimes:
            #It returns True (Is Control) when all times = 0
            #(self.get_quant_cond() = [0, 0, ..., 0]), else it returns False
            return set(t_quanttimes) == set([0])
        else:
            #It's not control, but because it's not a quantitative experiment
            #so return None to differentiate from False
            return None


class QuantPepExpCond(models.Model):
    unique_pep = models.CharField(max_length=80, db_index=True, default='')
    expgroup = models.ForeignKey(ExpGroup, default=-1)
    dballquanttimes = models.TextField(default='')
    dbavgratios = models.TextField(default='')
    dbstdv = models.TextField(default='')
    dbavgregulation = models.CharField(max_length=200, default='')
    # PQuantifier derived statistics:
    dbttestpassed95 = models.CharField(max_length=100, default='') #For each condition/time, according to `dballquanttimes`, T student Test at 95% confidence passed? 1 -> yes, 0 -> no. Ex. '0|1'
    dbttestpassed99 = models.CharField(max_length=100, default='') #For each condition/time, according to `dballquanttimes`, T student Test at 99% confidence passed? 1 -> yes, 0 -> no. Ex. '0|1'
    dbsdgreatersdmeanx3 = models.CharField(max_length=100, default='') #For each condition/time, according to `dballquanttimes`, is standard deviation greater than 3 times the standard deviation mean for that condition/time? 1 -> yes, 0 -> no. Ex. '0|1'
    # DanteR data:
    dbdanteravgratios = models.TextField(default='') #DanteR ANOVA log2(FoldCahnge), for each condition/time according to `dballquanttimes`. Ex. '1.2345|2.3456'
    dbdanterpvalues = models.TextField(default='') #DanteR ANOVA p-values, for each condition/time according to `dballquanttimes`. Ex. '0.0123|0.0456'
    dbdanteradjpvalues = models.TextField(default='') #DanteR Adjusted ANOVA p-values, for each condition/time according to `dballquanttimes`. Ex. '0.0123|0.0456'
    # DanteR processing constants:
    _PVAL_THRESHOLD = 0.05 #p-Value threshold
    _FC_THRESHOLD = 1.5 #Fold Change threshold

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        equality = True #Don't take primary key values into account.
        equality = equality and self.unique_pep == other.unique_pep
        equality = equality and self.expgroup_id == other.expgroup_id
        return equality

    def __hash__(self):
        hash_tuple = (self.unique_pep, self.expgroup_id)
        return hash(hash_tuple)

    def __unicode__(self):
        info = list()
        if self.unique_pep: info.append('Peptide: {0}'.format(self.unique_pep))
        if self.expgroup_id: info.append('ExpGroup: {0}'.format(self.expgroup_id))
        if self.dbavgregulation: info.append('Times: {0}'.format(self.dballquanttimes))
        return ', '.join(info)

    def get_quanttimes(self):
        if self.dballquanttimes:
            return map( int, self.dballquanttimes.split('|') )
        else:
            return None
    def set_quanttimes(self, allquanttimes_list):
        if allquanttimes_list:
            self.dballquanttimes = '|'.join( map(str, allquanttimes_list) )
            return self
        else:
            return None
    quanttimes = property(get_quanttimes, set_quanttimes)

    def get_ratios(self):
        if self.dbavgratios:
            return map( float, self.dbavgratios.split('|') )
        else:
            return None
    def set_ratios(self, avgratios_list):
        if avgratios_list:
            self.dbavgratios = '|'.join( map(str, avgratios_list) )
            return self
        else:
            return None
    ratios = property(get_ratios, set_ratios)

    def get_stdv(self):
        if self.dbstdv:
            return map( float, self.dbstdv.split('|') )
        else:
            return None
    def set_stdv(self, stdv_list):
        if stdv_list:
            self.dbstdv = '|'.join( map(str, stdv_list) )
            return self
        else:
            return None
    stdv = property(get_stdv, set_stdv)

    def get_reg_values(self):
        if self.dbavgregulation:
            return [ map( int, l_reg.split('^') )
                     for l_reg in self.dbavgregulation.split('|') ]
        else:
            return None
    def set_reg_values(self, regulation_l_list):
        if regulation_l_list:
            self.dbavgregulation = '|'.join( '^'.join( map(str, l_reg) )
                                             for l_reg in regulation_l_list )
            return self
        else:
            return None
    reg_values = property(get_reg_values, set_reg_values)

    def get_ttestpassed95(self):
        if self.dbttestpassed95:
            return map( int, self.dbttestpassed95.split('|') )
        else:
            return [None]*len(self.quanttimes)
    def set_ttestpassed95(self, ttestpassed95_list):
        if ttestpassed95_list:
            self.dbttestpassed95 = '|'.join( map(str, ttestpassed95_list) )
            return self
        else:
            return None
    ttestpassed95 = property(get_ttestpassed95, set_ttestpassed95)

    def get_ttestpassed99(self):
        if self.dbttestpassed99:
            return map( int, self.dbttestpassed99.split('|') )
        else:
            return [None]*len(self.quanttimes)
    def set_ttestpassed99(self, ttestpassed99_list):
        if ttestpassed99_list:
            self.dbttestpassed99 = '|'.join( map(str, ttestpassed99_list) )
            return self
        else:
            return None
    ttestpassed99 = property(get_ttestpassed99, set_ttestpassed99)

    def get_sdgreatersdmeanx3(self):
        if self.dbsdgreatersdmeanx3:
            return map( int, self.dbsdgreatersdmeanx3.split('|') )
        else:
            return [None]*len(self.quanttimes)
    def set_sdgreatersdmeanx3(self, sdgreatersdmeanx3_list):
        if sdgreatersdmeanx3_list:
            self.dbsdgreatersdmeanx3 = '|'.join( map(str, sdgreatersdmeanx3_list) )
            return self
        else:
            return None
    sdgreatersdmeanx3 = property(get_sdgreatersdmeanx3, set_sdgreatersdmeanx3)

    def get_danteravgratios(self):
        if self.dbdanteravgratios:
            return map( float, self.dbdanteravgratios.split('|') )
        else:
            return [None]*len(self.quanttimes)
    def set_danteravgratios(self, danteravgratios_list):
        if danteravgratios_list:
            self.dbdanteravgratios = '|'.join( map(str, danteravgratios_list) )
            return self
        else:
            return None
    danteravgratios = property(get_danteravgratios, set_danteravgratios)

    def get_danterpvalues(self):
        if self.dbdanterpvalues:
            return map( float, self.dbdanterpvalues.split('|') )
        else:
            return [None]*len(self.quanttimes)
    def set_danterpvalues(self, danterpvalues_list):
        if danterpvalues_list:
            self.dbdanterpvalues = '|'.join( map(str, danterpvalues_list) )
            return self
        else:
            return None
    danterpvalues = property(get_danterpvalues, set_danterpvalues)

    def get_danteradjpvalues(self):
        if self.dbdanteradjpvalues:
            return map( float, self.dbdanteradjpvalues.split('|') )
        else:
            return [None]*len(self.quanttimes)
    def set_danteradjpvalues(self, danteradjpvalues_list):
        if danteradjpvalues_list:
            self.dbdanteradjpvalues = '|'.join( map(str, danteradjpvalues_list) )
            return self
        else:
            return None
    danteradjpvalues = property(get_danteradjpvalues, set_danteradjpvalues)

    def get_danterchange(self):
        """
        :returns list : indicate change (1), no-change (0), or no data (None)
        for each time-condition, according to a fold change threshold and an
        adjusted p-value threshold.
        """
        change = list()
        for adjpval, avgratio in zip(self.danteradjpvalues, self.danteravgratios):
            if adjpval is None or avgratio is None:
                change.append(None)
            else:
                fc = 2 ** avgratio
                if (fc > self._FC_THRESHOLD or fc < 1/self._FC_THRESHOLD) and adjpval < self._PVAL_THRESHOLD:
                    change.append(1)
                else:
                    change.append(0)
        return change
    danterchange = property(get_danterchange)

    def reg_indexes_fits(self):
        """
        Calculate the most probable regulation condition of a peptide for each
        assayed time (each list inside the ``reg_values`` list property): the
        chosen condition (up, down or no regulation) has to have a greater
        number of experiments according to that condition than the other
        conditions.

        ``reg_index`` is the regulation condition (no-regulation(0), down(-1)
        or up(1) regulation), calculated based in the total number of
        experiments in each regulation condition.
        ``reg_fit`` is the index of confidence (goodness) of the calculated
        regulation condition (``reg_index``).
        """
        l_reg_inds_fits = list()
        for reg_list in self.reg_values:
            total = float( sum(reg_list) )
            if total > 1:
                up_or_down = reg_list[2] - reg_list[0] #upregulated - downregulated
                abs_up_or_down = abs(up_or_down)
                if up_or_down != 0: #down(-1) or up(1) regulation
                    reg_index = -1 if up_or_down < 0 else 1
                else: #No-regulation(0)
                    reg_index = 0
                    if reg_list[1]:
                        abs_up_or_down = reg_list[1]
                    else:
                        abs_up_or_down = total / 2 #This is to have a reg_fit of 0.5 (50%)
                #Calculate the goodness of the election:
                reg_fit = abs_up_or_down / total #TO_DO: Improve fit index
            else:
                reg_index = reg_list.index(1) - 1
                reg_fit = -1
            #
            l_reg_inds_fits.append( {'regulation': reg_index,
                                     'confidence': reg_fit} )
        return l_reg_inds_fits

    def reg_indexes_fits_threshold(self, threshold):
        """
        Alternative method to calculate the best regulation condition of a
        peptide for each assayed time (each list inside the ``reg_values`` list
        property): the chosen condition(up, down or no regulation) must have a
        greater percentage of experiments according to that condition than the
        threshold percentage.
        :caution: `threshold` percentage must be between 0.5 and 1.

        ``reg_fit`` is the calculated index of confidence (goodness) of the
        regulation condition calculated.
        """
        l_reg_inds_fits = list()
        for reg_list in self.reg_values:
            total = float(sum(reg_list))
            if total > 1:
                percent_reg_list = [each/total for each in reg_list]
                highest = max(percent_reg_list)
                if highest > threshold: #-1(down), 0(no) or 1(up) regulation:
                    reg_index = percent_reg_list.index(highest) - 1
                else: #No-regulation:
                    reg_index = 0
                #Calculate the goodness of the election:
                reg_fit = (highest - threshold) / (1 - threshold)
            else:
                reg_index = reg_list.index(1) - 1
                reg_fit = -1
            #
            l_reg_inds_fits.append( {'regulation': reg_index,
                                     'confidence': reg_fit} )
        return l_reg_inds_fits

    def time_values(self):
        """
        :returns: a dictionary with the following elements:
          {time: ( {'regulation': regulation index, 'confidence': confidence},
                   [#downregulated, #no-change, #upregulated],
                   mean ratio,
                   stdv,
                   t-test passed 95%,
                   t-test passed 99%,
                   DanteR mean ratio,
                   DanteR fold change > threshold and adjusted p-value < threshold ) }
        """
        return dict( zip(self.quanttimes, zip(self.reg_indexes_fits(),
                                              self.reg_values,
                                              self.ratios,
                                              self.stdv,
                                              self.ttestpassed95,
                                              self.ttestpassed99,
                                              self.danteravgratios,
                                              self.danterchange)) )

    def time2reg_data(self):
        """
        :returns OrderedDict : a dictionary with the following elements:
          {time: {'reg_index': {'regulation': regulation index,
                                'confidence': confidence},
                  'reg_values': [#downregulated, #no-change, #upregulated],
                  'ratio': mean ratio,
                  'stdv': stdv,
                  'ttest95': t-test passed 95%,
                  'ttest99': t-test passed 99%,
                  'danter_ratio': DanteR mean ratio (est),
                  'danter_change': DanteR fold change > threshold and adjusted p-value < threshold} }
        """
        time2reg_data = OrderedDict()
        for time, reg_index, reg_values, ratio, stdv, ttest95, ttest99 ,drratio, drchange in zip(self.quanttimes,
                                                                                                 self.reg_indexes_fits(),
                                                                                                 self.reg_values,
                                                                                                 self.ratios,
                                                                                                 self.stdv,
                                                                                                 self.ttestpassed95,
                                                                                                 self.ttestpassed99,
                                                                                                 self.danteravgratios,
                                                                                                 self.danterchange):
            time2reg_data[time] = {'reg_index': reg_index,
                                   'reg_values': reg_values,
                                   'ratio': ratio,
                                   'stdv': stdv,
                                   'ttest95': ttest95,
                                   'ttest99': ttest99,
                                   'danter_ratio': drratio,
                                   'danter_change': drchange}

        return time2reg_data


class NoPhospho_Stats(models.Model):
    """
    Stores statistical information for no phosphorilated peptides, for further
    T-test estimations by LymPHOS Tools/export_LymPHOSDB2CSVReport.py.

    X-NOTE: Maybe it's no more necessary since T-test calculations are done
    now in PQuantifier (since revision 138, at 2015-08-17).
    """
    quanttime = models.SmallIntegerField(primary_key=True)
    mu = models.FloatField(default=0.0)
    sigma = models.FloatField(default=0.0)
    n_spectra = models.IntegerField(default=1)


class Proteins(models.Model):
    ac = models.CharField(primary_key=True, max_length=25)
    phospho = models.BooleanField(db_index=True, default=False)
    is_decoy = models.BooleanField(db_index=True, default=False)
    up_id = models.CharField(max_length=25, default='')
    up_ver = models.PositiveIntegerField(default=0)
    name = models.CharField(max_length=300, default='')
    chr = models.CharField(max_length=5, default='')
    function = models.TextField(default='')
    location = models.CharField(max_length=300, default='')
    seq = models.TextField(default='')
    up_psites = models.TextField(default='')
    score_lcms = models.FloatField(default=0.0)
    #data_set = backward relation to the data objects supporting the protein
    #spot_set = backward relation to the spots where the protein was identified

    # Variables for caching/memoization purposes:
    _peptidenames = None
    _peptides = None
    _phosphopeptides = None
    _psitesfrompeps = None
    _protsfrompeps = None
    _psitesfrompeps_w_metadata = None

    # Class constants:
    AA2REGEX_PROTEOMICS  = {'B': '[BND]', 'Z': '[ZEQ]', 'J': '[JIL]', 'X': '.',
                            'I': '[JIL]', 'L': '[JIL]'}
    AA2NAME = {'S': 'Serine', 'T': 'Threonine', 'Y': 'Tyrosine'}

    def __unicode__(self):
        info = list()
        if self.ac: info.append('AC: {0}'.format(self.ac))
        if self.up_ver: info.append('UniProt version: {0}'.format(self.up_ver))
        return ', '.join(info)

    def get_peptidenames(self):
        """
        Memoized peptide LymPHOS sequences associated with this protein.
        """
        if not self._peptidenames:
            self._peptidenames = sorted(set( self.data_set.values_list('peptide',
                                                                       flat=True) ))
        return self._peptidenames
    #
    peptidenames = property(get_peptidenames)

    def get_peptides(self):
        """
        Memoized peptides (query of :class:`PeptideCache` instances) for this
        protein.
        """
        if not self._peptides:
            self._peptides = PeptideCache.objects.filter(peptide__in = self.peptidenames)
        return self._peptides
    #
    peptides = property(get_peptides)

    def get_phosphopeptides(self):
        """
        Memoized phospho-peptides (query of :class:`PeptideCache` instances
        filtered for only the phosphorylated ones) for this protein.
        """
        if not self._phosphopeptides:
            self._phosphopeptides = self.peptides.filter(phospho = True)
        return self._phosphopeptides
    #
    phosphopeptides = property(get_phosphopeptides)

    def get_n_peptides(self):
        return len(self.peptidenames)
    #
    n_peptides = property(get_n_peptides)

    def get_psitesfrompeps_w_metadata(self):
        """
        Memoized phospho-sites found in prot. from the p-sites in the peptides
        matching this protein, wit additional metadata.

        Returns a defaultdict containing ALL the p-site positions within the
        protein sequence (each position start at 1, not at 0 like python
        strings, lists, ...) with associated list of metadata dicts.
        """
        if not self._psitesfrompeps_w_metadata:
            prot_psites = defaultdict(list)
            # Get p-sites positions and metadata from related phosphopeptides:
            for ppeptide in self.phosphopeptides:
                ppep_seq = self.regex_format(ppeptide.peptide.upper())
                ppep_len = len(ppeptide.peptide) - 1
                ppep_psites_ascores = zip(ppeptide.psites, ppeptide.ascores)
                for match in re.finditer(ppep_seq, self.seq):
                    prot_ppep_pos = match.start() + 1 #phosphopeptide position in prot. sequence.
                    #Basic phosphopeptide metadata for all its associated p-sites:
                    prot_psite_metadata = {'ppeptide': ppeptide,
                                           'start_pos': prot_ppep_pos,
                                           'end_pos': prot_ppep_pos + ppep_len}
                    for psite, ascore in ppep_psites_ascores:
                        prot_psite_pos = prot_ppep_pos + psite #p-site position in prot. sequence.
                        prot_psite_metadata['ascore'] = ascore #Update metadata for this p-site.
                        prot_psites[prot_psite_pos].append(prot_psite_metadata)
            self._psitesfrompeps_w_metadata = prot_psites
        return self._psitesfrompeps_w_metadata
    #
    psitesfrompeps_w_metadata = property(get_psitesfrompeps_w_metadata)

    def get_psitesfrompeps(self):
        """
        Memoized phospho-sites found in prot. from the p-sites in the peptides
        matching this protein.

        Returns a set containing ALL the p-site positions within the protein
        sequence (each position start at 1, not at 0 like python strings, lists,
        ...)
        """
        if not self._psitesfrompeps:
#             prot_psites = set()
#             # Get p-sites positions from related phosphopeptides:
#             for ppeptide in self.phosphopeptides:
#                 ppep_seq = self.regex_format(ppeptide.peptide.upper())
#                 ppep_psites = ppeptide.psites
#                 for match in re.finditer(ppep_seq, self.seq):
#                     for psite in ppep_psites:
#                         prot_psites.add(match.start() + psite + 1) #Add p-site position in prot. sequence.
            self._psitesfrompeps = set( self.psitesfrompeps_w_metadata.keys() )
        return self._psitesfrompeps
    #
    psitesfrompeps = property(get_psitesfrompeps)

    def get_n_psitesfrompeps(self):
        return len(self.psitesfrompeps)
    #
    n_psitesfrompeps = property(get_n_psitesfrompeps)

    def _psite_one_letter(self, psite):
        return '{0}-{1}'.format(self.seq[psite - 1], psite)

    def _psite_full_name(self, psite):
        return '{0} {1}'.format(self.AA2NAME[ self.seq[psite - 1] ], psite)

    def get_oneletter_psitesfrompeps(self):
        return { self._psite_one_letter(psite)
                 for psite in self.get_psitesfrompeps() }
    #
    oneletter_psitesfrompeps = property(get_oneletter_psitesfrompeps)

    def get_named_psitesfrompeps(self):
        return { self._psite_full_name(psite)
                 for psite in self.get_psitesfrompeps() }
    #
    named_psitesfrompeps = property(get_named_psitesfrompeps)

    def _get_named_psitesfrompeps_w_metadata(self, psite_name_func):
        named_psite2metadata = OrderedDict()
        for psite, metadata in sorted( self.get_psitesfrompeps_w_metadata().items() ):
            named_psite = psite_name_func(psite)
            sorted_metadata = sorted( metadata, key=lambda d: d['start_pos'] )
            named_psite2metadata[named_psite] = sorted_metadata
        return named_psite2metadata

    def get_named_psitesfrompeps_w_metadata(self):
        return self._get_named_psitesfrompeps_w_metadata(self._psite_full_name)
    #
    named_psitesfrompeps_w_metadata = property(get_named_psitesfrompeps_w_metadata)

    def get_oneletter_psitesfrompeps_w_metadata(self):
        return self._get_named_psitesfrompeps_w_metadata(self._psite_one_letter)
    #
    oneletter_psitesfrompeps_w_metadata = property(get_oneletter_psitesfrompeps_w_metadata)

    def get_pepsin2d(self):
        """
        Check if any peptide identifying this prot. was found in a 2D-Gel.
        """
        anypepin2d = False
        # Get relational 2D information:
        for peptide in self.peptides:
            if peptide.pep:
                anypepin2d = True
                break
        return anypepin2d
    #
    pepsin2d = property(get_pepsin2d)

    def get_protsfrompeps(self):
        """
        Check if the combination of record.peptides identifying this prot. only
        match for this prot., or identify also other prots.
        Returns all the prots. identyfied by the peptides identifying this prot.
        """
        if not self._protsfrompeps:
            prots = set()
            # Get common proteins for related peptides:
            for peptide in self.peptides:
                if prots:
                    prots = prots.intersection(peptide.l_prots)
                else:
                    prots = set(peptide.l_prots)
#            prots.discard( (self.ac, self.name) ) #Eliminate the current prot.
            self._protsfrompeps = sorted(prots)
        return self._protsfrompeps
    #
    protsfrompeps = property(get_protsfrompeps)

    def get_grpprotsfrompeps(self):
        """
        Formating function. Returns a dictionary from self.protsfrompeps:
        [(P1,name), (P1-3,name), (P1-2,name),            {P1:       [2, 3],
         (self.ac,self.name),                    ==>      self.ac:  [],
         (P3-2,name), (P3-3,name)]                        P3-2:     [3]}
        """
        protsfrompepsgrpd = defaultdict(list)
        for prot in self.protsfrompeps:
            #[(P1,name), (P1-2,name), (P1-3,name),            {P1:       [0, 3, 2],
            # (self.ac,self.name),                    ==>      self.ac:  [0],
            # (P3-2,name), (P3-3,name)]                        P3:       [2, 3]}
            protroot, _, isonum = prot[0].partition('-')
            protsfrompepsgrpd[protroot].append(int(isonum) if isonum else 0)
        for protroot, isonums in protsfrompepsgrpd.items(): #Inline modification of a dictionary
            #{P1:       [0, 3, 2],         {P1:       [2, 3],
            # self.ac:  [0],         ==>    self.ac:  [],
            # P3:       [2, 3]}             P3-2:     [3]}
            isonums.sort()
            if 0 in isonums:
                isonums.remove(0)
            else:
                prot_iso = '{0}-{1}'.format(protroot, isonums.pop(0))
                protsfrompepsgrpd[prot_iso] = isonums
                del(protsfrompepsgrpd[protroot])
        return dict(protsfrompepsgrpd)
    #
    grpprotsfrompeps = property(get_grpprotsfrompeps)

    def get_n_protsfrompeps(self):
        return len(self.protsfrompeps)
    #
    n_protsfrompeps = property(get_n_protsfrompeps)

    def get_ac_noisoform(self):
        """
        Returns the ACcession number without the isoform part:
          'P1234-2' -> 'P1234'
        """
        return self.ac.partition('-')[0]
    #
    ac_noisoform = property(get_ac_noisoform)

    def regex_format(self, pep_seq, regex_replace=AA2REGEX_PROTEOMICS):
        """
        Formating of `pep_seq` string according to `regex_replace` dictionary.
        """
        rkeys = regex_replace.keys()
        return ''.join( regex_replace[each] if each in rkeys else each
                        for each in pep_seq )


class GelImage(models.Model):
    """
    Image-Condition Class (related to a gel-experiment object)
    """

    twodgel = models.ForeignKey('TwoDGel', default=-1,
                                related_name='rel_gelimages')
    name = models.CharField(max_length=100, default='')
    hasjpg = models.BooleanField(default=False)
    dye = models.CharField(max_length=10, default='')
    condition = models.CharField(max_length=50, default='')
    slope_x = models.FloatField(default=0.0)        # x and y coordinates corrections
    intercept_x = models.FloatField(default=0.0)    # Are they necessary???
    slope_y = models.FloatField(default=0.0)        #
    intercept_y = models.FloatField(default=0.0)    #

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        if self._get_pk_val() and other._get_pk_val():
            equality = self._get_pk_val() == other._get_pk_val()
        else: #No primary key defined for the objects (for ex. when objects are not saved)
            equality = True #Don't take pk into account
        equality = equality and self.name == other.name
        equality = equality and self.twodgel_id == other.twodgel_id
        return equality

    def __unicode__(self):
        info = list()
        if self.pk: info.append('ID: {0}'.format(self.pk))
        if self.name:
            info.append('Image: {0}'.format(self.name))
        if self.twodgel_id:
            info.append('2D Gel ID: {0}'.format(self.twodgel_id))
        if self.dye:
            info.append('Dye: {0}'.format(self.dye))
        return ', '.join(info)

    def file_name(self):
        if self.hasjpg:
            return '{0}.jpg'.format(self.name)
        else:
            return None


class TwoDGel(models.Model):
    """
    Gel-Experiment Class
    """

    experiment = models.OneToOneField(Experiments, default=-1)
    dbconditions = models.TextField(default='')
    name = models.CharField(max_length=15)
#    gelimage = models.OneToOneField(GelImage, default=0,
#                                    related_name='rel_twodgel')
    #spot_set = backward relation to the spots found in a gel-experiment
    #gelimage_set = backward relation to the image-conditions conforming this gel-experiment

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        if self._get_pk_val() and other._get_pk_val():
            equality = self._get_pk_val() == other._get_pk_val()
        else: #No primary key defined for the objects (ex. objects not saved)
            equality = True #Don't take pk into account
        equality = equality and self.experiment_id == other.experiment_id
        equality = equality and self.gelimage_id == other.gelimage_id
        return equality

    def __unicode__(self):
        info = list()
        if self.pk: info.append('ID: {0}'.format(self.pk))
        if self.experiment_id:
            info.append('Experiment: {0}'.format(self.experiment_id))
        return ', '.join(info)

    def get_conditions(self):
        if self.dbconditions:
            return self.dbconditions.split('|')
        else:
            return None

    def set_conditions(self, dye_cond_list):
        if dye_cond_list:
            self.dbconditions = '|'.join(dye_cond_list)
            return self
        else:
            return None

    conditions = property(get_conditions, set_conditions)

    def nconditions(self):
        return len(self.conditions)


class Spot(models.Model):
    """
    Spot Class (related to a gel-experiment object, with proteins objects)
    """

    twodgel = models.ForeignKey(TwoDGel, default=-1)
    x = models.IntegerField(default=0)
    y = models.IntegerField(default=0)
    proteins = models.ManyToManyField(Proteins, through = 'Spots2Proteins')
    area = models.IntegerField(default=0)
    pi = models.FloatField(default=0.0)
    mw = models.FloatField(default=0.0)
    ratio_ab = models.FloatField(default=0.0)
    anova = models.FloatField(default=0.0)
    fold = models.FloatField(default=0.0)
    max_cv = models.FloatField(default=0.0)
    ingel_id = models.IntegerField(default=0)
    #spotextra_set = backward relation to the spotextra data for every image-condition of the gel-experiment

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        if self._get_pk_val() and other._get_pk_val():
            equality = self._get_pk_val() == other._get_pk_val()
        else: #No primary key defined for the objects (ex. objects not saved)
            equality = True #Don't take pk into account
        equality = equality and self.twodgel_id == other.twodgel_id
        equality = equality and self.x == other.x
        equality = equality and self.y == other.y
        return equality

    def __unicode__(self):
        info = list()
        if self.twodgel_id:
            info.append('2D Gel ID: {0}'.format(self.twodgel_id))
        if self.pk: info.append('Spot: {0}'.format(self.pk))
        if self.x or self.y:
            info.append('(x, y): ({0}, {1})'.format(self.x, self.y))
        return ', '.join(info)


class SpotExtra(models.Model):
    """
    SpotExtra Class related to a spot object and a image-condition object
    """

    spot = models.ForeignKey(Spot, default=0)
    gelimage = models.ForeignKey(GelImage, default=-1)
    volume = models.FloatField(default=0.0)
    norm_volume = models.FloatField(default=0.0)
    background = models.FloatField(default=0.0)
    peak_height = models.FloatField(default=0.0)

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        if self._get_pk_val() and other._get_pk_val():
            equality = self._get_pk_val() == other._get_pk_val()
        else: #No primary key defined for the objects (ex. objects not saved)
            equality = True #Don't take pk into account
        equality = equality and self.spot_id == other.spot_id
        equality = equality and self.gelimage_id == other.gelimage_id
        return equality

    def __unicode__(self):
        info = list()
        if self.spot_id:
            info.append('Spot ID: {0}'.format(self.spot_id))
        if self.gelimage_id:
            info.append('Image: {0}'.format(self.gelimage_id))
        if self.pk: info.append('ID: {0}'.format(self.pk))
        return ', '.join(info)


class Spots2Proteins(models.Model):
    spot = models.ForeignKey(Spot)
    protein = models.ForeignKey(Proteins)
    score_gelpmf = models.FloatField(default=0.0)

    def __unicode__(self):
        info = list()
        if self.pk:
            info.append('ID: {0}'.format(self.pk))
        if self.spot:
            info.append('SpotID: {0}'.format(self.spot.pk))
        if self.protein:
            info.append('ProteinID: {0}'.format(self.protein.ac))
        return ', '.join(info)


class Data(AbstractData):
    peptide = models.CharField(max_length=80, db_index=True, default='')
    experiment = models.ForeignKey(Experiments, default=-1)
    out_file = models.CharField(max_length=100, default='')
    raw_file = models.CharField(max_length=200, default='')
    scan_i = models.PositiveIntegerField(default=0)
    scan_f = models.PositiveIntegerField(default=0)
    ms_n = models.PositiveSmallIntegerField(default=2)
    rt = models.PositiveIntegerField(default=0)
    mass = models.FloatField(default=0.0)
    deltamass = models.FloatField(default=0.0)
    ions = models.CharField(max_length=10, default='0/0')
    charge = models.SmallIntegerField(default=0)
    pep_orig = models.CharField(max_length=200, default='')
    pep_score = models.FloatField(default=0.0)
    _proteins = models.ManyToManyField(Proteins, db_table='lymphos_data_proteins')
    xcorr = models.FloatField(default=0.0)
    deltacn = models.FloatField(default=0.0)
    d = models.FloatField(default=0.0)
    omssa = models.FloatField(default=0.0)
    phenyx = models.FloatField(default=0.0)
    peaks = models.FloatField(default=0.0)
    easyprot = models.FloatField(default=0.0)
    qdata = models.TextField(default='')
    spot = models.ForeignKey(Spot, default=-1)

    tmp_prots = list()

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        if self._get_pk_val() and other._get_pk_val():
            equality = self._get_pk_val() == other._get_pk_val()
        else: #No primary key defined for the objects (ex. objects not saved)
            equality = True #Don't take it into account
        equality = equality and self.raw_file == other.raw_file
        equality = equality and self.scan_i == other.scan_i
        equality = equality and self.scan_f == other.scan_f
        equality = equality and self.pep_orig == other.pep_orig
        equality = equality and self.phospho == other.phospho
        return equality

    def __hash__(self):
        hash_tuple = (self.raw_file, self.scan_i, self.scan_f, self.pep_orig,
                      self.phospho)
        return hash(hash_tuple)

    def __unicode__(self):
        info = list()
        if self.pep_orig: info.append('Peptide: {0}'.format(self.pep_orig))
        if self.pk: info.append('ID: {0}'.format(self.pk))
        return ', '.join(info)

    def get_qdata(self):
        if self.qdata:
            return map(float, self.qdata.split('|'))
        else:
            return None

    def set_qdata(self, qdata_list):
        if qdata_list:
            self.qdata = '|'.join(map(str, qdata_list))
            return self
        else:
            return None

    qdata_l = property(get_qdata, set_qdata)

    def get_proteins(self, is_decoy=False):
        return self._proteins.filter(is_decoy=is_decoy)

    def set_proteins(self, proteins):
        if proteins:
            self._proteins = proteins
            return self
        else:
            return None

    proteins = property(get_proteins, set_proteins)

    def get_decoys(self):
        return self.get_proteins(is_decoy=True)

    decoys = property(get_decoys)

    def get_proteins_w_decoys(self):
        return self._proteins

    proteins_w_decoys = property(get_proteins_w_decoys, set_proteins)


class DataQuant(models.Model):
    dbquanttimes = models.CharField(max_length=100, default='')
    dbratios = models.CharField(max_length=100, default='')
    dbstdv = models.CharField(max_length=100, default='')
    dbregulation = models.CharField(max_length=20, default='')
    data = models.OneToOneField(Data)
    quantpepexpcond = models.ForeignKey(QuantPepExpCond, default=-1)

    tmp_expcond_expgroup = ''

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        if self._get_pk_val() and other._get_pk_val():
            equality = self._get_pk_val() == other._get_pk_val()
        else: #No primary key defined for the objects (ex. objects not saved)
            equality = True #Don't take it into account
        equality = equality and self.data_id == other.data_id
        equality = equality and self.quantpepexpcond_id == other.quantpepexpcond_id
        return equality

    def __unicode__(self):
        info = list()

        if self.data: info.append('Peptide: {0}'.format(self.data_id))
        if self.reg_values: info.append('Reg.: {0}'.format(self.reg_values))
        if self.pk: info.append('ID: {0}'.format(self.pk))
        return ', '.join(info)

    def get_quanttimes(self):
        if self.dbquanttimes:
            return map( int, self.dbquanttimes.split('|') )
        else:
            return None
    def set_quanttimes(self, quanttimes_list):
        if quanttimes_list:
            self.dbquanttimes = '|'.join( map(str, quanttimes_list) )
            return self
        else:
            return None
    quanttimes = property(get_quanttimes, set_quanttimes)

    def get_ratios(self):
        if self.dbratios:
            return map( float, self.dbratios.split('|') )
        else:
            return None
    def set_ratios(self, ratios_list):
        if ratios_list:
            self.dbratios = '|'.join( map(str, ratios_list) )
            return self
        else:
            return None
    ratios = property(get_ratios, set_ratios)

    def get_stdv(self):
        if self.dbstdv:
            return map( float, self.dbstdv.split('|') )
        else:
            return None
    def set_stdv(self, stdv_list):
        if stdv_list:
            self.dbstdv = '|'.join( map(str, stdv_list) )
            return self
        else:
            return None
    stdv = property(get_stdv, set_stdv)

    def get_reg_values(self):
        if self.dbregulation:
            return [ [ int(reg) ] for reg in self.dbregulation.split('|') ]
        else:
            return None
    def set_reg_values(self, regulation_list):
        if regulation_list:
            self.dbregulation = '|'.join( map(str, regulation_list) )
            return self
        else:
            return None
    reg_values = property(get_reg_values, set_reg_values)

    def time_values(self):
        """
        :return : a dictionary with the following elements:
            { time: ( [regulation], ratio, stdv ) }
        """
        return dict( zip(self.quanttimes, zip(self.reg_values,
                                              self.ratios,
                                              self.stdv)) )


class Spectra(models.Model):
    data = models.ForeignKey(Data)
    ms_n = models.PositiveSmallIntegerField(default=2)
    mass = models.TextField(default='')
    intensity = models.TextField(default='')

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        if self._get_pk_val() and other._get_pk_val():
            equality = self._get_pk_val() == other._get_pk_val()
        else: #No primary key defined for the objects (ex. objects not saved)
            equality = True #Don't take it into account
        equality = equality and self.ms_n == other.ms_n
        equality = equality and self.data_id == other.data_id
        return equality

    def __unicode__(self):
        info = list()
        if self.data: info.append('Peptide: {0}'.format(self.data_id))
        if self.ms_n: info.append('MS{0}'.format(self.ms_n))
        if self.pk: info.append('ID: {0}'.format(self.pk))
        return ', '.join(info)

    @property
    def mz_as_strings(self):
        return self.mass.split('|')

    @property
    def intensity_as_strings(self):
        return self.intensity.split('|')

    @property
    def x_mz(self):
        return map(float, self.mz_as_strings)

    @property
    def y_intensity(self):
        return map(float, self.intensity_as_strings)


class Users(models.Model):
    dbemail = models.CharField(primary_key=True, max_length=140) #e-mails (pk) are stored as double derivated hashes: kdf_sha256() -> pbkdf2_hmac()
    dbpasswd = models.CharField(max_length=350, default='') #Passwords are stored as encrypted derivated hashes: kdf_sha256() -> _encrypt_dbfield()
    dbpermissions = models.CharField(max_length=140, default='') #Encrypted permissions.
    dbdigest = models.CharField(max_length=400) #Encrypted e-mail digest.
    singup_date = models.DateTimeField(auto_now_add=True) #Creation datetime (update every new record is created).
    last_login = models.DateTimeField(auto_now=True) #Update every time it's saved.

    def __unicode__(self):
        return self.pk

    def _encrypt_dbfield(self, clear_text):
        # Add "salt" and randomization ("first encryption") of `clear_text`:
        text_len = len(clear_text)
        pos = random.randint(1, text_len - 1)
        salt0 = get_random_data(pos).replace('\x8b','\xfa')
        salt2 = get_random_data(text_len - pos).replace('\xfa','\x8b')
        saltedrnd_text = '%s\x8b%s\xfa%s'%(salt0,
                          xor_encrypt(clear_text, salt0 + salt2)[0][::-1],
                          salt2)
        saltedrndtext_len = len(saltedrnd_text)
        # Get a random cipher key that is associated to this object (to its `pk`):
        random.seed()
        sk_start = random.randint(2, 10)
        sk_end = random.randint(sk_start + 21, 48)
        salt = get_random_data( random.randint(15, 26) )[::-1]
        rnd_iter = random.randint(50000, 60000)
        deriv_key = pbkdf2_hmac('sha512', ( self.pk.decode('hex') +
                                settings.SECRET_KEY[sk_start:sk_end] ),
                                salt[::-1] + settings.PEPPER, rnd_iter,
                                dklen=saltedrndtext_len)
        cipher_key, rnd_key = xor_encrypt(deriv_key)
        # Encrypt the randomized salted `clear_text`:
        encrypted_text = xor_encrypt(saltedrnd_text, cipher_key)[0][::-1]
        # Pack the encrypted text and other data into a base64 string:
        pack0 = (salt + str(sk_end * 100 + sk_start) + rnd_key).encode('base64')
        pack1 = (str(rnd_iter) + encrypted_text).encode('base64')
        return "-".join( (pack0, pack1) )[::-1].encode('base64').swapcase()

    def _decrypt_dbfield(self, encrypted_text):
        # Unpack data from a base64 string:
        pack0, pack1 = encrypted_text.swapcase().decode('base64')[::-1].split('-')
        pack1 = pack1.decode('base64')
        encryptedtext_len = len( pack1[5:] )
        pack0 = pack0.decode('base64')
        salt = pack0[:-encryptedtext_len-4]
        sk_endstart = pack0[-encryptedtext_len-4:-encryptedtext_len]
        rnd_key = pack0[-encryptedtext_len:]
        # Reconstruction of the random cipher key:
        sk_slice = slice( int( sk_endstart[-2:] ), int( sk_endstart[:2] ) )
        deriv_key = pbkdf2_hmac( 'sha512', ( self.pk.decode('hex') +
                                 settings.SECRET_KEY[sk_slice] ),
                                 salt[::-1] + settings.PEPPER, int( pack1[:5] ),
                                 dklen=len(rnd_key) )
        cipher_key = xor_encrypt(deriv_key, rnd_key)[0]
        # Decrypt the salted `clear_text`:
        saltedrnd_text = xor_decrypt(pack1[5:][::-1], cipher_key)
        # Get rid of "salt" and de-randomization:
        salt0, halfsaltedrnd_text = saltedrnd_text.split('\x8b', 1)
        randomized_text, salt2 = halfsaltedrnd_text.rsplit('\xfa', 1)
        return xor_decrypt(randomized_text[::-1], salt0 + salt2)

    @staticmethod
    def _get_email_hash(clear_email):
        """
        Do hexadecimal hash of ``APPNAME`` + plain-text e-mail, ussing
        :func:`kdf_sha256`.
        """
        return kdf_sha256( settings.APPNAME + clear_email, 10000 )

    def _get_passwd_hash(self, clear_passwd):
        """
        Do hexadecimal hash of e-mail hash + plain-text password, ussing
        :func:`kdf_sha256`.
        """
        return kdf_sha256(self._get_email_hash(self.clear_email) + clear_passwd,
                          49000)

    def get_email(self):
        return self.dbemail
    def set_email(self, clear_email, min_len=6):
        if (isinstance( clear_email, (str, unicode) ) and '.' in clear_email[:-2]
            and clear_email.count('@')==1 and len(clear_email)>=min_len):
            
            if not self.dbpermissions:
                permissions = 0
            else:
                permissions = self.permissions
            self.dbemail = pbkdf2_hmac('sha256',
                                       self._get_email_hash(clear_email),
                                       settings.PEPPER, 90000).encode('hex')
            self.dbdigest = self._encrypt_dbfield(clear_email)
            self.permissions = permissions
            rnd_passwd = get_random_data(5).encode('hex')
            self.passwd = rnd_passwd
            print("CAUTION! New Password set to: %s"%rnd_passwd)
        else:
            raise ValueError('Invalid e-mail!')
    email = property(get_email, set_email)

    def get_passwd(self):
        return self._decrypt_dbfield(self.dbpasswd).encode('hex')
    def set_passwd(self, clear_passwd, min_len=6):
        if isinstance( clear_passwd, (str, unicode) ) and len(clear_passwd)>=min_len:
            self.dbpasswd = self._encrypt_dbfield( self._get_passwd_hash(clear_passwd).decode('hex') )
        else:
            raise ValueError('Invalid string Password!')
    passwd = property(get_passwd, set_passwd)

    def get_clear_email(self):
        return self._decrypt_dbfield(self.dbdigest)
    clear_email = property(get_clear_email, set_email)

    def get_permissions(self):
        return int( self._decrypt_dbfield(self.dbpermissions)[2:] )
    def set_permissions(self, value):
        if not isinstance(value, int):
            raise ValueError('Permissions should be an integer!')
        self.dbpermissions = self._encrypt_dbfield("29%s"%value)
    permissions = property(get_permissions, set_permissions)

    def check_passwd(self, othr_passwd, ots_uuid=None, othr_hashed=False):
        if not othr_hashed: #Comparisson password is in clear text (possibly because user browser has not javascript):
            othr_passwd_hash = self._get_passwd_hash(othr_passwd)
            return othr_passwd_hash == self.passwd
        elif ots_uuid: #Comparisson password is a hexadecimal hash, and One-Time-Salt used:
            passwd_2ndhash = kdf_sha256("/file/images/padlock{0}.png".format(ots_uuid) +
                                        self.passwd, 1000) #Do second hash of One-Time-Salt + user hashed password, ussing :func:`kdf_sha256`.
            return othr_passwd == passwd_2ndhash
        else: #Comparisson password is a hexadecimal hash, but NO One-Time-Salt used:
            return othr_passwd == self.passwd


class OTS(models.Model):
    """
    Time-limited One-Time-Salts.
    """
    session = models.CharField(primary_key=True, max_length=32, default=lambda: uuid4().hex) #Session random ID.
    uuid = models.CharField( max_length=36, default=lambda: str( uuid4() ) ) #One-Time-Salt.
    date = models.DateTimeField(db_index=True, auto_now_add=True) #Update every new record is created.

    def __unicode__(self):
        info = [self.uuid]
        if self.pk: info.append("Session: {0}".format(self.pk))
        if self.date: info.append("Date: {0}".format(self.date))
        return ', '.join(info)


class EventType(models.Model):
    name = models.CharField(max_length=15, default='')


class Events(models.Model):
    eventtype = models.ForeignKey(EventType)
    date = models.DateTimeField(auto_now_add=True)
    value = models.TextField(default='')
    subevents = models.ManyToManyField('self')


class Version(models.Model):
    db_version = models.CharField(max_length=50, default='')
    version_date = models.DateTimeField(auto_now_add=True)

