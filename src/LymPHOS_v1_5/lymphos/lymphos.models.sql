BEGIN;
CREATE TABLE `lymphos_conditions` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `name` varchar(100) NOT NULL,
    `miape_id` varchar(50) NOT NULL,
    `description` varchar(180) NOT NULL,
    `electrophoresis` varchar(300) NOT NULL,
    `protocol_name` varchar(300) NOT NULL,
    `protocol_description` longtext NOT NULL,
    `protocol_1d` longtext NOT NULL,
    `protocol_2d` longtext NOT NULL,
    `protocol_iterdimension` longtext NOT NULL,
    `directdetection` varchar(300) NOT NULL,
    `instrument` varchar(150) NOT NULL,
    `quant_cond` longtext NOT NULL,
    `label` varchar(10) NOT NULL
)
;
CREATE TABLE `lymphos_proteins` (
    `ac` varchar(25) NOT NULL PRIMARY KEY,
    `up_id` varchar(25) NOT NULL,
    `up_ver` integer UNSIGNED NOT NULL,
    `name` varchar(300) NOT NULL,
    `function` longtext NOT NULL,
    `location` varchar(200) NOT NULL,
    `seq` longtext NOT NULL,
    `up_psites` longtext NOT NULL
)
;
CREATE TABLE `lymphos_quantpepexpcond` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `unique_pep` varchar(80) NOT NULL,
    `experiment` varchar(200) NOT NULL,
    `condition_id` integer NOT NULL,
    `average` double precision NOT NULL,
    `stdev` double precision NOT NULL
)
;
ALTER TABLE `lymphos_quantpepexpcond` ADD CONSTRAINT `condition_id_refs_id_61126d99` FOREIGN KEY (`condition_id`) REFERENCES `lymphos_conditions` (`id`);
CREATE TABLE `lymphos_data_proteins` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `data_id` integer NOT NULL,
    `proteins_id` varchar(25) NOT NULL,
    UNIQUE (`data_id`, `proteins_id`)
)
;
ALTER TABLE `lymphos_data_proteins` ADD CONSTRAINT `proteins_id_refs_ac_6aafcd23` FOREIGN KEY (`proteins_id`) REFERENCES `lymphos_proteins` (`ac`);
CREATE TABLE `lymphos_data` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `condition_id` integer NOT NULL,
    `out_file` varchar(100) NOT NULL,
    `raw_file` varchar(200) NOT NULL,
    `scan_i` integer UNSIGNED NOT NULL,
    `scan_f` integer UNSIGNED NOT NULL,
    `ms_n` smallint UNSIGNED NOT NULL,
    `rt` integer UNSIGNED NOT NULL,
    `mass` double precision NOT NULL,
    `deltamass` double precision NOT NULL,
    `ions` varchar(10) NOT NULL,
    `charge` smallint NOT NULL,
    `pep_orig` varchar(200) NOT NULL,
    `peptide` varchar(80) NOT NULL,
    `pep_score` double precision NOT NULL,
    `psites` varchar(20) NOT NULL,
    `ascores` varchar(85) NOT NULL,
    `prot_n` smallint UNSIGNED NOT NULL,
    `xcorr` double precision NOT NULL,
    `deltacn` double precision NOT NULL,
    `d` double precision NOT NULL,
    `omssa` double precision NOT NULL,
    `phenyx` double precision NOT NULL,
    `qdata` longtext NOT NULL,
    `quant_pep_cond_id` integer NOT NULL
)
;
ALTER TABLE `lymphos_data` ADD CONSTRAINT `condition_id_refs_id_738d46c7` FOREIGN KEY (`condition_id`) REFERENCES `lymphos_conditions` (`id`);
ALTER TABLE `lymphos_data` ADD CONSTRAINT `quant_pep_cond_id_refs_id_18c48a3b` FOREIGN KEY (`quant_pep_cond_id`) REFERENCES `lymphos_quantpepexpcond` (`id`);
ALTER TABLE `lymphos_data_proteins` ADD CONSTRAINT `data_id_refs_id_2c574595` FOREIGN KEY (`data_id`) REFERENCES `lymphos_data` (`id`);
CREATE TABLE `lymphos_spectra` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `data_id` integer NOT NULL,
    `ms_n` smallint UNSIGNED NOT NULL,
    `mass` longtext NOT NULL,
    `intensity` longtext NOT NULL
)
;
ALTER TABLE `lymphos_spectra` ADD CONSTRAINT `data_id_refs_id_7012281c` FOREIGN KEY (`data_id`) REFERENCES `lymphos_data` (`id`);
CREATE TABLE `lymphos_users` (
    `mail` varchar(240) NOT NULL PRIMARY KEY,
    `passwd` varchar(24) NOT NULL,
    `permissions` smallint UNSIGNED NOT NULL,
    `singup_date` datetime NOT NULL,
    `last_logging` datetime NOT NULL
)
;
CREATE INDEX `lymphos_quantpepexpcond_115412aa` ON `lymphos_quantpepexpcond` (`condition_id`);
CREATE INDEX `lymphos_data_115412aa` ON `lymphos_data` (`condition_id`);
CREATE INDEX `lymphos_data_255197ac` ON `lymphos_data` (`quant_pep_cond_id`);
CREATE INDEX `lymphos_spectra_2e8ca968` ON `lymphos_spectra` (`data_id`);
COMMIT;
