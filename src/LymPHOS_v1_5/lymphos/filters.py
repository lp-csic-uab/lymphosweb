# -*- coding: utf-8 -*-
"""
:synopsis: Classes used to import data into a LymPHOS DataBase.

:created:    2011/03/08

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2011-2013 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '2.0'
__UPDATED__ = '2015-11-09'


#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

# Application imports:
from models import *
from logic import SearchDB
from utilities import LoggerCls

# Python core imports:
import re
import os
#import urllib2
import xml.etree.cElementTree   #X-NOTE: Faster C version imported
import json
import csv
import math
from collections import defaultdict
from zipfile import ZipFile, BadZipfile


#===============================================================================
# Importer/converter class definitions
#===============================================================================
class Converter(object):
    """
    Abstract class.
    Some file -> Database model object/s
    """
    # Class Attributes. Supported converters:
    JSON_VISOR = 1
    JSON_COND = 2
#    XML_UPROT = 3
    JSON_QUANT = 4
    ZIP_TWODGEL = 5
    CSV_SAMESPOTS = 6
    CSV_PMF = 7
    JPG_IMG = 8
    DANTER_DB = 9
    
    def __init__(self, in_data=None, in_file=None, zipped=True, extra_data=None):
        """
        :param str in_file: filename of the file with the data to be imported.
        :param object in_data: object with the data to be imported.
        :param bool zipped: set to True if `in_file` is zipped, else, False.
        Defaults to True.
        :param object extra_data: extra data needed for the import data process.
        
        Public Instance Attributes&Properties:             
        :attr str file: filename of the file with the data to be imported.
        :attr object data: object containing the data to be imported.
        :attr bool zipped: True if self.file is zipped, else, False.
        :attr LoggerCls logger: dictionary like object of errors&logs generated 
        during the import process.
        """
        self.data = in_data #Data to convert&import into LymPHOS
        self.file = in_file #File uploaded by the user to convert&import (may contain other files if it's a Zip archive)
        self.zipped = zipped
        self.extra_data = extra_data
        #
        self.ALLOWD_EXT = tuple() #Allowed extensions of the files to import
        self.CONVERTER = dict() #Converter/translation dictionary ('file fields/row/keys/tags' -dict()-> 'database fields')
        self.logger = LoggerCls() #Object for errors&logs generated during the import process.
    
    def load_data(self, io_file):
        # Implement load_data stuff in Subclasses!
        
        # The load_data stuff is Subclass Responsibility
        raise NotImplementedError('Override this method to add load_data '\
                                  'functionality to this Converter')
        # Recommended: return self.data

    def import_data(self, in_data=None):
        if in_data: self.data = in_data
        
        # Implement real import_data stuff in Subclasses!
        
        # The rest of import_data stuff is Subclass Responsibility

        # Recommended: return self
        return self

    def process_file(self, in_file=None):
        """
        #X-NOTE: restructured to read all the files in a .zip file as well as 
        #a single uncompressed file.
        For every file, open file, load data from file, close file, and import
        data into database.
        """
        if in_file: self.file = in_file
        #
        if self.zipped: # File is Zip Compressed:
            try:
                zfile = ZipFile(self.file, 'r')
                in_files = zfile.namelist()
            except IOError as exception:
                zip_error_msg = exception.strerror + ' : %s' % exception.filename
                self.logger.put_with_key('IO Error', zip_error_msg)
                raise exception #TO_DO: Fail more gracefully.
            except BadZipfile as exception:
                zip_error_msg = exception.args[0]
                self.logger.put_with_key('Zip Error', zip_error_msg)
                raise exception #TO_DO: Fail more gracefully.
            #
            open_method = zfile.open
            open_mode = 'rU' #In zipfiles the open always is binary (#X-NOTE: http://my.safaribooksonline.com/book/programming/python/0596001673/files/pythoncook-chp-4-sect-11)
        else: # File is not Compressed:
            in_files = [self.file]
            open_method = open
            open_mode = 'rbU'
        # Filter the list of input files accordingly with the extensions allowed
        # for the current data type, and convert it to a dictionary whose keys
        # are the file extensions allowed, so we can access files in the order
        # they appear in ``self.ALLOWD_EXT``:
        in_filesbyext = defaultdict(list)
        for in_file in in_files:
            ext = in_file.rpartition('.')[2].upper()
            if ext in self.ALLOWD_EXT:
                in_filesbyext[ext].append(in_file)
        # Load and import each input file, in the same order they appear in 
        # ``self.ALLOWD_EXT``:
        for ext in self.ALLOWD_EXT:
            for in_file in in_filesbyext[ext]: #X-NOTE: If ext is not a valid key in in_filesbyext, it doesn't crash, because in_filesbyext is a defaultdict(list) and thus return a empty list for non-valid key access
                try:
                    io_file = open_method(in_file, open_mode) #Open I/O stream #TEST: 'rb' -> 'r'
                except IOError as exception:
                    io_error_msg = exception.strerror + ' : %s' % exception.filename
                    self.logger.put_with_key('IO Error', io_error_msg)
                    raise exception #TO_DO: Fail more gracefully.
                except BadZipfile as exception:
                    zip_error_msg = exception.args[0]
                    self.logger.put_with_key('Zip Error', zip_error_msg)
                    raise exception #TO_DO: Fail more gracefully.
                # Tell the converter to load the data from the I/O stream:
                self.data = self.load_data(io_file)
                io_file.close()
                # Import into the database the data loaded on memory:
                self.import_data()
        #
        if self.zipped: zfile.close()
        #
        return self


class JsonConverter(Converter):
    """
    Abstract class
    JSON file -> Database model object
    """
    def load_data(self, io_file):
        self.data = json.load(io_file)
        return self.data
    
    def _process_json_key(self, json_key, field_conv, model_obj, 
                           json_branch=None, extra_data=None):
        if not json_branch:
            json_branch = self.data[json_key]
        #
        for field, params in field_conv.iteritems():
            jpath_t = params[0]
            if not jpath_t: # Special processing (no searching in JSon):
                jvalue = {'file': self.file.replace('.zip',''), 
                          'key': json_key, 
                          'extra': extra_data}
            else:
                jvalue = json_branch
                # For every sub-PATH key in the PATH tuple of keys for the field
                # value in the JSon sub-dictionary:
                for jsubpath in jpath_t:
                    jvalue_keys = jvalue.keys()
                    if jsubpath in jvalue_keys: # The sub-PATH key is in JSon sub-dictionary:
                        jvalue = jvalue[jsubpath]
                    # There are various possible valid sub-PATH keys ('OR keys':
                    # key1 OR key2 OR ...) in the JSon sub-dictionary for the
                    # current database field value?:
                    elif isinstance(jsubpath, tuple):
                        tmp_jvalue = None #Filter sub-PATH keys in tuple not found in JSon sub-dictionary.
                        for forkey in jsubpath:
                            if forkey in jvalue_keys and not tmp_jvalue:
                                tmp_jvalue = jvalue[forkey]
                                break
                        jvalue = tmp_jvalue
                        if not jvalue: break
                    else: # Filter sub-PATH key not found in JSon sub-dictionary:
                        jvalue = None
                        break

            format_funct = params[-1]
            if jvalue and format_funct:
                jvalue = format_funct(jvalue)
            if jvalue:
                setattr(model_obj, field, jvalue)
        return model_obj


class CSVConverter(Converter):
    """
    Abstract class
    CSV file -> Database model object
    """
    def load_data(self, io_file):
        self.data = list()
        csvreader = csv.reader(io_file)
        for row in csvreader:
            self.data.append(row)
        return self.data    
        

class JsonCondConverter(JsonConverter):
    """
    JSON Conditions file -> Experiments and ExpGroup model objects Imported
    
    Imported ExpGroup model objects update previous model objects in the
    database with the same id ('pk'). Imported Experiments model objects replace
    previous model objects in the database with the same id ('pk').
    
    self.CONVERTER dictionary to Experiments and ExpGroup Model objects (keys in
    the first level) is used, where keys in the second level represent each
    field of the Model, and the values are tuples, whose position [0]
    corresponds to the "key/PATH" (a new n-keys tuple) in the dictionary tree of
    the JSON file. The tuples also include a function object in position [-1] to
    process the data obtained from the JSON dictionary and convert it to the
    expected data format in the Model's field.
    """
    def __init__(self, in_data=None, in_file=None):
        super(JsonCondConverter, self).__init__(in_data, in_file)
        #
        self.ALLOWD_EXT = ('DB',)
        self.CONVERTER = {
                  'Experiments':{'pk': (None,
                                        lambda d: int(d['key'])),                                
                                 'expgroup_id': ((u'type_id',), 
                                                 int),
                                 'dbsub_exps': ((u'sub_id',), 
                                                None),
                                 'name': ((u'name',), 
                                          None),
                                 'dbmiape_ids': ((u'miapes',),
                                                 None),
                                 'description': ((u'description',),
                                                 None),
                                 'instrument': ((u'instrument',), 
                                                None),
                                 'quant_cond': ((u'quant',),
                                                None),
                                 'label': ((u'label',),
                                           None),
                                 },
                  'ExpGroup':   {'pk': ((u'type_id',), 
                                        int),
                                 'name': ((u'description',), 
                                          None),
                                 'description': ((u'name',),
                                                 None),
                                 },
        }
    
    def import_data(self, in_data=None):
        """
        Imports Experimental Design into Experiments and ExpGroup models/tables
        from a JSON file.
        """
        # TO_DO: prepare new Condition JSon with information about supra-
        # experiments (expgrous).
        super(JsonCondConverter, self).import_data(in_data)
        #
        # Pointers to the conversion dict-filters to use:
        exp_conv = self.CONVERTER['Experiments']
        expgroup_conv = self.CONVERTER['ExpGroup']
        # Get ALL current ExpGroup records in the DataBase:
        db_expgroups = set( SearchDB().st_expgroup().get_query() )
        # Default dict for information grouping about ExpGroup records:
        expgroup2names_descs = defaultdict( lambda: { 'names': set(), 
                                                      'descriptions': set() } )
        for json_key in self.data:
            experiment_record = self._process_json_key(json_key, exp_conv, 
                                                       Experiments())
            expgroup_record = self._process_json_key(json_key, expgroup_conv, 
                                                     ExpGroup())
            if expgroup_record not in db_expgroups: #Create new record in DataBase
                expgroup_record.save()
                db_expgroups.add(expgroup_record)
            # Collect all names and descriptions for the same ExGroup record:
            names_descs = expgroup2names_descs[expgroup_record]
            names_descs['names'].add(expgroup_record.name)
            names_descs['descriptions'].add(expgroup_record.description)
            # Create ForeingKey relation between experiment and expgroup:
            experiment_record.expgroup = expgroup_record
            #
            experiment_record.save()
        # Update ExGroup records with all its names and descriptions:
        for exp_group, names_descs in expgroup2names_descs.items():
            exp_group.name = '; '.join(sorted( names_descs['names'] ))
            exp_group.description = '; '.join(sorted( names_descs['descriptions'] ))
            exp_group.save(force_update=True)
        #
        return self


class JsonQuantConverter(JsonConverter):
    """
    JSON PQuantifier file -> QuantPepExpCond and DataQuant model records.
    Only T-test statistical information calculated in PQuantifier is imported
    into QuantPepExpCond records. For importation of DanteR statistical 
    information, :class:`DanteRDBConverter` should be used.
    
    :caution: During the importation all previous QuantPepExpCond and DataQuant
    model objects in datababase are DELETED!
    
    self.CONVERTER conversion dictionary to QuantPepExpCond and DataQuant Model
    objects (keys in the first level) is used, where keys in the second level
    represent each field of the Model, and the values are tuples, whose position
    [0] corresponds to the "key/PATH" (a new n-keys tuple) in the dictionary
    tree of the JSON file. The tuples also include a function object in position
    [-1] to process the data obtained from the JSON dictionary and convert it to
    the expected data format in the Model's field.
    """
    # X-NOTE: for now, no update is possible!
    def __init__(self, in_data=None, in_file=None):
        """
        Constructor
        """
        super(JsonQuantConverter, self).__init__(in_data, in_file)
        #
        self.ALLOWD_EXT = ('JSON',)
        self.CONVERTER = {
                  'QuantPepExpCond':{'unique_pep': (None,
                                                    lambda d: d['extra']),                                
                                     'expgroup_id': (None,
                                                     lambda d: int(d['key'])),
                                     'quanttimes': ((u'stages',),
                                                    lambda l: l[1:]),
                                     'ratios': ((u'values',),
                                                lambda l: [r[0] for r in l]),
                                     'stdv': ((u'values',),
                                              lambda l: [s[1] for s in l]),
                                     'reg_values': ((u'flag_signif',),
                                                    None),
                                     'ttestpassed95': ((u'ttestpassed95',),
                                                       None),
                                     'ttestpassed99': ((u'ttestpassed99',),
                                                       None),
                                     'sdgreatersdmeanx3': ((u'warn_sdmeanx3',),
                                                           None),
                                     },
                  'DataQuant':   {'quanttimes': ((u'stages',),
                                                 lambda l: l[1:]),
                                  'ratios': ((u'values',),
                                             lambda l: [r[0] for r in l]),
                                  'stdv': ((u'values',),
                                           lambda l: [s[1] for s in l]),
                                  'reg_values': ((u'flag_signif',),
                                                 None),
                                  'tmp_expcond_expgroup': ((u'average',),
                                                           None),
                                  },
        }
    
    def fmt_pep(self, pep, pep_orig):
        """
        Locates phospho-sites in a peptide pep_orig to format peptide pep, and
        returns the formated peptide
        """
        pos = 0
        fmt_pep = list(pep)
        aa_mod = re.split('[\\)\\(], ', pep_orig)
        for each in aa_mod:
            if each.isdigit():
                i_each = int(each)
                if i_each in (21, 23):
                    psite = pos - 1
                    fmt_pep[psite] = fmt_pep[psite].lower()
            else:
                pos += len(each)
        return ''.join(fmt_pep)        
    
    def import_data(self, in_data=None):
        """
        Imports Quantitative Analysis Data into QuanPepExpCond and DataQuant
        models/tables from a JSON file generated by the PQuantifier Statistical
        Python Program.
        """
        #
        super(JsonQuantConverter, self).import_data(in_data)
        #
        # Pointers to the conversion dict-filters to use:
        qpepcond_conv = self.CONVERTER['QuantPepExpCond']
        dataq_conv = self.CONVERTER['DataQuant']
        # DELETE previous stored data!! (X-NOTE: for now, no update is possible!):
        QuantPepExpCond.objects.filter(pk__gt=0).delete() #Don't delete null record.
        DataQuant.objects.all().delete()
        # Process & Import new data:
        for json_seq in self.data:
            qpepcond_records = dict()
            # Get data from 'average' into QuantPepExpCond records:
            for json_key in self.data[json_seq]['average']:
                json_branch = self.data[json_seq]['average'][json_key]
                qpepcond_record = self._process_json_key(json_key, 
                                                         qpepcond_conv, 
                                                         QuantPepExpCond(), 
                                                         json_branch, 
                                                         json_seq)
                #
                qpepcond_record.save()
                # Store the QuantPepExpCond records for the current json_seq in
                # a dict, so they can be used in relationships with
                # corresponding DataQuant records:
                qpepcond_records[ int(json_key) ] = qpepcond_record
            # Get data from 'partials' into DataQuant records:
            for l_scans in self.data[json_seq]['partials'].itervalues():
                for d_scan in l_scans:
                    dataq_record = self._process_json_key(None, 
                                                          dataq_conv, 
                                                          DataQuant(), 
                                                          d_scan)
                    # Create OneToOne relation with a Data record:
                    raw_file = d_scan['file'] + '.raw'
                    scan_i = d_scan['scan']
                    data_record = Data.objects.get(raw_file=raw_file, 
                                                   scan_i=scan_i)
                    dataq_record.data = data_record
                    # Create OneToMany relation with a QuantPepExpCond record:
                    qpepcond_record = qpepcond_records[dataq_record.tmp_expcond_expgroup]
                    dataq_record.quantpepexpcond = qpepcond_record
                    #
                    dataq_record.save()
        return self


class JsonIntegratorConverter(JsonConverter):
    """
    JSON Integrator file -> Data, Protein and Spectra model objects Imported
    
    Imported Data and Spectra model objects are added to the database with new
    ids ('pk'). Imported Protein model objects update previous model objects in
    the database with the same id ('ac').
    
    self.CONVERTER conversion dictionary to Data and Spectra Model objects (keys
    in the first level) is used, where keys in the second level represent each
    field of the Model, and the values are tuples, whose position [0]
    corresponds to the "key/PATH" (a new n-keys tuple) in the dictionary tree of
    the JSON file. The tuples also include a function object in position [-1] to
    process the data obtained from the JSON dictionary and convert it to the
    expected data format in the Model's field.
    """
    def __init__(self, in_data=None, in_file=None, zipped=True, 
                 extra_data=None, only_phospho=False, with_decoys=True, 
                 iterative=False):
        """
        :param bool only_phospho: Imports only phospho-peptides (True) or all
        peptides (False, default). Defaults to import all peptides
        (only_phospho=False) so this changes the previous interface of the
        function.
        :param bool with_decoys: Imports all proteins (True, default) or only
        non-decoy proteins (False).
        :param bool iterative: uses :method:`self.iter_load_data` (True) or
        :method:`self.load_data` (False, default) to load JSON data into memory
        and process it.
        """
        super(JsonIntegratorConverter, self).__init__(in_data, in_file, zipped, 
                                                      extra_data)
        #
        self.ALLOWD_EXT = ('DB', 'IDB') #X-NOTE: IDB -> "Integrator DataBase".
        self.CONVERTER = {
                  'Data':{
                          'experiment_id': (('experiment',), 
                                            int),
                          'out_file': (None, 
                                       lambda d: os.path.split(d['file'])[-1]),
                          'raw_file': (None, 
                                       lambda d: d['key'].split('.', 1)[0] + 
                                                 '.raw'),
                          'scan_i': (None, 
                                     lambda d: int(d['key'].split('.')[1])),
                          'scan_f': (None, 
                                     lambda d: int(d['key'].split('.')[-1])),
                          'ms_n': (('ms',), 
                                   int), 
                          'rt': ( (('Phenyx','phenyx'),'rt'), #FIXME: Needs consensus data.
                                  int ),
                          'mass': ( (('Sequest','sequest'), 'actual_mass',), #FIXME: Needs consensus data.
                                    float ),
                          'deltamass': (('consensus_deltamz',), 
                                        float),
                          'ions': ( (('Sequest','sequest'), 'ions'), #FIXME: Needs consensus data.
                                    None ),
                          'charge': ( ('consensus_z',), 
                                      lambda s: int(float(s)) ), #X-NOTE: needed to handle float data from EasyProt.
                          'pep_orig': (None, 
                                       self.find_pep_orig),
                          'peptide': (('consensus',), 
                                      None),
                          'pep_score': (('ascore_data', 'peptide_score'), 
                                        float),
                          'dbpsites': (None, 
                                       self.find_psites),
                          'dbascores': (('ascore_data', 'ascore'), 
                                        lambda l: '|'.join(map(str, l))),
                          'tmp_prots': (('consensus_proteins',), 
                                        lambda l: [Proteins(ac=each) for each in l.split('|') if self.with_decoys or (not self.with_decoys and 'decoy' not in each)]),
                          'xcorr': ( (('Sequest','sequest'), 'xcorr'), 
                                     float),
                          'deltacn': ( (('Sequest','sequest'), 'deltacn'), 
                                       float),
                          'd': ( (('Sequest','sequest'), 'd'), 
                                 float ),
                          'omssa': ( (('Omssa','omssa'), 'pvalue'), 
                                     float ),
                          'phenyx': ( (('Phenyx','phenyx'), 'zscore'), 
                                      float ),
                          'peaks': ( (('Peaks','peaks'), 'logp'), 
                                     float ),
                          'easyprot': ( (('Easyprot','easyprot'), 'zscore'), 
                                        float ),
                          'qdata': ( None,
                                     self.find_qdata ),
                          'spot': (None,
                                   lambda d: d['extra'].get( int(d['key'].split('_')[1]) ) 
                                             if d['extra'] else None ),
                          },

                  'Spectra':{
                             'ms_n': (('ms',), 
                                      int), 
                             'mass': (('spectral_info', 'mass_array'), 
                                      None),
                             'intensity': (('spectral_info', 'intensity_array'), 
                                           None),
                             },
        }
        #
        self.prot_imp = UniProtXmlConverter(db_file=settings.UP_ZIP_DB) #The protein importer filter object to use.
        self.saved_prots = list() #X-NOTE: The possible information lost in a new import after a transaction roll-back was Fixed by working on a copy (see :method:`self._import_jsonv`).
        self.only_phospho = only_phospho #Imports only phospho-peptides (True) or all peptides (False).
        self.with_decoys = with_decoys #Imports all proteins (True) or only non-decoy proteins (False).
        self.iterative = iterative
    
    @staticmethod
    def _quant_field(pep_orig):
        if '737' in pep_orig:
            return 'tmt_info'
        elif '214' in pep_orig:
            return 'itraq_info'
        else:
            return None
    
    def find_qdata(self, d):
        field = self._quant_field( self.find_pep_orig(d) )
        qdata = self.data[d['key']].get(field, None)
        return qdata
    
    def find_psites(self, d):
        """
        Locates phospho-sites in a peptide pep_orig and return the list of
        their positions as a '|' delimited string
        """
        pep_orig = self.find_pep_orig(d)
        pos = 0
        psites_pos = list()
        aa_mod = re.split('[\\)\\(, ]', pep_orig)
        for each in aa_mod:
            if each.isdigit():
                i_each = int(each)
                if i_each in (21, 23): psites_pos.append(str(pos - 1))  #X-NOTE: 21 indicates Phosphorilation, 23 indicates Dehidration, equivalent to Phosphorilation lost
            else:
                pos += len(each)
        return '|'.join(psites_pos)
    
    def find_pep_orig(self, d):
        """
        Find the right original peptide sequence (with number modifiers) from
        ['ascore_data']['peptide'], if present, or from ['consensus_mod']
        """
        seq_mod = self.data[d['key']].get('ascore_data', dict()).get('peptide', 
                                                                     None) #['ascore_data']['peptide']
        if not seq_mod:
            seq_mod = self.data[d['key']].get('consensus_mod', None) #['consensus_mod']
        #
        return seq_mod
    
    def iter_load_data(self, io_file, objects = 1):
        """
        Iterative loading and processing of data (uses less memory) from
        `io_file` JSON file.
        """
        json_chunk = list()
        n_objects = 0
        obrakets = 0
        for row in io_file:
            if row is not '{' and row is not '}': #Avoid start and end of Integrator JSON file (first level JSON object)
                row = row.strip() #Strip leading and trailing spaces, '\n', ...
                json_chunk.append(row)
                obrakets += row.count('{') - row.count('}')
                if obrakets == 0: #End of JSON object in second level
                    n_objects += 1
                if n_objects == objects: #When reaching the number of objects requested per chunk:
                    #Build a string with the JSON object, parse it, and store it in a temp. dict.:
                    json_chunk[-1] = json_chunk[-1].rstrip(',')
                    tmp_data = json.loads('{ %s }'%''.join(json_chunk))
                    #Call here the import function:
                    self.import_data(in_data = tmp_data)
                    #Or yield the partial result (self.load_data() should be changed too):
                    #yield tmp_data
                    json_chunk = list()
                    n_objects = 0
        if n_objects: #After reaching end of file, if there are remaining objects to flush:
            #Build a string with the JSON object, parse it, and store it in a temp. dict.:
            json_chunk[-1] = json_chunk[-1].rstrip(',')
            tmp_data = json.loads('{ %s }'%''.join(json_chunk))
            #Call here the import function:
            self.import_data(in_data = tmp_data)
            #Or yield the partial result (self.load_data() should be changed too):
            #yield tmp_data
    
    def load_data(self, io_file):
        if not self.iterative: #Load all the data from io_file in memory
            return super(JsonIntegratorConverter, self).load_data(io_file)
        else: #Use iterative loading and processing of data (use less memory)
            return self.iter_load_data(io_file)
    
    def import_data(self, in_data=None):
        """
        Imports Experimental Data into Data and Spectra models/tables
        from a JSON file generated by the Integrator Perl Program.
        Also triggers the import of Protein Associated Data from UniProt XML
        files.
        """
        
        super(JsonIntegratorConverter, self).import_data(in_data)
        #
        data_conv = self.CONVERTER['Data']
        spectra_conv = self.CONVERTER['Spectra']
        
        cached_prots = list(self.saved_prots) #Copy the list of previous saved proteins in this session to avoid problems if the importation crashes
        unsaved_prots = set()
        unsaved_data = dict()
        
        for json_key in self.data:
            # Don't process new Integrator engine configuration keys:
            if ('engine' in json_key) or ('params' in json_key):
                continue
            #
            # Every JSon first level key contains a data record:
            data_record = self._process_json_key(json_key, data_conv, Data(), 
                                                 extra_data=self.extra_data)
            # Only continue if peptide matches proteins and its phosphorilation
            # state matches the only_phospho attribute:
            if  data_record.tmp_prots and (not self.only_phospho or (self.only_phospho and data_record.dbpsites)):
                # Phosphorilation mark for peptide and related proteins:
                phospho = bool(data_record.dbpsites)
                data_record.phospho = phospho
                if phospho: # Format peptide name according to phospho-sites:
                    fmt_peptide = list(data_record.peptide)
                    for index in data_record.get_psites():
                        fmt_peptide[index] = fmt_peptide[index].lower()
                    data_record.peptide = ''.join(fmt_peptide)
                # Get related Proteins:
                dr_proteins = list()
                decoys = 0
                for protein in data_record.tmp_prots:
                    protein_record = None
                    protein_record_changed = False
                    protein_record_new = False
                    # Increase import performance not importing proteins already
                    # imported in the same import session:
                    if protein in cached_prots: #Protein previously imported in this session:
                        protein_record = cached_prots[cached_prots.index(protein)]
                    else: #Protein not previously imported in this session:
                        try: #Get it from LymPHOS MySQL database:
                            protein_record = Proteins.objects.get(ac=protein.ac)
                        except ObjectDoesNotExist: #Otherwise get it from UniProt data:
                            protein_record = self.prot_imp.import_data(protein)
                            protein_record_new = True
                    # Handle protein record:
                    if protein_record:
                        if phospho and not protein_record.phospho:
                            protein_record.phospho = phospho
                            protein_record_changed = True
                        if protein_record_changed and not protein_record_new:
                            protein_record.save(force_update=True)
                        elif protein_record_new:
                            protein_record.save(force_insert=True)
                            cached_prots.append(protein_record)
                        dr_proteins.append(protein_record)
                    else:
                        unsaved_prots.add(protein.ac)
                    if getattr(protein_record, 'is_decoy', False):
                        decoys += 1
                data_record.n_prots = len(dr_proteins) - decoys
                # Creates ManyToMany relation data <-> proteins:
                # CAUTION! data_record must be saved before Proteins and Spectra
                # relations are established, because these relations need a
                # valid ``Data.id`` value!
                data_record.save(force_insert=True)
                data_record.proteins = dr_proteins
                #
                # Related Spectra:
                spectra_record = self._process_json_key( json_key, spectra_conv,
                                                         Spectra() )
                # Creates ForeingKey relation spectrum -> data:
                spectra_record.data = data_record 
                spectra_record.save(force_insert=True)
                
            else: #DEBUG
                unsaved_data[json_key] = self.data[json_key]
        
        self.saved_prots = cached_prots #Update the copy of session saved proteins, only if no raised errors...
        #
        # Do some error management:
        if unsaved_prots:                           
            self.logger.put_with_key('UnsavedProteins', unsaved_prots)
        if unsaved_data:                           
            self.logger.put_with_key('UnsavedData', unsaved_data)
        #
        return self
    
    def _get_search_engines(self):
        """
        Return a list of the search engines from the JSON Integrator file. Every
        element in the list is a dictionary containing the name of the search
        engine ('engine') and the result parameters for each match exported by
        Integrator ('params')
        """
        if not self.data or self.iterative:
            return None
        #
        search_engines = list()
        engine_keys = ('fst_engine', 'snd_engine', 'trd_engine')
        params_keys = ('fst_params', 'snd_params', 'trd_params')
        for engine_key, params_key in zip(engine_keys, params_keys):
            search_engines.append({'engine': self.data[engine_key],
                                   'params': self.data[params_key]})
        return search_engines


class UniProtXmlConverter(Converter):
    """
    UniProt XML+FASTA files -> Protein model object Returned
    """
    #TO_DO:
    #    Use conversion dictionaries of keys representing each field of the
    #    Model, and, as values, a tuple with the corresponding "tag-PATH" (1
    #    string) in the XML tree of a XML file in position [0]. The tuple also
    #    includes a function object in position [-1] to pass the data obtained
    #    from the XML tree and convert it to the expected data format in the
    #    Model's field
    
#    SEQ_NOTFOUND = "<br/>Sorry. <em>{0} Deleted</em> from UniProt<br/>" \
#                   "<br/> Try to search in <a href='http://www." \
#                   "uniprot.org/uniprot/{0}?version=*'>UniProtKB " \
#                   "History for {0}</a>"
    
    HEAD = '{http://uniprot.org/uniprot}' #Common key for all XML tags
    CHRMS = [str(num) for num in range(1,23)] + ['X', 'Y', 'MT']
    
    def __init__(self, in_data=None, db_file=None):
        """
        Constructor
        """
        super(UniProtXmlConverter, self).__init__(in_data, db_file, zipped=True)
        # Use a ZIP file containing XML and FASTA files for human proteins from
        # UniProt:
        try:
            self.zfile = ZipFile(self.file, 'r')
#            self.db_files = self.zfile.namelist() #X-NOTE: Do not read namelist (too many files -> too much time)
        except IOError as exception:
            zip_error_msg = exception.strerror + ' : %s' % exception.filename
            self.logger.put_with_key('IO Error', zip_error_msg)
            raise exception #TO_DO: Fail more gracefully.
        except BadZipfile as exception:
            zip_error_msg = exception.args[0]
            self.logger.put_with_key('Zip Error', zip_error_msg)
            raise exception #TO_DO: Fail more gracefully.
        zfolder = self.zfile.namelist()[0] #Read first element in zip file.
        if not zfolder[-1] in ('\\', '/'): #Test if first element is a folder (ends width / or \\).
            self.zfolder = ''
        else:
            self.zfolder = zfolder
        # Get chromosome information for each protein AC:
        self.protac2chrm = dict()
        for chrm in self.CHRMS:
            self.protac2chrm.update( dict.fromkeys( self._load_chr_prots(chrm), 
                                                    chrm) )
        #X-NOTE: Currently self.CONVERTER is not used.
        self._xml_e_tree = xml.etree.cElementTree.ElementTree
        self.CONVERTER = {'up_id': (self.HEAD + 'entry/' + 
                                    self.HEAD + 'name',
                                    self._xml_e_tree.find,
                                    lambda e: e.text),
                          'name': (self.HEAD + 'entry/' + 
                                   self.HEAD + 'protein/*/' + 
                                   self.HEAD + 'fullName',
                                   self._xml_e_tree.findall,
                                   lambda e: '. '.join([x.text for x in e])), #Modify for truncate at 300 characters
                          'function': (self.HEAD + 'entry/' + 
                                       self.HEAD + 'comment',
                                       self._xml_e_tree.findall,
                                       self.find_function),
                          'location': (self.HEAD + 'entry/' + 
                                       self.HEAD + 'comment/' +
                                       self.HEAD + 'subcellularLocation/' + 
                                       self.HEAD + "location",
                                       self._xml_e_tree.findall,
                                       lambda e: '. '.join(list(set(each.text 
                                                                    for each 
                                                                    in e)))), #TO_DO: Modify for truncate at 300 characters
                          'seq': (self.HEAD + 'entry/' + 
                                  self.HEAD + 'sequence', 
                                  self._xml_e_tree.find,
                                  lambda e: e.text.replace('\n','')), #TO_DO: Modify to use fasta files
                          'psites': (self.HEAD + 'entry/' + 
                                     self.HEAD + 'feature',
                                     self._xml_e_tree.findall,
                                     self.find_psites),
        }
    
    def find_psites(self, features):
        """
        Locates phospho-sites in a list of XML elements with features
        information for a protein fetched from the UniProtKB and return the
        list of phospho-sites described and their positions
        """
        psites = list()
        for xml_elem in features:
            feature_desc = xml_elem.get('description','')
            if feature_desc in ('Phosphoserine','Phosphotyrosine', 
                                'Phosphothreonine'):
                psite_aa = feature_desc.replace('Phospho','').capitalize()
                psite_pos = xml_elem.find(self.HEAD + 'location/' + 
                                          self.HEAD + 'position').get('position')
                psites.append(psite_aa + ' ' + psite_pos)
        return psites
    
    def find_function(self, comments):
        """
        Locates protein function in a list of XML elements with comments
        information for a protein fetched from the UniProtKB and return this
        function
        """
        function = 'Not described'
        for xml_elem in comments:
            if xml_elem.get('type', '') == 'function':
                function = xml_elem.findtext(self.HEAD + 'text')
                break
        return function
    
    def _load_chr_prots(self, chr):
        chr_prots = list()
        # Open the Chromosome file corresponding to `chr`:
        filen = '{0}nextprot_ac_list_chromosome_{1}.txt'.format(self.zfolder,
                                                                chr)
        io_file = None
        try:
            io_file = self.zfile.open(filen, 'rU')
        except (IOError, KeyError, BadZipfile) as exception:
            file_error_msg = ' : '.join((getattr(exception, 'strerror', ''), 
                                         getattr(exception, 'message', ''), 
                                         getattr(exception, 'filename', ''))
                                        )
            self.logger.put_with_key('Chromosome file', file_error_msg)
            if io_file: io_file.close()
        # Get the protein accession codes:
        if io_file and not io_file.closed:
            chr_prots = [row.strip().split('_')[-1] for row in io_file]
            io_file.close()
        #
        return chr_prots
    
    def _get_sequence(self, ac):
        """
        Returns the aminoacid sequence from the FASTA file corresponding to the
        provided protein ACcession (`ac`).
        """
        # Open the FASTA file corresponding to the ACcession:
        seq_io = None
        try:
            seq_io = self.zfile.open('{0}{1}.fasta'.format(self.zfolder, ac), 
                                     'rU')
        except (IOError, KeyError, BadZipfile) as exception:
            file_error_msg = ' : '.join((getattr(exception, 'strerror', ''), 
                                         getattr(exception, 'message', ''), 
                                         getattr(exception, 'filename', ''))
                                        )
            self.logger.put_with_key('Prot. FASTA file', file_error_msg)
            if seq_io: seq_io.close()
        # Get the cleaned sequence:
        if seq_io and not seq_io.closed:
            _ = seq_io.readline() #Discard FASTA first line (sequence information).
            sequence = u''.join(seq_io.readlines()).translate({32: None,  #Coherce to unicode and 
                                                               9:  None,  #clean sequence from spaces, ...
                                                               10: None, 
                                                               45: None, 
                                                               13: None})
            seq_io.close()
        else:
#            sequence = self.SEQ_NOTFOUND.format(prot_obj.ac)
            sequence = ''
        #
        return sequence
    
    def load_data(self, io_file):
        #X-NOTE: Currently this function is not used.
        prot_xml = xml.etree.cElementTree.ElementTree()
        self.data = prot_xml.parse(io_file)
        return self.data
    
    def import_data(self, in_data=None):    #DEBUG, #REFACTORIZE and #OPTIMIZE
        """
        Fetch information data from XML files in UniProt Knowledge Base format 
        using a protein object accession number (prot_obj.ac).
        The XML data, is parsed and then stored into the protein object passed 
        in in_data parameter.
        The protein sequence data is fetched from FASTA files.
        """
        prot_obj = in_data
        # Only get UniProt XML data for real proteins, not for decoys:
        if 'decoy' in prot_obj.ac:
            prot_obj.is_decoy = True
            prot_obj.seq = self._get_sequence(prot_obj.ac)
            return prot_obj #Returns protein object marked as decoy and only with sequence information
        else:
            prot_obj.is_decoy = False
        
        prot_xml = xml.etree.cElementTree.ElementTree()
        # Get UniProt XML file object (prot_io) from self.zfile ZIP file:
        prot_io = None
        prot_ac_no_isoform = prot_obj.ac.partition('-')[0]
        try:
            prot_io = self.zfile.open('{0}{1}.xml'.format(self.zfolder,
                                                          prot_ac_no_isoform), 
                                      'rU')
        except (IOError, KeyError, BadZipfile) as exception:
            file_error_msg = ' : '.join((getattr(exception, 'strerror', ''), 
                                         getattr(exception, 'message', ''), 
                                         getattr(exception, 'filename', ''))
                                        )
            self.logger.put_with_key('Prot. XML file', file_error_msg)
            if prot_io: prot_io.close()
#            prot_obj.seq = self.SEQ_NOTFOUND.format(prot_obj.ac)
            prot_obj.seq = ''
            return prot_obj
        # Parse XML data fetched from the UniProt XML file:
        prot_xml.parse(prot_io)
        prot_io.close()
        #
        # Assign UniProtKB XML data to attributes/fields of protein object:
        xml_elem = prot_xml.find(self.HEAD + 'entry')
        prot_obj.up_ver = int(xml_elem.get('version'))
        xml_elem = prot_xml.find(self.HEAD + 'entry/' + self.HEAD + 'name')
        prot_obj.up_id = xml_elem.text
        xml_elem = prot_xml.findall(self.HEAD + 'entry/' + self.HEAD + 'protein/*/' + 
                                    self.HEAD + 'fullName')
        prot_name = '. '.join([each.text for each in xml_elem])
        if len(prot_name)> 300:                 #Limit name to 300 characters
            prot_name = prot_name[:257] + '...'
        prot_obj.name = prot_name
        xml_elem = prot_xml.findall(self.HEAD + 'entry/' + self.HEAD + 'comment')
        prot_obj.function = self.find_function(xml_elem)
        xml_elem = prot_xml.findall(self.HEAD + 'entry/' + self.HEAD + 'comment/' +
                                    self.HEAD + 'subcellularLocation/' + 
                                    self.HEAD + "location")
        prot_location = ', '.join(list(set(each.text for each in xml_elem)))
        if len(prot_location)> 300: # Limit location to 300 characters:
            prot_location = prot_location[:257] + '...'
        prot_obj.location = prot_location
        # Assing chromosome to protein:
        prot_obj.chr = self.protac2chrm.get(prot_ac_no_isoform, '')
        # Get sequence from UniProtKB FASTA file to avoid processing XML for
        # isoforms (slow):
        prot_obj.seq = self._get_sequence(prot_obj.ac)
        # Assing UniProt p-sites to protein:
        xml_elem = prot_xml.findall(self.HEAD + 'entry/' + self.HEAD + 'feature')
        prot_obj.up_psites = ', '.join(self.find_psites(xml_elem))
        return prot_obj


class TwoDGelZIPConverter(Converter):
    """
    Zip 2D-Gel multi-file -> 2D-Gel model objects and jpgs Imported
    """
    def __init__(self, in_data=None, in_file=None):
        """
        Constructor
        """
        super(TwoDGelZIPConverter, self).__init__(in_data, in_file, zipped=True)
        #
        self.twodgel = TwoDGel() #The 2D Gel experiment object to import from csv
        self.spots = dict() #The spot objects imported from csv accessible by ingel_id as keys
        self.gelimages = dict() #The gelimage objects imported from csv accessible by name as keys
        #
        self.ALLOWD_EXT = ('CSV', 'PMF', 'JPG', 'DB')
        self.extra_data = {'2DGel': self.twodgel,  #To use as a "pointer" to link/share self.gelimages, self.spots and self.twodgel attributes between different converters
                           'Spots': self.spots, 
                           'GelImages': self.gelimages,
                           }
        self.CONVERTER = {'CSV' : SameSpotsCSVConverter(extra_data=self.extra_data),
                          'PMF': PMFCSVConverter(extra_data=self.extra_data),
                          'JPG': ImageFileConverter(dest_path=os.path.join('images', 
                                                                           'gels')),
                          'DB': JsonIntegratorConverter(zipped=False, 
                                                        extra_data=self.spots,
                                                        only_phospho=True), #TO_DO: Change it to False?
                          }
        #
        self.data_file = None #File whit-in self.file (this is inside the zip file uploaded by the user) to import (self.file MUST be a Zip archive!)
        self.data_type = None #Extension/data_type of file to import
    
    def load_data(self, io_file):
        self.data_file = io_file.name
        self.data_type = self.data_file.rpartition('.')[2].upper()
        converter = self.CONVERTER[self.data_type]
        converter.file = self.data_file
        self.data = converter.load_data(io_file)
        return self.data
    
    def import_data(self, in_data=None):
        #
        super(TwoDGelZIPConverter, self).import_data(in_data)
        #
        self.CONVERTER[self.data_type].import_data()
        self._post_import()
        return self
    
    def _post_import(self):
        if self.data_type == 'JPG':
            gelimage = self.gelimages[self.data_file.rpartition('.')[0]]
            gelimage.hasjpg = True
            gelimage.save(force_update=True)
        return self


class SameSpotsCSVConverter(CSVConverter):
    """
    CSV SameSpots file -> TwoDGel, GelImage, Spot and SpotExtra model objects Imported
    """
    def __init__(self, in_data=None, in_file=None, zipped=False, extra_data=None):
        """
        Constructor
        """
        super(SameSpotsCSVConverter, self).__init__(in_data, in_file, zipped, 
                                                    extra_data)
        #
        self.ALLOWD_EXT = ('CSV',)
        self.CONVERTER = {'spot' : {'ingel_id': ('#', int),
                                    'anova': ('Anova (p)', float),
                                    'fold': ('Fold', float),
                                    'max_cv': ('Max CV (%)', float),
                                    'x': ('X Position', int),
                                    'y': ('Y Position', int),
                                    'area': ('Area', int),
                                    'pi': ('pI', float),
                                    'mw': ('MW', float),
                                    },
                          'extra': {'norm_volume': ('Normalized Volume', float),
                                    'volume': ('Volume', float),
                                    'background': ('Background', float),
                                    'peak_height': ('Peak Height', float),
                                    },
                          }
        #
        if self.extra_data:
            self.twodgel = self.extra_data['2DGel']
            self.spots = self.extra_data['Spots']
            self.gelimages = self.extra_data['GelImages']
        else:
            self.twodgel = TwoDGel() #The 2D Gel experiment object to import from csv
            self.spots = dict() #The spot objects to import from csv, accessible by ingel_id as keys
            self.gelimages = dict() #The gelimages objects to import from csv, accessible by name as keys, and whose images can be imported from jpgs
#            self.extra_data = {'2DGel': self.twodgel, 'Spots': self.spots}
#        self.spotextras = defaultdict(list) #The spotextra objects to import from csv
    
    def import_data(self, in_data=None):            #REFACTORIZE and #OPTIMIZE
        #
        super(SameSpotsCSVConverter, self).import_data(in_data)
        #        
        #1.- Import data in TwoDGel object and save it into the database:
        conditions = list(set(self.data[1])) #Conditions and empty strings are repeated across header-line 1, so remove repetitions
        conditions.remove('') #Remove empty string
        try:
            conditions.remove('Tags') #Remove 'Tags' header (if present)
        except ValueError:
            pass
        self.twodgel.name = os.path.basename(self.file).rpartition('.')[0]
        self.twodgel.conditions = conditions
        self.twodgel.save()
        #2.- Import data in GelImage objects, save them into the database, and
        #store them in self.gelimages dictionary:
        for condition in conditions:             #Get data for all conditions
            index = self.data[1].index(condition) #Locate the index where condition start, according to header-line 1
            while True:
                gelimage = GelImage()
                gelimage.name = name = self.data[2][index] #Get GelImage name from header-line 2
                gelimage.condition = condition
                gelimage.dye = name[-3:]
                gelimage.twodgel = self.twodgel #Establish ForeingKey references
                gelimage.save()
                self.gelimages[name] = gelimage #Index it by name for further use in ForeingKey references in SpotExtra data importation
                index += 1 #Move to the next position
                if self.data[1][index] is not '': break #Exit the loop when another condition is found in header-line 1
        #3.- Import data in Spot and SpotExtra objects, and save them into
        #database:
        #3.1.- Pointers to the conversion dict-filters to use:
        spot_conv = self.CONVERTER['spot']
        extra_conv = self.CONVERTER['extra']
        for csv_line in xrange(3, len(self.data)):
            #3.2.- Spot:
            spot = Spot()
            spot.twodgel = self.twodgel #Related gel association
            for field, params in spot_conv.iteritems():
                csv_field = params[0]
                format_funct = params[-1]
                try:
                    index = self.data[2].index(csv_field)
                except ValueError: #csv_field in conversion-dict is not in SameSpots CSV file
                    continue
                csv_value = self.data[csv_line][index] #Get value from csv data 
                if csv_value:
                    if format_funct:
                        csv_value = format_funct(csv_value)
                    if math.isinf(csv_value) or math.isnan(csv_value): #We can't store nan (NaN: not a number) or inf (infinite) values in MySQL
                        csv_value = -1.0 #So, use a negative value instead
                    setattr(spot, field, csv_value)
            spot.save()
            self.spots[spot.ingel_id] = spot #Index it for further use in ForeingKey references in MS data importation
            #3.3.- SpotExtra:
            spotextra = dict() #Dict of SpotExtra objects where keys are gelimages names
            for imagekey, gelimage in self.gelimages.iteritems():
                spotextra[imagekey] = SpotExtra()
                spotextra[imagekey].spot = spot         #Establish ForeingKey references
                spotextra[imagekey].gelimage = gelimage #to related spot and gelimages
            len_gelimages = len(self.gelimages)
            for field, params in extra_conv.iteritems():
                csv_field = params[0]
                format_funct = params[-1]
                try:
                    index = self.data[0].index(csv_field)
                except ValueError:
                    continue
                for sub_index in range(index, index + len_gelimages):
                    csv_value = self.data[csv_line][sub_index]
                    imagekey = self.data[2][sub_index]
                    if format_funct:
                        csv_value = format_funct(csv_value)
                    setattr(spotextra[imagekey], field, csv_value)
            for spotextra_obj in spotextra.values():
                spotextra_obj.save()
        return self


class PMFCSVConverter(CSVConverter):
    """
    CSV PMF file + Spot dict -> Protein and Spots2Proteins intermediate-relation model objects Imported
    """    
    def __init__(self, in_data=None, in_file=None, zipped=False, extra_data=None):
        """
        Constructor
        """
        super(PMFCSVConverter, self).__init__(in_data, in_file, zipped, extra_data)
        #
        self.ALLOWD_EXT = ('CSV',)
        self.CONVERTER = {'SS_SpotID': ('SS_SpotID', int),
                          'ac': ('ac', None),
                          'score_gelpmf': ('score', float),
                          }
        #
        self.spots = self.extra_data['Spots'] #The spot objects imported from csv accessible by ingel_id as keys
        #
        self.prot_imp = UniProtXmlConverter() #The protein importer filter object to use
    
    def import_data(self, in_data=None):            #REFACTORIZE and #OPTIMIZE
        #
        super(PMFCSVConverter, self).import_data(in_data)
        #
        header_keys = self.data.pop(0)
        for row in self.data:
            spot2protein = Spots2Proteins() #Create the intermediate relationship object
            csv_row = dict( zip(header_keys, row) )
            #Get the spot corresponding to the SameSpot ID:
            csv_field, csv_type = self.CONVERTER['SS_SpotID']
            csv_value = csv_type(csv_row[csv_field])
            spot_record = self.spots[csv_value] 
            spot2protein.spot = spot_record
            #Get the protein corresponding to the identified AC:
            csv_field, _ = self.CONVERTER['ac']
            protein_record, created = Proteins.objects.get_or_create(ac=csv_row[csv_field])
            if created:
                protein_record = self.prot_imp.import_data(protein_record)
                protein_record.phospho = False #If the protein is found ussing PMF, we don't know if it's phosphorilated or not.
                protein_record.save()
            spot2protein.protein = protein_record
            #Get the PMFP score:
            csv_field, csv_type = self.CONVERTER['score_gelpmf']
            csv_value = csv_type(csv_row[csv_field])
            spot2protein.score_gelpmf = csv_value
            #Save relationship to DB:
            spot2protein.save(force_insert=True)


class ImageFileConverter(Converter):
    """
    JPG file in original path or in zip file -> JPG file in destination path
    """    
    def __init__(self, in_data=None, in_file=None, zipped=False, dest_path=None):
        """
        Constructor
        """
        super(ImageFileConverter, self).__init__(in_data, zipped, in_file)
        #
        self.ALLOWD_EXT = ('JPG',)
        #
        if dest_path:
            self.dest_path = os.path.join(settings.STATIC_DOC_ROOT, dest_path).replace('\\','/') #The output path for the images
        else:
            self.dest_path = settings.STATIC_DOC_ROOT
    
    def load_data(self, io_file):
        self.data = io_file.read()
        return self.data
    
    def import_data(self, in_data=None):
        #
        super(ImageFileConverter, self).import_data(in_data)
        #
        out_filename = os.path.basename(self.file).replace('\\','/')
        out_img_file = os.path.join(self.dest_path, out_filename).replace('\\','/')
        with open(out_img_file, 'wb') as io_file:
            io_file.write(self.data)


class DanteRDBConverter(Converter):
    """
    Calculated DanteR SQLite DB results -> DanteR fields in QuantPepExpCond 
    model objects from LymPHOS DB.
    
    :caution: Only DanteR statistical information is imported into already
    existing QuantPepExpCond records. For importation of QuantPepExpCond
    records (from a PQuantifier JSON file), :class:`JsonQuantConverter` should
    be used first!
    """
    import sqlite3
    
    _SQL_TABLES = {'p_adjustment_ANOVA_TABLE': 'PAdj_ANOVA_LogT_T_Data',
                   'ANOVA_TABLE': 'ANOVA_LogT_T_Data'}
    _SQL_ROWS = {'row_id': 'row_names'}
    ALLOWD_EXT = ('DB', 'SQLITE')
    
    def __init__(self, in_data=None, in_file=None, id2sqltable=None, 
                 id2sqlrow=None, separator='_'):
        self.connection = None
        self._filen = None
        super(DanteRDBConverter, self).__init__(in_data, in_file, False, None)
        #
        if id2sqltable:
            self.id2sqltable = id2sqltable
        else:
            self.id2sqltable = self._SQL_TABLES
        if id2sqlrow:
            self.id2sqlrow = id2sqlrow
        else:
            self.id2sqlrow = self._SQL_ROWS
        self.separator = separator
        self.id2sqltablerow = self.id2sqltable.copy() # id2sqltablerow = id2sqltable + id2sqlrow
        self.id2sqltablerow.update( self.id2sqlrow )  #
    
    def __del__(self):
        self.close_conn() #Try to ensure database close.
    
    def open_conn(self):
        """
        Open the connection to the database file, if there is no opened one.
        """
        if not self.connection:
            self.connection = self.sqlite3.connect(self.file)
        return self
    
    def close_conn(self):
        """
        Close the connection to the database, if there is an opened one.
        """
        if self.connection:
            self.connection.close()
            return True
        else:
            return False
    
    @property
    def file(self):
        return self._filen
    @file.setter
    def file(self, file_name):
        if file_name != self._filen:
            # Close the previous connection (if some) and open a new one to the
            # new DB file:
            self.close_conn()
            self._filen = file_name
            self.open_conn()
    
    def load_data(self):
        """
        Method for loading and mix correctly all quantitative data (ANOVA and 
        p-adjusted ANOVA data) from the DanteR SQlite database.
        
        :return list of dict : a list containing a row dictionary for the mix 
        of each record in the database ANOVA table + each corresponding record 
        in the database p-adjusted ANOVA table.
        """
        row_id = self.id2sqlrow['row_id']
        separator = self.separator
        sql_p_adj = "SELECT * FROM {p_adjustment_ANOVA_TABLE} ORDER BY {row_id} ASC;"
        sql_anova = "SELECT * FROM {ANOVA_TABLE} ORDER BY {row_id} ASC;"
        keys_p_adj = list()
        keys_anova = list()
        cursors = list()
        for sql, table, new_keys in ( (sql_p_adj, 'padj', keys_p_adj), 
                                      (sql_anova, 'anova', keys_anova) ):
            # Do a new query (one for each table of interest):
            cursor = self.connection.cursor()
            cursor.execute( sql.format(**self.id2sqltablerow) )
            cursors.append(cursor)
            # Generate the keys for the table records from this query (order of
            # field names in `cursor.description` is the same as corresponding
            # data in each row):
            for field_desc in cursor.description:
                field = field_desc[0] #Get field name.
                if field != row_id: #Quantitative fields(Ex.: 'C15_est', 'C15_pval'):
                    # New keys are tuples for futher fast and easy access, and
                    # include the table "name" to avoid key collission between 
                    # records from the two tables (wich have similar field 
                    # names). Ex.: 'C15_est' -> (15, 'est', 'anova')
                    time_str, particle = field.split(separator)
                    new_key = ( int( time_str[1:] ), particle, table) 
                else:
                    new_key = 'peptide'
                new_keys.append(new_key)
        # Get data from the two queries and mix it:
        quant_data = list() #List of row dictionaries.
        for row_p_adj, row_anova in zip(*cursors):
            assert row_p_adj[0] == row_anova[0] #TEST!
            # Generates a new row dictionary with data from each record in the 
            # database ANOVA table + each corresponding record in the database 
            # p-adjusted ANOVA table:
            quant_data.append( dict( zip(keys_p_adj + keys_anova, 
                                         row_p_adj  +  row_anova) ) )
        self.data = quant_data
        #
        for cursor in cursors:
            cursor.close()
        return quant_data
    
    def _get_supraexp_id4filen(self, in_file=None):
        """
        Get the Supra-experiment ID from a file name. Ex.:
          '/upload/4_filename.sqlite' -> 4
        
        :param str in_file: filename to process. Defaults to ``self.file``.
        
        :return int : the Supra-experiment ID.
        """
        if not in_file: in_file = self.file
        filen = os.path.basename(in_file)
        return int( filen.partition(self.separator)[0] )
    
    def import_data(self, in_data=None):
        super(DanteRDBConverter, self).import_data(in_data)
        #
        type_id = self._get_supraexp_id4filen() #Get Supra-experiment.
        for row in self.data:
            # Get the corresponding existing QuantPepExpCond record from
            # LymPHOS DB:
            try:
                qpepcond = QuantPepExpCond.objects.get(unique_pep=row['peptide'], 
                                                       expgroup_id=type_id)
            except ObjectDoesNotExist as _:
                self.logger.put_with_key('DanteR peptides not matching LymPHOS PQuant', 
                                         'Peptide {0} of supra-experiment {1} '\
                                         'not found in LymPHOS DB'.format(row['peptide'], type_id))
                self.logger.put_with_key('UnsavedData', 
                                         'Peptide {0} of supra-experiment {1} '\
                                         'not found in LymPHOS DB'.format(row['peptide'], type_id))
                continue
            # Get the needed DanteR row data in the rigth time order (as in 
            # QuantPepExpCond.quanttimes):
            danteravgratios = list()
            danterpvalues = list()
            danteradjpvalues = list()
            for time in qpepcond.quanttimes:
                danteravgratios.append( row[time, 'est', 'anova'] )
                danterpvalues.append( row[time, 'pval', 'anova'] )
                danteradjpvalues.append( row[time, 'pval', 'padj'] )
            # Update the QuantPepExpCond record wit DanteR data:
            qpepcond.danteravgratios = danteravgratios
            qpepcond.danterpvalues = danterpvalues
            qpepcond.danteradjpvalues = danteradjpvalues
            qpepcond.save(force_update=True)
        #
        return self

    def process_file(self, in_file=None):
        """
        :caution: only works with no zipped individuall sqlite3 files.
        """
        if in_file: self.file = in_file
        #
        self.load_data()
        self.import_data()
        #
        return self
    

