# -*- coding: utf-8 -*-

'''
Created on 21/01/2011

@authors: LP CSIC/UAB (lp.csic@uab.cat)
'''

import datetime

class NewsAndLogs(object):
    '''
    Mock Model class for supply News and Logs for the sidebar of the
    LymPHOS 1.5 Web-App without a real database
    '''
    __news_current_id = 0
    __logs_current_id = 0
    NEW = -1
    LOG = -2
    def __init__(self, text, type=-1, date_time=None):
        if not (text and isinstance(text, str)):
            del self
            raise TypeError('Incorrect text value or type')
        if not isinstance(date_time, datetime.datetime) :
            del self
            raise TypeError('Incorrect date_time type')
            
        if type == -1 :
            self.id = self.__class__.__news_current_id
            self.__class__.__news_current_id += 1
            self.type = self.NEW
        elif type == -2 :
            self.id = self.__class__.__logs_current_id
            self.__class__.__logs_current_id += 1
            self.type = self.LOG
        else :
            del self
            raise TypeError('Incorrect type value')
        
        self.text = text
        if date_time:
            self.date_time = date_time
        else:
            self.date_time = datetime.datetime.now()
        
    def date_string(self):
        return (self.date_time.date().isoformat())
               
        