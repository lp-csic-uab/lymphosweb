# -*- coding: utf-8 -*-
"""
:synopsis:   Support classes for LymPHOS_v1_5 project views

:created:    2011-01-14

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2011-2016 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '2.0'
__UPDATED__ = '2016-07-14'


#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django.db import transaction
from django.conf import settings
from django.http import HttpResponse, HttpResponseForbidden

# Application imports:
from lymphos import logic, filters, models
from lymphos.utilities import kdf_sha256
from basic_classes import FormsHtmlPage

# Python core imports:
from collections import defaultdict
import datetime
import os
from hashlib import sha1
try:
    from hashlib import pbkdf2_hmac
except ImportError as _:
    from backports.pbkdf2 import pbkdf2_hmac


#===============================================================================
# Support classes definitions
#===============================================================================
class AccesControl(object):

    def __init__(self, request=None, dl_logs=None, dl_errors=None,
                 min_permission=0):
        self.request = request
        if dl_logs != None:
            self._logs = dl_logs
        else:
            self._logs = defaultdict(list)
        if dl_errors != None:
            self._errors = dl_errors
        else:
            self._errors = defaultdict(list)
        self.min_permission = min_permission

    def _purge_old_OTS(self, minutesold=3):
        """
        Deletes old One-Time-Salt records from database.
        """
        old_date = datetime.datetime.now() - datetime.timedelta(minutes=minutesold)
        old_ots = logic.SearchDB().st_ots(old_date, 'old_ots').get_query('old_ots')
        old_ots.delete()

    def _get_OTS(self, session_id):
        """
        Gets, and return an One-Time-Salt record from the DataBase.
        But first calls :method:`_purge_old_OTS`.
        """
        self._purge_old_OTS()
        return logic.SearchDB().get_ots(session_id)

    def generate_OTS(self):
        """
        Generates, and return a new One-Time-Salt record.
        Also calls :method:`_purge_old_OTS`.
        """
        self._purge_old_OTS()
        new_ots = models.OTS()
        new_ots.save()
        return new_ots

    def login(self, log_key='Log-in'):
        """
        Validates user credentials entered in the login_view template.
        """
        request = self.request
        # Get One-Time-Salt from the DataBase for the current log-in session:
        session_id = request.POST['l_session']
        ots = self._get_OTS(session_id)
        if not ots: #If no OTS in DataBase (expired OTS, fake OTS, ...), don't allow the user to log-in:
            return
        # Get input credentials and matching existing user in DataBase:
        hashed_post = bool( int( request.POST['hashed'] ) )
        email = request.POST['lu_name']
        if hashed_post:
            email_hash = str(email) #Unicode -> String (needed for some implementations of :func:`pbkdf2_hmac`).
        else:
            email_hash = kdf_sha256( settings.APPNAME + email.strip(), 10000 )
        deriv_email = pbkdf2_hmac('sha256', email_hash, settings.PEPPER, 90000)
        user = logic.SearchDB().get_user( deriv_email.encode('hex') )
        # Check user password with existing user:
        if user and user.check_passwd(request.POST['lu_code'], ots.uuid, hashed_post):
            # Check user permissions for current session:
            if user.permissions >= self.min_permission:
                # Set a user session cookie for the user:
                request.session['luid1'] = user.pk.decode('hex')[::-1].encode('base64').swapcase()
                request.session['luid2'] = sha1( str(user.singup_date) ).hexdigest()
                browser = request.META.get('HTTP_USER_AGENT', '')
                clnt_host = request.META.get('REMOTE_HOST', '')
                clnt_user = request.META.get('USER', '')
                request.session['bhuid'] = sha1(browser+clnt_host+clnt_user).hexdigest()
                # Updates the user :attr:`last_login` in DB:
                user.save()
                # Log the log-in event:
                log_msg = '{0:%Y-%m-%d %H:%M} : '\
                          '{1} Logged in'.format(user.last_login, user.pk)
                self._logs[log_key].append(log_msg)
            else: #Error: Invalid permissions:
                self._errors[log_key].append('Not enough accession level!')
        else: #Error: Invalid password:
            self._errors[log_key].append('Invalid log-in!')
        #
        ots.delete() #Delete already used One-Time-Salt.
        return

    def check_current_login(self, log_key='Log-in'):
        """
        Checks if a user is already loged-in and with the right permission
        level, by means of validating user session cookie.
        """
        validity = False
        request = self.request
        uid = request.session.get('luid1', None)
        if uid is not None:
            user = logic.SearchDB().get_user( uid.swapcase().decode('base64')[::-1].encode('hex') )
            singupdate_hash = request.session.get('luid2', '')
            browser_host_user_hash = request.session.get('bhuid', '')
            browser = request.META.get('HTTP_USER_AGENT', '')
            clnt_host = request.META.get('REMOTE_HOST', '')
            clnt_user = request.META.get('USER', '')
            #
            if (user and sha1( str(user.singup_date) ).hexdigest() == singupdate_hash and
                sha1(browser+clnt_host+clnt_user).hexdigest() == browser_host_user_hash and
                user.permissions >= self.min_permission):
                validity = True
            else:
                request.session.flush() #Deletes the invalid user session cookie from user's browser.
                self._errors[log_key].append('Problems with current logged user!')
        return validity

    def logout(self, log_key='Log-out'):
        """
        Get out of a validated user session.
        """
        self.request.session.flush() #Deletes the session cookie from user's browser.
        log_msg = '{0:%Y-%m-%d %H:%M} : '\
                  'Logged out'.format(datetime.datetime.now())
        self._logs[log_key].append(log_msg)
        return


class UploadPage(FormsHtmlPage):

    template_name = 'upload_section.html'
    section_title = 'Upload Data'

    def __init__(self, *args, **kwargs):
        self._logs = defaultdict(list)  #Prepare context attribute to store logs.
        self.importer = None #Declares the importer object attribute.
        self.cache_msg = False #At session start no message about rebuilding cache.
        self.date = None #Current date and time for logs, to set at every action.

        # __init__ from superclass:
        super(UploadPage, self).__init__(*args, **kwargs)

        self.login = AccesControl(self.request, self._logs, self._errors, 1)

        # Selector dictionary containing keys to be found in a request.POST
        # dictionary, and a method to be called with specific extra information
        # needed by this method as a kwargs dictionary:
        self._OPT2ACT = {
          'log-in': {'method': self.login.login,
                     'kwargs': {'log_key': 'Log-in'},
                     },
          'submit_conditions': {'method': self.upload_and_import,
                                'kwargs': {'file_field': 'fconditions',
                                           'importer_cls': filters.JsonCondConverter,
                                           'has_cachemsg': True,
                                           'log_key': 'Conditions'},
                                },
          'submit_dataset': {'method': self.upload_and_import,
                             'kwargs': {'file_field': 'fdataset',
                                        'importer_cls': filters.JsonIntegratorConverter,
                                        'has_cachemsg': True,
                                        'log_key': 'DataSets'},
                             },
          'submit_quant': {'method': self.upload_and_import,
                           'kwargs': {'file_field': 'fquantitative',
                                      'importer_cls': filters.JsonQuantConverter,
                                      'has_cachemsg': True,
                                      'log_key': 'Quantitative'},
                           },
          'submit_danter': {'method': self.upload_and_import,
                            'kwargs': {'file_field': 'fdanter',
                                       'importer_cls': filters.DanteRDBConverter,
                                       'has_cachemsg': False,
                                       'log_key': 'Quantitative'},
                           },
          'rebuild_cache': {'method': self.rebuild_cache,
                            'kwargs': {'log_key': 'Cache'},
                            },
          'log-out': {'method': self.login.logout,
                      'kwargs': {'log_key': 'Log-out'},
                      },
          'apply-log-out': {'method': self.rebuild_logout,
                            'kwargs': {'log_key': 'Cache'},
                            },
                          }

    def _update_errors(self, error_keys, dct):
        for error_key in error_keys:
            self._errors[error_key].extend(dct[error_key])
        return self.errors

    def _handle_upload(self, file_key, log_key):
        """
        Handle the upload of any file (in field `file_key`) to the server.
        """
        up_file = self.request.FILES[file_key]
        fullpath_up_file = os.path.join(settings.MEDIA_ROOT, up_file.name) #Upload directory + filename.
        if fullpath_up_file.rpartition('.')[2].upper() == 'ZIP': #Zip extension:
            imp_zipped = True
        else:
            imp_zipped = False

        tmp_file = None
        try:
            tmp_file = open(fullpath_up_file, 'wb+')
            if up_file.multiple_chunks(): #Uploaded file is large:
                for chunk in up_file.chunks(): #Upload in chunks
                    tmp_file.write(chunk)
            else: #Uploaded file is less than 2.5MB:
                tmp_file.write( up_file.read() ) #Fast bulk upload and write
        except IOError:
            log_msg = '{0:%Y-%m-%d %H:%M} : '\
                      'Error Uploading File {1}'.format(self.date, up_file.name)
            self._put_error(log_key, log_msg.rsplit(' : ', 1)[-1])
            self._logs[log_key].append(log_msg)
            fullpath_up_file = None #No file uploaded to further import
        finally:
            if tmp_file:
                tmp_file.close()

        return fullpath_up_file, imp_zipped

    def upload_and_import(self, file_field, importer_cls, has_cachemsg=False,
                          log_key='Upload and Import'):
        """
        Call :method:`_handle_upload` to get the file into the server and then
        call :method:`import_file` to get the file data into the LymPHOS
        DataBase.
        """
        log_msg = ''
        if self.request.FILES.get(file_field, False):
            imp_file, imp_zipped = self._handle_upload(file_field, log_key)
            if imp_file: #If there were no errors during uploading:
                self.importer = importer_cls() #Set the right importer.
                # Try to import the file:
                errors = self.import_file(imp_file, imp_zipped, log_key)
                os.remove(imp_file) #Remove the uploaded file.
                if has_cachemsg:
                    self.cache_msg = not errors #Only true if there were no errors.
        else:
            log_msg = '{0:%Y-%m-%d %H:%M} : '\
                      'No file provided for uploading'.format(self.date)
            self._logs[log_key].append(log_msg)

        return self

    @transaction.commit_on_success
    def import_file(self, imp_file, imp_zipped, log_key):
        """
        Import the uploaded `imp_file` using the page ``self.importer``.
        """
        log_msg = ''
        errors = False
        self.importer.zipped = imp_zipped
        # Converter execution (load data & import data):
        self.importer.process_file(imp_file)
        # Errors check:
        imp_log_keys = self.importer.logger.keys()
        imp_error_keys = [each for each in imp_log_keys if 'Error' in each]
        imp_file_name = imp_file.rpartition('.')[0]
        if not imp_error_keys: #No importer errors:
            log_msg = '{0:%Y-%m-%d %H:%M} : '\
                      '{1} Imported into LymPHOS DataBase'.format(self.date,
                                                                  imp_file_name)
        else: #Some importer errors in file reading:
            errors = True
            log_msg = '{0:%Y-%m-%d %H:%M} : '\
                      'Errors Reading uploaded file {1}'.format(self.date,
                                                                imp_file_name)
            # Update the page ``self._errors`` with the importer errors:
            self._update_errors(imp_error_keys, self.importer.logger())
        self._logs[log_key].append(log_msg)
        # If any not imported data is available, Log it to disk:        #DEBUG
        if 'UnsavedData' in imp_log_keys:
            n_datarecords = 0
            for index, dict_data in enumerate(self.importer.logger('UnsavedData')):
                n_datarecords += len(dict_data)
                log_fname = '{0}_{1}.log'.format(imp_file, index)
                with open(log_fname, 'w') as log_file:
                    filters.json.dump(dict_data, log_file, indent=4)
            log_msg = '{0} Data record(s) not imported (see {1}_*.log '\
                      'for details)'.format(n_datarecords, imp_file_name)
            self._logs[log_key].append(log_msg)
            imp_log_keys.remove('UnsavedData')
        for il_key in imp_log_keys:
            log_msg = ', '.join( self.importer.logger(il_key) )
            self._logs[log_key].append(log_msg)
        del self.importer.logger.log
        #
        return errors

    def rebuild_cache(self, log_key='Cache'):
        """
        Rebuild the DB PeptideCache table.
        """
        new_cache = logic.MySQLCache()
        new_cache.peptide_cache()
        self.cache_msg = False
        log_msg = '{0:%Y-%m-%d %H:%M} : Changes applied. '\
                  'PeptideCache Table Rebuilded'.format(self.date)
        self._logs[log_key].append(log_msg)
        return self

    def rebuild_logout(self, log_key='Cache'):
        """
        Rebuild the DB PeptideCache table and then Log-out the user.
        """
        self.rebuild_cache(log_key)
        self.login.logout(log_key='Log-out')
        return self

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context:
        context = super(UploadPage, self).get_context_data(**kwargs)
        # Verifies if the user has an active session:
        logged = self.login.check_current_login()
        # Update the context:
        if not logged:
            context['ots'] = self.login.generate_OTS() #Time limited One-Time-Salt.
        context.update({'logs': self._logs.items(),
                        'cache_msg': self.cache_msg,
                        'login_action': '/upload/',
                        'logged': logged})
        return context

    def get(self, request, *args, **kwargs):
        self.login.request = self.request = request
        #
        del self.errors
        # Let the corresponding superclass :method:``get`` do its stuff, and
        # return the :class:`HttpResponse` object generated:
        return super(UploadPage, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.login.request = self.request = request
        #
        for opt_key, act_dct in self._OPT2ACT.items():
            if opt_key in request.POST:
                self.date = datetime.datetime.now()
                method = act_dct['method']
                method( **act_dct['kwargs'] )
                break
        # Let the superclass :method:``post`` do its stuff, and return the
        # :class:`HttpResponse` object generated:
        return super(UploadPage, self).post(request, *args, **kwargs)


class DownloadPage(FormsHtmlPage):

    template_name = 'download_section.html'
    section_title = 'Download Data'

    def __init__(self, *args, **kwargs):
        self.date = None #Current date and time for logs.

        # __init__ from superclass:
        super(DownloadPage, self).__init__(*args, **kwargs)

        self.login = AccesControl(self.request, dl_errors=self._errors)

        # Selector dictionary containing keys to be found in a request.POST
        # dictionary, and a method to be called with specific extra information
        # needed by this method as a kwargs dictionary:
        self._OPT2ACT = {
          'log-in': {'method': self.login.login,
                     'kwargs': {'log_key': 'Log-in'},
                     },
          'download_dbexport_quant': {'method': self.download_file,
                                      'kwargs': {'filename': 'dbexport_quant.zip',
                                                 'content_type': 'application/zip'},
                                      },
          'download_dbexport_all': {'method': self.download_file,
                                    'kwargs': {'filename': 'dbexport_all.zip',
                                               'content_type': 'application/zip'},
                                    },
          'log-out': {'method': self.login.logout,
                      'kwargs': {'log_key': 'Log-out'},
                      },
                          }

    def download_file(self, filename, content_type=''):
        # Verify if the user has an active valid session:
        if self.login.check_current_login():
            # Create a :class:`HttpResponse` object to store and send the download file:
            response = HttpResponse()
            # :class:`HttpResponse` object headers:
            response['Content-Type'] = content_type
            response['Content-Disposition'] = 'attachment; '\
                                              'filename={0}'.format(filename)
            # Put file contents in :class:`HttpResponse` object:
            fullpath_dw_file = os.path.join(settings.SPECIAL_DOC_ROOT, filename) #Download directory + filename.
            with open(fullpath_dw_file, 'rb') as io_file:
                response.write( io_file.read() )
            #
            return response
        else: # Error: Invalid login credentials:
            return HttpResponseForbidden('Error 403 - Forbidden: Access is Denied')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context:
        context = super(DownloadPage, self).get_context_data(**kwargs)
        # Verify if the user has an active valid session:
        logged = self.login.check_current_login()
        # Update the context:
        if not logged:
            context['ots'] = self.login.generate_OTS() #Time-limited One Time Salt for a unlogged user.
        context.update( {'login_action': '/download/',
                         'logged': logged} )
        return context

    def get(self, request, *args, **kwargs):
        self.login.request = self.request = request
        #
        del self.errors
        # Let the corresponding superclass :method:``get`` do its stuff, and
        # return the :class:`HttpResponse` object generated:
        return super(DownloadPage, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.login.request = self.request = request
        #
        for opt_key, act_dct in self._OPT2ACT.items():
            if opt_key in request.POST:
                self.date = datetime.datetime.now()
                method = act_dct['method']
                response = method( **act_dct['kwargs'] )
                if response:
                    # Return the download contents (a :class:`HttpResponse`
                    # object containing a file):
                    return response
                break
        # Let the superclass :method:``post`` do its stuff, and return the
        # :class:`HttpResponse` object generated:
        return super(DownloadPage, self).post(request, *args, **kwargs)

