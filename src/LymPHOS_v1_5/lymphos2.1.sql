SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `lymphos_lymphos2d` ;
CREATE SCHEMA IF NOT EXISTS `lymphos_lymphos2d` DEFAULT CHARACTER SET latin1 ;
USE `lymphos_lymphos2d` ;

-- -----------------------------------------------------
-- Table `lymphos_lymphos2d`.`lymphos_expgroup`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphos2d`.`lymphos_expgroup` (
  `id` INT NOT NULL ,
  `name` MEDIUMTEXT NOT NULL ,
  `description` MEDIUMTEXT NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphos2d`.`lymphos_experiments`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphos2d`.`lymphos_experiments` (
  `id` INT NOT NULL ,
  `expgroup_id` INT NOT NULL DEFAULT 0 ,
  `dbsub_exps` VARCHAR(50) NOT NULL DEFAULT '' ,
  `name` VARCHAR(100) NOT NULL DEFAULT '' ,
  `miape_id` VARCHAR(50) NOT NULL DEFAULT '' ,
  `description` VARCHAR(180) NOT NULL DEFAULT '' ,
  `instrument` VARCHAR(150) NOT NULL DEFAULT '' ,
  `label` VARCHAR(10) NOT NULL DEFAULT '' ,
  `quant_cond` MEDIUMTEXT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `miapes_id_fk1` (`miape_id` ASC) ,
  INDEX `exp_group_id_fk1` (`expgroup_id` ASC) ,
  CONSTRAINT `expgroup_id_fk1`
    FOREIGN KEY (`expgroup_id` )
    REFERENCES `lymphos_lymphos2d`.`lymphos_expgroup` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphos2d`.`lymphos_proteins`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphos2d`.`lymphos_proteins` (
  `ac` VARCHAR(25) NOT NULL DEFAULT '' ,
  `up_id` VARCHAR(25) NOT NULL DEFAULT '' ,
  `up_ver` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `name` VARCHAR(300) NOT NULL DEFAULT '' ,
  `function` MEDIUMTEXT NOT NULL ,
  `location` VARCHAR(300) NOT NULL DEFAULT '' ,
  `seq` MEDIUMTEXT NOT NULL ,
  `up_psites` MEDIUMTEXT NOT NULL ,
  PRIMARY KEY (`ac`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphos2d`.`lymphos_quantpepexpcond`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphos2d`.`lymphos_quantpepexpcond` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `unique_pep` VARCHAR(80) NOT NULL DEFAULT '' ,
  `expgroup_id` INT NOT NULL DEFAULT 0 ,
  `dballquanttimes` MEDIUMTEXT NOT NULL ,
  `dbavgratios` MEDIUMTEXT NOT NULL ,
  `dbstdv` MEDIUMTEXT NOT NULL ,
  `dbavgregulation` VARCHAR(200) NOT NULL DEFAULT '' ,
  PRIMARY KEY (`id`) ,
  INDEX `expgroup_id_fk2` (`expgroup_id` ASC) ,
  INDEX `quantpepexpcond_uniquepep` (`unique_pep` ASC) ,
  CONSTRAINT `expgroup_id_fk2`
    FOREIGN KEY (`expgroup_id` )
    REFERENCES `lymphos_lymphos2d`.`lymphos_expgroup` (`id` ))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphos2d`.`lymphos_data`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphos2d`.`lymphos_data` (
  `experiment_id` INT NOT NULL ,
  `id` INT NOT NULL AUTO_INCREMENT ,
  `out_file` VARCHAR(100) NOT NULL ,
  `raw_file` VARCHAR(200) NOT NULL ,
  `scan_i` INT UNSIGNED NOT NULL ,
  `scan_f` INT UNSIGNED NOT NULL ,
  `ms_n` TINYINT UNSIGNED NOT NULL ,
  `rt` INT UNSIGNED NOT NULL ,
  `mass` FLOAT NOT NULL ,
  `deltamass` FLOAT NOT NULL ,
  `ions` VARCHAR(10) NOT NULL ,
  `charge` TINYINT NOT NULL ,
  `pep_orig` VARCHAR(200) NOT NULL ,
  `peptide` VARCHAR(80) NOT NULL ,
  `pep_score` FLOAT NOT NULL ,
  `dbpsites` VARCHAR(20) NOT NULL ,
  `dbascores` VARCHAR(85) NOT NULL ,
  `n_prots` SMALLINT UNSIGNED NOT NULL ,
  `xcorr` FLOAT NOT NULL ,
  `deltacn` FLOAT NOT NULL ,
  `d` FLOAT NOT NULL ,
  `omssa` FLOAT NOT NULL ,
  `phenyx` FLOAT NOT NULL ,
  `qdata` MEDIUMTEXT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `experiment_id_fk1` (`experiment_id` ASC) ,
  CONSTRAINT `experiment_id_fk1`
    FOREIGN KEY (`experiment_id` )
    REFERENCES `lymphos_lymphos2d`.`lymphos_experiments` (`id` ))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphos2d`.`lymphos_data_proteins`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphos2d`.`lymphos_data_proteins` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `data_id` INT NOT NULL ,
  `proteins_id` VARCHAR(25) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `data_id_protein_id` (`data_id` ASC, `proteins_id` ASC) ,
  INDEX `data_id_fk2` (`data_id` ASC) ,
  INDEX `proteins_id_fk1` (`proteins_id` ASC) ,
  CONSTRAINT `proteins_id_fk1`
    FOREIGN KEY (`proteins_id` )
    REFERENCES `lymphos_lymphos2d`.`lymphos_proteins` (`ac` ),
  CONSTRAINT `data_id_fk2`
    FOREIGN KEY (`data_id` )
    REFERENCES `lymphos_lymphos2d`.`lymphos_data` (`id` ))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphos2d`.`lymphos_spectra`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphos2d`.`lymphos_spectra` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `data_id` INT NOT NULL ,
  `ms_n` TINYINT UNSIGNED NOT NULL ,
  `mass` MEDIUMTEXT NOT NULL ,
  `intensity` MEDIUMTEXT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `data_id_fk1` (`data_id` ASC) ,
  CONSTRAINT `data_id_fk1`
    FOREIGN KEY (`data_id` )
    REFERENCES `lymphos_lymphos2d`.`lymphos_data` (`id` ))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphos2d`.`lymphos_users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphos2d`.`lymphos_users` (
  `mail` VARCHAR(240) NOT NULL ,
  `passwd` VARCHAR(24) NOT NULL ,
  `permissions` TINYINT UNSIGNED NOT NULL ,
  `singup_date` DATETIME NOT NULL ,
  `last_logging` DATETIME NOT NULL ,
  PRIMARY KEY (`mail`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphos2d`.`lymphos_dataquant`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphos2d`.`lymphos_dataquant` (
  `data_id` INT NOT NULL ,
  `id` INT NOT NULL AUTO_INCREMENT ,
  `dbquanttimes` VARCHAR(100) NOT NULL DEFAULT '' ,
  `dbratios` VARCHAR(100) NOT NULL DEFAULT '' ,
  `dbstdv` VARCHAR(100) NOT NULL DEFAULT '' ,
  `dbregulation` VARCHAR(100) NOT NULL DEFAULT '' ,
  `quantpepexpcond_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `data_id_fk3` (`data_id` ASC) ,
  INDEX `quantpepexpcond_id_fk1` (`quantpepexpcond_id` ASC) ,
  CONSTRAINT `data_id_fk3`
    FOREIGN KEY (`data_id` )
    REFERENCES `lymphos_lymphos2d`.`lymphos_data` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `quantpepexpcond_id_fk1`
    FOREIGN KEY (`quantpepexpcond_id` )
    REFERENCES `lymphos_lymphos2d`.`lymphos_quantpepexpcond` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphos2d`.`lymphos_peptidecache`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphos2d`.`lymphos_peptidecache` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `peptide` VARCHAR(80) NOT NULL ,
  `dbpsites` VARCHAR(20) NOT NULL ,
  `dbascores` VARCHAR(85) NOT NULL ,
  `n_prots` SMALLINT UNSIGNED NOT NULL ,
  `dbproteins` MEDIUMTEXT NOT NULL ,
  `quantitative` TINYINT(1)  NOT NULL ,
  `n_scans` SMALLINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `lymphos_peptidecache_peptide` (`peptide` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphos2d`.`lymphos_miapes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphos2d`.`lymphos_miapes` (
  `id` VARCHAR(3) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `lymphos_lymphos2d`.`lymphos_expgroup`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
USE `lymphos_lymphos2d`;
INSERT INTO `lymphos_lymphos2d`.`lymphos_expgroup` (`id`, `name`, `description`) VALUES (0, 'No data', '');

COMMIT;

-- -----------------------------------------------------
-- Data for table `lymphos_lymphos2d`.`lymphos_experiments`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
USE `lymphos_lymphos2d`;
INSERT INTO `lymphos_lymphos2d`.`lymphos_experiments` (`id`, `expgroup_id`, `dbsub_exps`, `name`, `miape_id`, `description`, `instrument`, `label`, `quant_cond`) VALUES (0, 0, '', 'No condition', '', '', '', '', '');

COMMIT;

-- -----------------------------------------------------
-- Data for table `lymphos_lymphos2d`.`lymphos_quantpepexpcond`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
USE `lymphos_lymphos2d`;
INSERT INTO `lymphos_lymphos2d`.`lymphos_quantpepexpcond` (`id`, `unique_pep`, `expgroup_id`, `dballquanttimes`, `dbavgratios`, `dbstdv`, `dbavgregulation`) VALUES (0, 'No cuantitative data', 0, '', '', '', '');

COMMIT;
