# -*- coding: utf-8 -*-
"""
:synopsis:   Mass Spectrometry classes for LymPHOS_v1_5 project views

:created:    2011-01-14

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2011-2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '2.0'
__UPDATED__ = '2015-11-04'


#===============================================================================
# Imports
#===============================================================================
#DJango imports:
from django.db.models import Count
from django.http import HttpResponse, HttpResponseForbidden, Http404

#Application imports:
from django.conf import settings
from lymphos import logic
from basic_classes import FormsHtmlPage, SearchResultPage, SearchReaultPagePOST2GET, DinamicFile
from support_classes import AccesControl
#JSONVisor imports:
from jvisor_fragmentor import get_ion_dict

#matplotlib imports:
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.figure import Figure

#Python core imports:
from collections import defaultdict
import datetime
import re

#===============================================================================
# MS View classes definitions
#===============================================================================
class PeptideResultPage(SearchResultPage):
    """
    SubClass of SearchResultPage, to search for peptides and generate the
    corresponding results view.
    """

    result_template = 'peptide_result.html'
    paginated = True
    itemsxpage = 100
    post_search_field = 'peptide_sequence'
    search_default = '*'

    def __init__(self, *args, **kwargs):
        # __init__ from Superclass:
        super(PeptideResultPage, self).__init__(*args, **kwargs)

        # Update inherited Pseudo_Constant _REGEX_REPLACE with new text to do
        # regex substitutions:
        self._REGEX_REPLACE.update( {'B': '[BND]',
                                     'Z': '[ZEQ]',
                                     'J': '[JIL]',
                                     'X': '.',
                                     'I': '[IJ]',
                                     'L': '[LJ]'} )
        # Update other inherited Pseudo_Constants:
        self._search_validator = r"""[A-Ztsy*?]"""
        self.allowed_order_fields = ('peptide', 'n_scans', 'quantitative',
                                     'gel', 'n_prots', 'phospho')

    def search(self, rs_search=None):
        if rs_search:                    #Set the string search of the object
            self.s_search = rs_search
        else:                            #Do the searches
            self._searches.st_peptidecache(self._s_search, 'cachedpep',
                                           o_tfields = self.order_fields)
            peptides = self._searches.get_query('cachedpep')
            if peptides:
                self.results = peptides
                super(PeptideResultPage, self).search() #Basic pagination stuff
            else:
                self.results = None
                self._put_error( "Peptide Search Error",
                                 "No results found for peptides matching " \
                                 "*{0}*".format(self._o_s_search) )
        return self.results

    def get_context_data(self, **kwargs):
        """
        Format results to be included in the context dictionary of the page
        object, so they can be rendered through the show() method in a result
        template.
        """
        # Call the base implementation first to get a context:
        context = super(PeptideResultPage, self).get_context_data(**kwargs)
        # Return if there is no search result data to add:
        if not self.results:
            return context
        # Table header data:
        table_headers = ( {'html': 'Peptide',
                           'title': 's,t,y represent phosphorylation on Ser, Thr or Tyr',
                           'width': '24%',
                           'order_by': 'peptide'},
                          {'html': '&nbsp;#&nbsp;specs&nbsp;',
                           'title': 'Number of spectra identifying the peptide',
                           'width': '4%',
                           'order_by': 'n_scans'},
                          {'html': '<img alt="quantitative_data" src="/files/images/qdata.png">',
                           'title': 'Quantitative data availability',
                           'width': '6%',
                           'order_by': 'quantitative'},
# No more 2D Gel information
#                           {'html': '<img alt="2d_data" src="/files/images/2dgel.png">',
#                            'title': 'Associated 2D Gel experiment',
#                            'width': '6%',
#                            'order_by': 'gel'},
# No more 2D Gel information
                          {'html': '&nbsp;#&nbsp;prots&nbsp;',
                           'title': 'Number of proteins containing the peptide in the Uniprot (SwissProt+Trembl) Human database',
                           'width': '4%',
                           'order_by': 'n_prots'},
                          {'html': 'Proteins',
                           'title': 'Uniprot ACcession numbers',
                           'width': '58%',
                           'order_by': ''},
                         )
        context['table_headers'] = table_headers
        # Update the context with search results:
        context['peptides'] = self.results
        context['n_peptides'] = self.results.paginator.count
        return context

    def post(self, request, *args, **kwargs):
        if request.POST.has_key('peptide_search'):
            self.post_search_type = 'peptide_search'
            self.post_search_field = 'peptide_sequence'
            self.results_title = 'Search Results : Phospho-peptides found in LymPHOS DB'
        elif request.POST.has_key('all_peptides_search'):
            self.post_search_type = 'all_peptides_search'
            self.post_search_field = ''
            self.results_title = 'Browsing All Phospho-peptides found in LymPHOS DB'

        return super(PeptideResultPage, self).post(request, *args, **kwargs)


class PeptideResultPage2D(PeptideResultPage):

    results_title = 'Search Results : Peptides from 2D Gels found in LymPHOS DB'

    def search(self, rs_search=''):
        if rs_search:
            self.s_search = rs_search    #Set the string search of the object
        else:                            #Do the searches:
            self._searches.st_peptidecache(self._s_search, 'cachedpep2d',
                                           o_tfields = self.order_fields,
                                           gel = True) #Get only peptides from 2D Gels
            peptides = self._searches.get_query('cachedpep2d')
            if peptides:
                self.results = peptides
                super(PeptideResultPage, self).search() #Calls the search method in the super-super class
            else:
                self.results = None
                self._put_error( "Peptide Search 2D Error",
                                 "No results found for peptides from 2D Gels " \
                                 "matching *{0}*".format(self._o_s_search) )
        return self.results

    def post(self, request, *args, **kwargs):
        self.post_search_type = 'gel_peptide_search'
        self.post_search_field = 'gel_peptide_sequence'
        self.results_title = 'Search Results : Peptides from 2D Gels found in LymPHOS DB'

        return super(PeptideResultPage2D, self).post(request, *args, **kwargs)


class ProtResultPage(SearchResultPage):
    """
    SubClass of SearchResultPage, to search for proteins and generate the
    corresponding results view.
    """
    result_template = 'protein_result.html'
    paginated = True
    itemsxpage = 50

    def __init__(self, *args, **kwargs):
        #Pseudo_Constants for all derived instance classes:
        self._PROT_FIELDS = ('ac', 'phospho', 'is_decoy', 'name', 'chr', 'seq')

        #__init__ from Superclass:
        super(ProtResultPage, self).__init__(*args, **kwargs)

        self._search_validator = r"""[ /\w\.\-\*\?\)\(]"""
        self.allowed_order_fields = ('ac', 'name', 'chr',
                                     'gel', 'function', 'location', 'phospho')

    def search(self, rs_search=None):
        if rs_search:
            self.s_search = rs_search    #Set the string search of the object
        else:                            #Do the searches:
            self._searches.st_proteins(self._s_search, 'proteins',
                                       True, #CAUTION!: only_phospho set to True
                                       False, #CAUTION!: with_decoy set to False
                                       self.order_fields,
                                       *self._PROT_FIELDS)
            proteins = self._searches.get_query('proteins')
            if proteins:
                self.results = proteins
                super(ProtResultPage, self).search()
            else:
                self.results = None
                self._put_error("Protein Search Error",
                                "No results found for proteins matching " \
                                "*%s*" % self._o_s_search)
        return self.results

    def get_context_data(self, **kwargs):
        """
        Format results to be included in the context dictionary of the page
        object, so they can be rendered through the show() method in a result
        template.
        """
        # Call the base implementation first to get a context:
        context = super(ProtResultPage, self).get_context_data(**kwargs)
        # Return if there is no search result data to add:
        if not self.results:
            return context
        # Table header data:
        table_headers = ( {'html': 'Proteins',
                           'title': 'Protein Accession Number',
                           'width': '8%',
                           'order_by': 'ac'},
                          {'html': 'Protein Names',
                           'title': 'Diferent Protein Names found',
                           'width': '54%',
                           'order_by': 'name'},
                          {'html': 'Chr.',
                           'title': 'Chromosome where the gene for this protein is',
                           'width': '4%',
                           'order_by': 'chr'},
                          {'html': '&nbsp;#&nbsp;Phospho Sites',
                           'title': 'phosphorylation sites found',
                           'width': '6%',
                           'order_by': ''},
                          {'html': 'Unequivocal identification',
                           'title': 'Protein Uniquely identified by these Phospho Peptides?',
                           'width': '22%',
                           'order_by': ''},
# No more 2D Gel information
#                           {'html': '<img alt="2d_data" src="/files/images/2dgel.png" />',
#                            'title': 'Does it has been identified in some 2D Gel experiment?',
#                            'width': '6%',
#                            'order_by': ''},
# No more 2D Gel information
                         )
        context['table_headers'] = table_headers
        # Update the context with search results:
        context['proteins'] = self.results
        context['n_proteins'] = self.results.paginator.count
        return context

    def post(self, request, *args, **kwargs):
        if request.POST.has_key('protein_search'):
            self.post_search_type = 'protein_search'
            self.post_search_field = 'protein_text'
            self.results_title = 'Search Results : Phosphorylated Proteins found in LymPHOS DB'
        else:
            self.post_search_type = 'all_proteins_search'
            self.post_search_field = ''
            self.results_title = 'Browsing All Phosphorylated Proteins found in LymPHOS DB'

        return super(ProtResultPage, self).post(request, *args, **kwargs)


class ProtResultPage2D(ProtResultPage):

    results_title = 'Search Results : Proteins found in LymPHOS DB identified in some 2D Spot'

    def search(self, rs_search=''):
        if rs_search:
            self.s_search = rs_search    #Set the string search of the object
        else:                            #Do the searches:
            self._searches.st_proteins(self._s_search, 'proteins',
                                       True, #CAUTION!: only_phospho set to True
                                       False, #CAUTION!: with_decoys set to False
                                       self.order_fields,
                                       *self._PROT_FIELDS,
                                       data__spot__id__gt=0)  #Get only proteins identified at least in one spot of a 2D Gels
            proteins = self._searches.get_query('proteins')
            proteins = proteins.distinct() #Avoids duplicates due to the queries spanning backwards relations (see distinct() in django QuerySet API documentation)
            if proteins:
                self.results = proteins
                super(ProtResultPage, self).search() #Calls the search method in the super-super class
            else:
                self.results = None
                self._put_error("Protein Search Error",
                                "No results found for proteins matching " \
                                "*%s*" % self._o_s_search)
        return self.results

    def post(self, request, *args, **kwargs):
        self.post_search_type = 'gel_protein_search'
        self.post_search_field = 'gel_protein_text'
        self.results_title = 'Search Results : Proteins found in LymPHOS DB identified in some 2D Spot'

        return super(ProtResultPage2D, self).post(request, *args, **kwargs)


class PeptideViewPage(SearchReaultPagePOST2GET):
    """
    SubClass of SearchResultPage, to search for  a peptide and generate it's
    corresponding peptide data view.
    """

    result_template = 'peptide_view.html'
    results_title = 'Search Results : Peptide View'
    paginated = True
    itemsxpage = 5

    def __init__(self, *args, **kwargs):
        #Pseudo_Constants for all derived instance classes:
        self._DECIMALS = 2
        self._COLOR_NS = 'white'
        self._COLOR_P = '#57FF00'
        self._COLOR_PD = 'red'

        #__init__ from Superclass:
        super(PeptideViewPage, self).__init__(*args, **kwargs)

        self._search_validator = r"""[A-Ztsy]"""

    def search(self, rs_search=''):
        if rs_search:
            self.s_search = rs_search    #Set the string search of the object
        else:                            #Do the searches:
            self._searches.st_data('^({0})$'.format(self._s_search), 'peptides',
                                   only_phospho=False)
            peptides = self._searches.get_query('peptides')
            if peptides:
                self.results = peptides
                super(PeptideViewPage, self).search()
            else:
                self.results = None
                self._put_error("Peptide Search Error",
                                "No results found for peptide " \
                                "'{0}'".format(self._o_s_search))
        return self.results

    def get_context_data(self, **kwargs):
        """
        Format results to be included in the context dictionary of the page
        object, so they can be rendered in a peptide view template
        """
        # Call the base implementation first to get a context:
        context = super(PeptideViewPage, self).get_context_data(**kwargs)
        # Return if there is no search result data to add:
        if not self.results:
            return context

        #Temporal collections used:
        s_prots = set()

        #Iterates over data records (objects) in results (a Django relational
        #QuerySet with peptide-experiment data):
        for record in self.results.object_list:
            pep = record.peptide
            lt_ascores = [(asc,) for asc in record.get_ascores()]
            l_pos = record.get_psites()
            d_ascores = dict(zip(l_pos, lt_ascores))
#            record.htmlpeptide = self.format_pep(pep, d_ascores)
            lt_ascores = list()
            for _, asc in sorted(d_ascores.items()):
                if asc[0] == 1000.00:
                    t_asc_color = ('No sense', self._COLOR_NS)
                elif asc[0] > self._PEP_THRESHOLD:
                    t_asc_color = ('{1:.{0}f}'.format(self._DECIMALS , asc[0]),
                                   self._COLOR_P)
                else:
                    t_asc_color = ('{1:.{0}f}'.format(self._DECIMALS , asc[0]),
                                   self._COLOR_PD)
                lt_ascores.append(t_asc_color)
            record.lt_ascores = lt_ascores
            record.a_ncols = 7 - len(lt_ascores)
            record.scan = '%i - %i' % (record.scan_i, record.scan_f)
            # Redundant relational information:
            s_prots.update(record.proteins.all().values_list('ac', 'name'))
            if record.qdata:
                record.quantitative = True
                record.q_label = record.experiment.label.upper()
                if record.q_label == 'TMT':
                    ions = range(126, 131+1)
                elif record.q_label == 'ITRAQ':
                    ions = range(114, 117+1)
                record.q_ions = ions
                record.q_ncols = len(ions) + 1
                record.q_conds = record.experiment.quant_cond.split('|')
                record.q_intens = map(float, record.qdata.split('|'))
            record.exp = record.experiment.description
            record.exp_id = record.experiment.id
            record.spectrometer = record.experiment.instrument
            record.spectra = record.spectra_set.all().values_list('id')
        #
        #Get corresponding peptide record from PeptideCache model, for
        #quantitative information:
        self._searches.st_peptidecache('^({0})$'.format(self._s_search),
                                       only_phospho=False)
        pep = self._searches.get_query()[0]

        # Update the context dictionary with processed result data:
        context.update({
#                        'htmlpeptide': htmlpeptide,
                        'htmlpeptide': pep.htmlpeptide,
                        'peptide': self._o_s_search,
#                        'peptide_qdata': peptide_qdata,
                        'peptide_qdata': pep.quantitative,
                        'experiments': self.results,
                        'n_experiments': self.results.paginator.count,
                        'proteins': sorted(s_prots),
                        'threshold': self._PEP_THRESHOLD,
                        'decimals': self._DECIMALS,
                        })
        return context


class ProtViewPage(SearchReaultPagePOST2GET):
    """
    SubClass of SearchResultPage, to search for a protein and generate it's
    corresponding not paginated protein data view.
    """

    result_template = 'protein_view.html'
    paginated = False
    results_title = 'Search Results : Protein View'

    def __init__(self, *args, **kwargs):
        #Pseudo_Constants:
        self._CSS_PEP = 'secpepinprot'

        #__init__ from Superclass:
        super(ProtViewPage, self).__init__(*args, **kwargs)

        self._search_validator = r"""[A-Za-z0-9\-]"""

    def search(self, rs_search=''):
        if rs_search:
            self.s_search = rs_search    #Set the string search of the object
        else:                            #Do the searches:
            protein = self._searches.get_protein(self._s_search, 'protein')
            if protein:
                protein.htmlseq = list(protein.seq)
                protein.seq_nums = ''
                protein.named_psites = list()
                self.results = protein
                #No pagination stuff, so don't call super(ProtViewPage, self).search()
            else:
                self.results = None
                self._put_error("Protein Search Error",
                                "No results found for protein " \
                                "'%s'" % self._o_s_search)
        return self.results

    def get_context_data(self, **kwargs):
        """
        Format result to be included in the context dictionary of the page
        object, so it can be rendered in a protein view template.
        """
        # Call the base implementation first to get a context:
        context = super(ProtViewPage, self).get_context_data(**kwargs)
        # Return if there is no search result data to add:
        if not self.results:
            return context
        #
        protein = self.results
        # Format protein sequence as HTML in ``protein.htmlseq``:
        if protein.seq:
            AA_L2NAME = {'S': 'Serine', 'T': 'Threonine', 'Y': 'Tyrosine'}
            # - Iterates over ``protein.psitesfrompeps_w_metadata`` to get
            #   protein p-sites and phosphopeptides positions within the python
            #   ``protein.htmlseq`` list, and all protein p-site ascores:
            protseq_ppeptides_pos = set() #Protein phosphopeptides positions.
            protseq_psites2ascores = defaultdict(list) #Protein p-site ascores.
            for prot_psite, metadata in protein.psitesfrompeps_w_metadata.items():
                protseq_psite = prot_psite - 1 #From biological protein p-site sequence position to python list position.
                for data in metadata:
                    protseq_ppeptides_pos.add( (data['start_pos'] - 1, data['end_pos'] - 1) ) #From biological phosphopeptide sequence position in protein to python list position.
                    protseq_psites2ascores[protseq_psite].append( data['ascore'] ) #Aggregate all ascores for current protein p-site.
            # - Format protein p-sites according to ``protseq_psites2ascores``:
            for protseq_psite in sorted( protseq_psites2ascores.keys() ):
                self.results.htmlseq[protseq_psite] = self.results.htmlseq[protseq_psite].lower()
                psite_aa = protein.seq[protseq_psite]
                psite_aa = protein.seq[protseq_psite]
                psite_aa_pos = '%s %i' % (AA_L2NAME[psite_aa],
                                          protseq_psite + 1)
                protein.named_psites.append(psite_aa_pos)
                if max( protseq_psites2ascores[protseq_psite] ) > self._PEP_THRESHOLD:
                    smod = self._CSS_P
                else:
                    smod = self._CSS_PD
                self.format_aaseq(protseq_psite,
                                  '<span class=\"%s\" title=\"%s\">' \
                                  '%s</span>' % (smod, psite_aa_pos, '%s'))
            # - Format protein sequence according to matching phosphopeptides
            #   start and end positions:
            for pos_start, pos_end in protseq_ppeptides_pos:
                self.format_aaseq(pos_start,
                                  '<span class=\"' + self._CSS_PEP + '\">%s')
                self.format_aaseq(pos_end, '%s</span>')
            # - Final format of ``protein.htmlseq`` in AA blocks and lines:
            for grp in [(10, ''), (8, '&nbsp;')]:
                tmp_htmlseq = list()
                slc_start = 0
                for slc_end in range( grp[0], len(protein.htmlseq)+grp[0], grp[0] ):
                    tmp_htmlseq.append( grp[1].join(protein.htmlseq[slc_start:slc_end]) )
                    slc_start = slc_end
                protein.htmlseq = tmp_htmlseq
            protein.html_rows = len(protein.htmlseq)
            protein.htmlseq = '<br/>'.join(protein.htmlseq)
            # - Sequence numbers as HTML:
            protein.html_seqnums = '<br/>'.join( ['{0:04}'.format(i*80+1) for i
                                                  in range(protein.html_rows)] )
        #
        # Update the context with search results before returning:
        context['protein'] = protein
        return context

    def format_aaseq(self, pos, mod):
        self.results.htmlseq[pos] = mod % self.results.htmlseq[pos]
        return self.results.htmlseq


class QuickResultPage(SearchResultPage):             #TO_DO: Improve even more
    """
    SubClass of SearchResultPage, to search for peptides and/or proteins and
    generate the corresponding results view.
    """

    result_template='quick_result.html'
    paginated = False
    post_search_field = 'quick_text'
    results_title = 'Quick Search Results :'

    def __init__(self, *args, **kwargs):
        """
        Constructor
        """
        #__init__ from Superclass:
        super(QuickResultPage, self).__init__(*args, **kwargs)

        #Attributes and attribute initialization:
        self.results = False
        self._p_results = [PeptideResultPage(), ProtResultPage()]

    def search(self, rs_search=None):
        if rs_search:
            self.s_search = rs_search    #Set the string search of the object
        else:                            #Do the searches:
            for p_result in reversed(self._p_results):
                p_result.request = self.request
                p_result.args = self.args
                p_result.kwargs = self.kwargs
                p_result.itemsxpage = 50
                p_result.s_search = self._o_s_search #Send the original search string
                if p_result.errors:
                    self._errors.update(p_result.errors)
                    self._p_results.remove(p_result)
            if len(self._p_results) > 0:
                del self.errors
                self.results = True
        return self.results

    def get_context_data(self, **kwargs):
        """
        Extract results from results pages to be included in the context
        dictionary of the quick search page object, so they can be rendered
        in a quick search template.
        """
        # Call the base implementation first to get a context:
        context = super(QuickResultPage, self).get_context_data(**kwargs)
        # Update the context with search results:
        tmp_context_dict = context.copy()
        for p_result in self._p_results:
            context.update(p_result.get_context_data(**kwargs))
        context.update(tmp_context_dict)
        context['quicksearch'] = True
        context['search_string'] = self._o_s_search.strip('*')
        return context


class Spectra2File(DinamicFile): #TO_DO: Finish re-implementation.
    """
    Class for generating dynamic PNG or MGF files from a Spectra model data:
      Spectra model -> PNG or MGF file
    """

    file_type = 'PNG' #Defaults to PNG file-type.

    def __init__(self, *args, **kwargs):
        super(Spectra2File, self).__init__(*args, **kwargs)
        #
        self._s_search = None
        self._searches = logic.SearchDB()
        self.results = None
        self.x_mass = list()
        self.y_intens = list()
        self.peptide = ''
        self.charge = 1
        self.mass = 0

    # ``self.s_search`` is a read-write property:
    def _get_s_search(self):
        return self._s_search
    def _set_s_search(self, rs_search):
        self._s_search = rs_search
        if self.search():
            self._get_data_from_results()
            return self
        else:
            return None
    s_search = property(_get_s_search, _set_s_search)

    def search(self, rs_search=''):
        if rs_search: # Set the string search of the object:
            self.s_search = rs_search
        elif self._s_search: # Do the searches:
            # Get a concrete spectrum object from the spectra table:
            spectrum = self._searches.get_spectra(self._s_search, 'spectra')
            self.results = spectrum
        return self.results

    def _get_data_from_results(self):
        self.x_mass = self.results.x_mz
        self.y_intens = self.results.y_intensity
        self.peptide = self.results.data.pep_orig
        self.charge = self.results.data.charge
        self.mass = self.results.data.mass
        return self

    def _identify_ions(self):
        """
        efms = expected fragment masses
        imass = ion mass
        fmass = expected fragment mass
        """
        PRECISSION = 0.4

        ion_dict = get_ion_dict(self.peptide, self.charge)

        efms = sorted(ion_dict.keys())
        nions = len(self.x_mass)
        colors = ['k'] * nions
        annotations = [''] * nions
        #
        last_annot = 0
        for iidx, imass in enumerate(self.x_mass):   #ions
            if (imass - last_annot) < 1: continue
            fragment = ''
            for fmass in efms:                        #expected fragments
                if abs(imass-fmass) < PRECISSION:
                    fragment += ' %s' % ion_dict[fmass]
                    last_annot = imass
            #
            if fragment:
                #quito solo un fragmento. no es eficiente...
                efms.pop(0)
                #
                if 'y' in fragment:
                    color = 'red'
                elif 'b' in fragment:
                    color = 'blue'
                elif 'M' in fragment:
                    color = 'green'
                #
                annotations[iidx] = fragment
                colors[iidx] = color
        return (colors, annotations)

    def _mark_ion(self, axe, colors, annotations):
        """
        Annotate the ion type in espectra.
        """
        # Uses axes.annotate(s, xy, xytext, rotation) because axes.text(s,x,y)
        # doesn't work very well with zoom:
        annotate = axe.annotate

        for idx, annotation in enumerate(annotations):
            if annotation:
                annotate(annotation, xy=(self.x_mass[idx], self.y_intens[idx]),
                                     color=colors[idx], xytext=None,
                                     va ='bottom', rotation='vertical')

    def spectra(self, spec_id=None):
        """
        Generates a PNG image with the spectrum data selected by spec_id,
        ussing matplotlib, and returns it for in-line insertion.
        """
        # Search for the spectrum ID:
        if spec_id: self.s_search = spec_id
        # No spectrum found:
        if not self.results:
            raise Http404()

        fig = Figure(figsize=(10,4), dpi=75, frameon=False)
        fig.add_subplot(1, 1, 1)
        #fig.axes[0].set_ylim(bottom=0, top=max(self.y_intens))
        fig.axes[0].set_xlim(left=140, right=1800)

        zeros = [0] * len(self.x_mass)

        colors, annotations = self._identify_ions()
        fig.axes[0].vlines(self.x_mass, zeros, self.y_intens, color = colors,
                           label='False')
        self._mark_ion(fig.axes[0], colors, annotations)

        canvas = FigureCanvasAgg(fig)
        self.response['Content-Type'] = 'image/png'
        canvas.print_png(self.response)
        return self.response

    def mgf(self, spec_id=None):
        """
        Generates a MGF file with the spectrum data selected by spec_id, ussing
        specifications in http://www.matrixscience.com/help/data_file_help.html
        and returns it for download.
        """
        # Search for the spectrum ID:
        if spec_id: self.s_search = spec_id
        # No spectrum found:
        if not self.results:
            raise Http404()

        self.response['Content-Type'] = 'text/mgf'
        self.response['Content-Disposition'] = 'attachment; filename=spectrum-'\
                                               '{0}.mgf'.format(self.s_search)
        self.response.write('#Downloaded from LymPHOS v{0} on {1}\n'
                            '# http://www.lymphos.org\n\n'\
                            '# {2}\n'\
                            '# (cc) BY-NC-SA ( http://creativecommons.org/licenses/by-nc-sa/3.0/ )\n\n'\
                            'BEGIN IONS\n'\
                            'TITLE=Spectrum MS{3} Id {4} of Peptide {5} from LymPHOS v{0}\n'\
                            'PEPMASS={6:.3f}\n'\
                            'CHARGE={7}\n'.format(
                               settings.__VERSION__,
                               datetime.datetime.now().ctime(),
                               settings.__AUTHORS__,
                               self.results.ms_n, self._s_search, self.peptide,
                               self.mass,
                               str( abs(self.charge) )+'-' if self.charge < 0
                               else str(self.charge)+'+'
                                                  )
                            )
        for mass, intensity in zip(self.x_mass, self.y_intens):
            self.response.write( '{0:.3f} {1:.2f}\n'.format(mass, intensity) )
        self.response.write('END IONS')
        return self.response

    def get(self, request, *args, **kwargs):
        # Don't allow MGF downloads via GET method to avoid mass automatic
        # downloads:
        if self.file_type == 'MGF':
            return HttpResponseForbidden('Error 403 - Forbidden: Access is Denied')
        else:
            return self.post(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.filename = args[0]
        #
        # Get the Spectra ID from the file name:
        if self._name:
            spec_id = self._name.rpartition('-')[2]
        # Return the correct contents (HttpResponse) according to the requested
        # file-type (according to urls.py configuration for ``self.file_type``):
        if self.file_type == 'PNG':
            return self.spectra(spec_id)
        elif self.file_type == 'MGF':
            return self.mgf(spec_id)


class PeptideSpectra2MGF(FormsHtmlPage):
    """
    Class for generating single dynamic MGF files from all the Spectra models
    of a peptide:
      Peptide Spectra models -> MGF file
    """

    template_name = 'download_spectra_section.html'
    section_title = 'Download Spectra Data'

    def __init__(self, *args, **kwargs):
        super(PeptideSpectra2MGF, self).__init__(*args, **kwargs)
        #
        self.login = AccesControl(self.request, dl_errors=self._errors)
        #
        self.filename = '' #Filename to download. Contains the peptide sequence (Ex.: AAAsAA-spectra.mgf).
        self._s_search = None #String to search for, in regex format.
        self._o_s_search = None #Original string to search for (entered by the user).
        self._search_validator = r"""[A-Ztsy]"""
        self._searches = logic.SearchDB() #Search engine to use (LymPHOS_v1_5.lymphos.logic.SearchDB in this case).
        self.results = None #Results obtained in the search.

    def _sanitize_check(self, string, error_key=None):
        errors = ', '.join( set(re.sub( self._search_validator, '', string )) )
        if errors:
            error_msg = 'You searched for \' {0} \' , but \' {1} \' is/are not '\
                        'allowed in this search.'.format(string, errors)
            self._put_error(error_key, error_msg)
        return errors

    # ``self.s_search`` is a read-write property:
    def _get_s_search(self):
        return self._s_search
    def _set_s_search(self, rs_search):
        self._o_s_search = rs_search
        errors = self._sanitize_check(self._o_s_search, 'Search Text')
        if not errors:
            self._s_search = rs_search
            self.search()
        return self
    s_search = property(_get_s_search, _set_s_search)

    def search(self, rs_search=''):
        if rs_search:
            self.s_search = rs_search    #Set the string search of the object
        else:                            #Do the searches:
            self._searches.st_data('^({0})$'.format(self._s_search), 'peptides',
                                   only_phospho=False)
            peptides = self._searches.get_query('peptides')
            if peptides:
                self.results = peptides
            else:
                self.results = None
                self._put_error("Peptide Search Error",
                                "No results found for peptide " \
                                "'{0}'".format(self._o_s_search))
        return self.results

    def peptide_mgfs(self, peptide=None):
        """
        Generates a MGF file with all the spectra data for `peptide`, ussing
        specifications in http://www.matrixscience.com/help/data_file_help.html
        and returns it for download.
        """
        # Search for the peptide:
        if peptide: self.s_search = peptide
        # No peptide found:
        if not self.results:
            raise Http404()
        # Create an HttpResponse object to store and send the new MGF file:
        response = HttpResponse()
        # :class:`HttpResponse` object headers:
        response['Content-Type'] = 'text/mgf'
        response['Content-Disposition'] = 'attachment; filename={0}-'\
                                          'spectra.mgf'.format(self.s_search)
        # Write MGF file Header information:
        l_version = settings.__VERSION__
        response.write('#Downloaded from LymPHOS v{0} on {1}\n'
                       '# http://www.lymphos.org\n\n'\
                       '# {2}\n'\
                       '# (cc) BY-NC-SA ( http://creativecommons.org/licenses/by-nc-sa/3.0/ )\n'\
                       ''.format(l_version, datetime.datetime.now().ctime(),
                                 settings.__AUTHORS__)
                       )
        # Write each Spectrum data:
        for data_record in self.results:
            ms_n = data_record.ms_n
            peptide = data_record.pep_orig
            charge = data_record.charge
            mass = data_record.mass
            for spectrum in data_record.spectra_set.all():
                sid = spectrum.id
                x_mass = spectrum.x_mz
                y_intens = spectrum.y_intensity
                response.write('\nBEGIN IONS\n'\
                               'TITLE=Spectrum MS{1} Id {2} of Peptide {3} from LymPHOS v{0}\n'\
                               'PEPMASS={4:.3f}\n'\
                               'CHARGE={5}\n'.format(l_version, ms_n, sid,
                                                     peptide, mass,
                                                     str( abs(charge) )+'-'
                                                     if charge < 0
                                                     else str(charge)+'+'
                                                     )
                               )
                for mass, intensity in zip(x_mass, y_intens):
                    response.write( '{0:.3f} {1:.2f}\n'.format(mass, intensity) )
                response.write('END IONS\n')
        return response

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context:
        context = super(PeptideSpectra2MGF, self).get_context_data(**kwargs)
        # Verifies if the user has an active session:
        logged = self.login.check_current_login()
        # Update the context:
        if not logged:
            context['ots'] = self.login.generate_OTS() #Time-limited One Time Salts.
        context.update({'login_action': '/download_all_mgfs/' + self.filename,
                        'logged': logged})
        return context

    def get(self, request, *args, **kwargs):
        # Don't allow MGF downloads via GET method to avoid mass automatic
        # downloads:
        return HttpResponseForbidden('Error 403 - Forbidden: Access is Denied')

    def post(self, request, *args, **kwargs):
        self.login.request = self.request = request
        self.filename = args[0]
        # In the middle of a log-in process:
        if 'log-in' in request.POST:
            self.login.login(log_key='Log-in') #Verify log-in credentials.
        # Get the peptide sequence from the file name (Ex.: AAAAsAA-spectra -> AAAAsAA):
        peptide = self.filename.rpartition('-')[0]
        # Search for the peptide:
        self.s_search = peptide
        # Too few spectra or already loged-in:
        if self.results and ( self.results.count() <= PeptideViewPage.itemsxpage or self.login.check_current_login() ):
            # Return the download contents (an HttpResponse with a MGF file):
            return self.peptide_mgfs()
        # Let the superclass post() method do its stuff, and return the
        # HttpResponse object generated:
        return super(PeptideSpectra2MGF, self).post(request, *args, **kwargs)


class QuantSummaryPage(SearchReaultPagePOST2GET):
    """
    SubClass of SearchResultPage, to summarice quantitative data for a peptide
    in each quantitative experiment group (supra-experiment) that raised
    peptide quantification data.
    """

    result_template = 'quantitative_summary.html'
    paginated = False
    results_title = 'Results : Quantitative Summary'

    def __init__(self, *args, **kwargs):
        #Pseudo_Constants for all derived instance classes:
        self._Q_FIELDS = ('unique_pep', 'expgroup__name', 'dballquanttimes',
                          'dbavgratios', 'dbstdv', 'dbavgregulation')
        self._REG2IMG_COLOR_TXT = { 1: ('up_reg_{0}.png', 'green', 'up-regulaion'),
                                    0: ('no_reg_{0}.png', 'grey', 'no-change'),
                                   -1: ('down_reg_{0}.png', 'red', 'down-regulation')}
        self._DECIMALS = 2

        #__init__ from Superclass:
        super(QuantSummaryPage, self).__init__(*args, **kwargs)

        self._REGEX_REPLACE.update({'B': '[BND]',
                                    'Z': '[ZEQ]',
                                    'J': '[JIL]'})
        self._search_validator = r"""[A-Ztsy]"""

    def search(self, rs_search=None):
        """
        Search for a peptide (assigned to self._s_search through self.s_search)
        in any quantitative supra-experiment.
        """
        if rs_search:                   #Set the string search of the object
            self.s_search = rs_search
        else:                                               #Do the searches
            self._searches.st_qpepcond('^({0})$'.format(self._s_search), None,
                                       'qpepexpgroup', *self._Q_FIELDS)
            qpepexpgroup = self._searches.get_query('qpepexpgroup')
            if qpepexpgroup:
                self.results = qpepexpgroup
            else:
                self.results = None
                self._put_error("Quantitative Peptide Search Error",
                                "No results found for peptides matching " \
                                "'{0}'".format(self._o_s_search))
        return self.results

    def get_context_data(self, **kwargs):
        """
        Summarize statistical quantitative peptide data from QuantPepExpCond
        for each supra-experiment in witch the search peptide is found, to
        include it in the context dictionary of the page object, so it can be
        rendered through the show() method in a result template.
        """
        # Call the base implementation first to get a context:
        context = super(QuantSummaryPage, self).get_context_data(**kwargs)
        # Return if there is no search result data to add:
        if not self.results:
            return context
        #
        # Iterates over QuantPepExpCond objects in self.results (a Django
        # QuerySet) to get all the needed data and format it:
        for record in self.results:
            record.name = '{0} (Cond. ID: {1})'.format(record.expgroup.name,
                                                       record.expgroup.id)
            time2reg_values = record.time_values()
            for time in time2reg_values.keys():
                d_regulation = time2reg_values[time][0]
                reg_fit = d_regulation['confidence']
                if reg_fit > 0.80:
                    img_fit = 4 #Full colored arrow
                    reg_txt_fit = '{0} (strong trend)'
                elif reg_fit > 0.60:
                    img_fit = 3
                    reg_txt_fit = '{0} (moderate trend)'
                elif reg_fit > 0.40:
                    img_fit = 2
                    reg_txt_fit = '{0} (weak trend)'
                elif reg_fit > 0.20:
                    img_fit = 1
                    reg_txt_fit = '{0} (very weak trend)'
                elif reg_fit > -1:
                    img_fit = 0 #Empty arrow
                    reg_txt_fit = '{0} (ambiguous trend)'
                else:
                    img_fit = "single"
                    reg_txt_fit = '{0} (caution, single spectrum)'
                #
                reg_value = d_regulation['regulation']
                d_regulation['img'] = self._REG2IMG_COLOR_TXT[reg_value][0].format(img_fit)
                d_regulation['color'] = self._REG2IMG_COLOR_TXT[reg_value][1]
                d_regulation['text'] = reg_txt_fit.format( self._REG2IMG_COLOR_TXT[reg_value][2] )
                d_regulation['confidence'] = d_regulation['confidence']
            record.sorted_time_values = sorted( time2reg_values.items() )
            expgroup_data = record.dataquant_set.filter(data__experiment__expgroup__id=record.expgroup_id)
            n_exp = expgroup_data.values('data__experiment').annotate( Count('data__experiment') ) #Let SQL to group related dataquant records by experiment
            record.n_exp = n_exp.count()
            record.n_spectra = expgroup_data.count()
            record.arrow_tail = len(record.quanttimes)

        # Format the peptide name as html acording to data in PeptideCache model:
        self._searches.st_peptidecache('^({0})$'.format(self._s_search))
        pep = self._searches.get_query()[0]
        psites = pep.get_psites()
        d_ascores = dict( zip(psites, [(each,) for each in pep.get_ascores()]) )
        htmlpeptide = self.format_pep(pep.peptide, d_ascores)

        # Update the context with search results:
        context.update( {'conditions': self.results,
                         'htmlpeptide': htmlpeptide,
                         'n_psites': len(psites),
                         'peptide': self._o_s_search,
                         'decimals': self._DECIMALS,
                         'reg_color': 'orange'} )
        return context


class QuantViewPage(SearchReaultPagePOST2GET):
    """
    SubClass of SearchResultPage, to summarize quantitative data for a peptide
    in each quantitative experiment inside a group (supra-experiment) that
    raised peptide quantification data.
    """

    result_template = 'quantitative_view.html'
    paginated = False
    results_title = 'Results : Detailed Quantitative Data'

    def __init__(self, expgroup_id=None, *args, **kwargs):
        # Pseudo_Constants for all derived instance classes:
        self._QD_FIELDS = ('id', 'experiment')
        self._DECIMALS = 2
        self._REG2COLOR = { 1: 'green',
                            0: 'black',
                           -1: 'red'}

        self.expgroup_id = expgroup_id

        # __init__ from Superclass:
        super(QuantViewPage, self).__init__(*args, **kwargs)

        self._search_validator = r"""[A-Ztsy]"""

    def search(self, rs_search=None):
        """
        Search for a peptide (assigned to self._s_search through self.s_search)
        in the quantitative supra-experiment defined in self.expgroup_id.
        """
        if rs_search:                   #Set the string search of the object
            self.s_search = rs_search
        else:                                               #Do the searches
            self._searches.st_data('^({0})$'.format(self._s_search), 'qpepexp',
                                   True,    #CAUTION!: only_phospho set to True
                                   *self._QD_FIELDS,
                                   experiment__expgroup__id = self.expgroup_id,
                                   qdata__gt = '', dataquant__id__gt = 0)
            qpepexp = self._searches.get_query('qpepexp')
            if qpepexp:
                self.results = qpepexp
            else:
                self.results = None
                self._put_error("Quantitative Peptide Search Error",
                                "No results found for peptides " \
                                "matching ' {0} ' in experiment group " \
                                "{1}".format(self._o_s_search, self.expgroup_id))
        return self.results

    def get_context_data(self, **kwargs):
        """
        Statistical quantitative peptide data from DataQuant for each
        experiment in witch the search peptide is found within the supra-
        experiment, to include it in the context dictionary of the page object,
        so it can be rendered through the show() method in a result template.
        """
        # Call the base implementation first to get a context:
        context = super(QuantViewPage, self).get_context_data(**kwargs)
        # Return if there is no search result data to add:
        if not self.results:
            return context

        #Iterates over record Data objects in self.results (a Django QuerySet)
        #to get all the needed data:

        #1.- Agregate data in d_experiments, using a dictionary (a defaultdict)
        d_experiments = defaultdict(lambda: {'data_list': list(),
                                             'n_spectra': 0,
                                             'n_rows': 3})
        record = None
        for record in self.results:
            if record.experiment.iscontrol():
                continue
            exp_id = record.experiment.id #Get the exp_id key (the experiment.id of each record) to agregate on
            if exp_id not in d_experiments.keys(): # The first time we see this exp_id key:
                d_experiments[exp_id]['id'] = exp_id
                d_experiments[exp_id]['name'] = record.experiment.description
                qtimes = record.dataquant.quanttimes
                d_experiments[exp_id]['times'] = qtimes
                d_experiments[exp_id]['arrow_tail'] = len(qtimes)
            # And, every time we see the same exp_id key:
            d_time_values = record.dataquant.time_values()
#             for tkey in d_time_values: #Replaces regulation values (-1, 0, 1) with colors for a nicer view
#                 d_time_values[tkey][0][0] = self._REG2COLOR[ d_time_values[tkey][0][0] ]
            d_experiments[exp_id]['data_list'].append(sorted( d_time_values.items() ))
            d_experiments[exp_id]['n_spectra'] += record.spectra_set.count()
            d_experiments[exp_id]['n_rows'] += 1
        #2.- Append sorted data from d_experiments dictionary to l_experiments
        #list
        l_experiments = [d_experiments[exp_id] for exp_id in
                         sorted(d_experiments.keys())]

        #Format the peptide name as html acording to data in PeptideCache model:
        self._searches.st_peptidecache('^({0})$'.format(self._s_search))
        pep = self._searches.get_query()[0]
        psites = pep.get_psites()
        d_ascores = dict( zip(psites, [(each,) for each in pep.get_ascores()]) )
        htmlpeptide = self.format_pep(pep.peptide, d_ascores)

        # Update the context with search results:
        context.update({'condition': record.experiment.expgroup.name,
                        'condition_id': self.expgroup_id,
                        'experiments': l_experiments,
                        'htmlpeptide': htmlpeptide,
                        'n_psites': len(psites),
                        'peptide': self._o_s_search,
                        'decimals': self._DECIMALS})
        return context

    def get(self, request, *args, **kwargs):
        """
        Overriding :method:`get` to store the experimental group ID (in
        args[2]) as ``self.expgroup_id``
        """
        self.expgroup_id = int(args[2])
        return super(QuantViewPage, self).get(request, *args, **kwargs)

