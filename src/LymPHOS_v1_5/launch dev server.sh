#!/bin/sh

./manage.py runserver localhost:8181 &
echo Waiting Django server...
sleep 5
echo Trying to open http://localhost:8181 in default web browser...
xdg-open http://localhost:8181