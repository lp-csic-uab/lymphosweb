# -*- coding: utf-8 -*-
"""
:synopsis:   Basic classes for LymPHOS_v1_5 project views

:created:    2011-01-14

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2011-2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '2.0'
__UPDATED__ = '2016-07-04'


#===============================================================================
# Imports
#===============================================================================
#DJango imports:
from django.views.generic.base import View, TemplateView
from django.http import HttpResponse, HttpResponseRedirect
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.conf import settings

#Application imports:
from lymphos import logic
from lymphos.utilities import NoPaginator

#Python core imports:
from collections import defaultdict
import re


#===============================================================================
# Views basic classes definitions
#===============================================================================
class BasicHtmlPage(TemplateView):
    """Raw basic class for Pseudo-static web-page views management"""

    template_name = 'base.html' #The template name to use.
    section_title = '' #The section title (to add to the title of the page).

    def __init__(self, *args, **kwargs):
        """
        Simply adds a default null (None) :attr:``request`` to the instance.
        """
        super(BasicHtmlPage, self).__init__(*args, **kwargs)
        self.request = getattr(self, 'request', None)

    def get_context_data(self, **kwargs):
        """
        :returns: a default base context dictionary for the render, updated
        with any 'extra_context' dictionary present in ``self.kwargs``
        """
        # Call the base implementation first to get a context:
        base_context = super(BasicHtmlPage, self).get_context_data(**kwargs)
        # Add a Default base context:
        base_context.update( {'section_title': self.section_title,
                              'base_template': 'base.html',
                              'css_sheet': 'default.css',
                              'APPNAME': settings.APPNAME,
                              'CELLTYPE': settings.CELLTYPE,
                              } )
        # Update with optional extra context dictionary defined in
        # ``urlpatterns`` after :method:`as_view` call and passed in
        # ``self.kwargs`` as `extra_context` key:
        base_context.update( self.kwargs.get( 'extra_context', dict() ) )
        return base_context


class FormsHtmlPage(BasicHtmlPage):
    """
    Basic class for web-page Forms views management.

    So it implements the use of Django (1.2 and up) csrf (Cross Site Request
    Forgery) middleware protection: in the :method:`post`, it uses
    :method:`render_to_response`; so each POST form in the corresponding
    template should contain the tag {% csrf_token %}
    There is also an implementation of a basic error logging system
    (:property:`errors`, :method:`_put_error`).
    """

    def __init__(self, *args, **kwargs):
        super(FormsHtmlPage, self).__init__(*args, **kwargs)
        # Prepare context attribute for form errors:
        self._errors = defaultdict(list) #TO_DO: Change this to a :class:`LoggerCls` object

    def _put_error(self, key='Error', error_msg=''):
        """
        Add the `error_msg` to the ``self._errors`` defaultdict.

        :param str key: error type.
        :param str error_msg: error message.
        """
        if not error_msg:
            error_msg = 'Error in {0}'.format(str(self))
        self._errors[key].append(error_msg)

    #self.errors is a read_only property:
    def get_errors(self):
        """errors getter"""
        return self._errors
    def reset_errors(self):
        """errors deleter"""
        self._errors = defaultdict(list)
    errors = property(fget = get_errors, fdel = reset_errors)

    def get_context_data(self, **kwargs):
        """
        :returns: a default base context dictionary updated with form error
        information.
        """
        # Call the base implementation first to get a context:
        context = super(FormsHtmlPage, self).get_context_data(**kwargs)
        # Update the context with error data (``self._errors`` contents):
        context['errors'] = self._errors.items()
        del self.errors #Clear the ``self._errors`` defaultdict.
        return context

    def post(self, request, *args, **kwargs):
        """
        Default handler method for POST requests.

        :returns: an :class:`HttpResponse` object for a ``context`` dictionary
        (from :method:`self.get_context_data`) rendered against a template 
        (using :method:`self.render_to_response` from superclass).
        """
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def put(self, request, *args, **kwargs):
        """
        Default dummy handler method for PUT requests. PUT is a valid HTTP verb
        for creating (with a known URL) or editing an object, but browsers only
        support POST for now.
        """
        return self.post(request, *args, **kwargs)


class SearchResultPage(FormsHtmlPage):
    """
    Abstract SubClass of FormsHtmlPage; use it to subclass in searching classes
    that also generate the corresponding result views.
    """

    template_name = 'result_section.html'
    result_template = '' #Result template to import into the result section.
    irs_search = '' #Input String to search for.
    paginated = False #Indicates whether the formated results must or mustn't be paginated
    itemsxpage = None #Items per page for the Paginator object in abstract :method:`search`.
    post_search_field = '' #POST search field value to search for.
    search_default = '*'
    results_title = 'Search Results'

    def __init__(self, *args, **kwargs):
        # Pseudo_Constants for all derived instance classes:
        self._PEP_THRESHOLD = 19 #Minimum Q-Ascore threshold.
        self._CSS_P = 'secP' #CSS class for p-sites over the QAscore threshold.
        self._CSS_PD = 'secPdoubt' #CSS class for p-sites below the QAscore threshold.
        self._REGEX_REPLACE = {'\\*': '.+', #Basic replacement dictionary for search strings regex formating (:method:`regex_format`)
                               '\\?': '.',
                               '\\-': '-'}
        # __init__ from superclass:
        super(SearchResultPage, self).__init__(*args, **kwargs)
        # Attributes and attribute initialization:
        self._search_validator = '.'
        self._s_search = None #String to search for, in regex format.
        self._o_s_search = None #Original string to search for (entered by the user).
        self._order_fields = tuple() #Fields to order the search by.
        self.allowed_order_fields = tuple() #Fields allowed to order the search by.
        self._searches = logic.SearchDB() #Search engine to use (LymPHOS_v1_5.lymphos.logic.SearchDB in this case).
        #
        self.results = None #Results obtained in the search.
        #
        self.post_search_type = None
        if self.irs_search: self.s_search = self.irs_search

    # self.s_search is a read-write property:
    def get_s_search(self):
        return self._o_s_search
    def set_s_search(self, rs_search):
        self._o_s_search = rs_search
        errors = self._sanitize_check(self._o_s_search, 'Search Text')
        if not errors:
            self._s_search = self.regex_format(rs_search)
            self.search()
    s_search = property(get_s_search, set_s_search)

    def _sanitize_check(self, string, error_key=None):
        errors = ', '.join( set(re.sub( self._search_validator, '', string )) )
        if errors:
            error_msg = 'You searched for \' {0} \' , but \' {1} \' is/are not '\
                        'allowed in this search.'.format(string, errors)
            self._put_error(error_key, error_msg)
        return errors

    def regex_format(self, rs_search):
        """
        Protection against regular expressions and formating of `rs_search`
        string according to ``self._REGEX_REPLACE`` dictionary.
        """
        rl_search = list()
        rkeys = self._REGEX_REPLACE.keys()
        for each in list(rs_search):
            each = re.escape(each) #Protection against regular expressions.
            if each in rkeys:
                each = self._REGEX_REPLACE[each]
            rl_search.append(each)
        return ''.join(rl_search)

    # self.order_fields is a read-write property:
    def get_order_fields(self):
        return self._order_fields
    def set_order_fields(self, order_fields):
        if self._order_fields != tuple(order_fields):
            self._order_fields = list()
            # Only accept allowed fields (in ``self.allowed_order_fields``):
            for field in order_fields:
                if field.startswith('-'):
                    cmp_field = field[1:] #Only compare against ascending order fields (the defaults for a django order_by() queryset method)
                else:
                    cmp_field = field
                if cmp_field in self.allowed_order_fields:
                    self._order_fields.append(field)
            self._order_fields = tuple(self._order_fields)
    order_fields = property(get_order_fields, set_order_fields)

    def format_pep(self, pep, d_ascores): #REFACTORIZE: put it in models.py? -> Some work done: PeptideCache.htmlpeptide and Data.htmlpeptide
        """
        Format a peptide string as partial html, where phosphorylation sites
        (p-sites) are market accordingly to the maximum QAscore value from all
        QAscores obtained for those p-sites in different experiments.
        """
        l_pep = list(pep)
        for pos in d_ascores:
            pos_class = self._CSS_P if max(d_ascores[pos]) > self._PEP_THRESHOLD \
                        else self._CSS_PD #TO_DO: deal with cases where d_ascores.values() are no iterables
            l_pep[pos] = '<span class=\"%s\">%s</span>' % ( pos_class,
                                                            l_pep[pos] )
        return ''.join(l_pep)

    def search(self, rs_search=None):
        """
        Do the corresponding search using the search classes in logic.py
        module, according to s_seachr property, and store the obtained results
        in self.result as a SearchQuery or a list (containing models objects,
        ...) of the results to be processed by other methods.
        Basic functionality for pagination is implemented here, so subclasses
        can use it overriding this method and calling super() at the end.
        """
        # Basic pagination stuff (call super().search() after real search stuff
        # to use it):
        if self.paginated:
            paginator = Paginator(self.results, self.itemsxpage) #Creates the paginator object from self.results
            #Make sure page request exists. If not, deliver first page:
            page = int(self.request.POST.get('page', '1'))
            #If page requested is out of range, deliver last page of results:
            try:
                self.results = paginator.page(page)
            except (EmptyPage, InvalidPage):
                self.results = paginator.page(paginator.num_pages)
        else:
            self.results = NoPaginator(self.results)

        # Implement real search stuff in Subclasses!

        # The rest of search stuff is Subclass Responsibility.

        # Recommended: return self.results
        return self.results

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context:
        context = super(SearchResultPage, self).get_context_data(**kwargs)
        # Indicate specific class formating context keys to use:
        context.update( {'result_template': self.result_template,
                         'section_title': self.results_title,
                         'css_sheet': 'default_nosidebar.css',
                         'class_search': r'current'
                         } )
        # Add current search information for search POST re-enters for
        # paginated and sortable views:
        if self.post_search_type:
            context.update( {'search_type': self.post_search_type,
                             'search_input': self.post_search_field,
                             'search_value': self.s_search,
                             'search_order_by': self.order_fields
                             } )
        return context

    def post(self, request, *args, **kwargs):
        """
        Handler method for search POST requests.

        :returns: a :class:`HttpResponse` object with the search results.
        """
        # Get the search data and do the search:
        self.order_fields = request.POST.get('order_by', '').split(',')
        self.s_search = request.POST.get( self.post_search_field,
                                          self.search_default )

        return super(SearchResultPage, self).post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        """
        Handler method for search GET requests.

        :returns: a :class:`HttpResponse` object with the search results.
        """
        if args:
            self.post_search_type = None
            self.s_search = args[1]
            return super(SearchResultPage, self).get(request, *args, **kwargs)
        else:
            return HttpResponseRedirect('/search/')


class SearchReaultPagePOST2GET(SearchResultPage):
    """
    Abstract SubClass of `SearchResultPage` that redirect :method:`post` calls
    to :method:`get` calls; use it to subclass in searching classes that do not
    get search parameters through POST requests.
    """

    def post(self, request, *args, **kwargs):
        """
        The real search parameters are in ``self.args`` and/or
        ``self.request.GET`` -> redirect to :method:``self.get``.

        So this will ONLY be called from same-page pagination bar (for the
        pagination stuff).
        """
        self.post_search_type = None #Ensure no POST search parameters will be added to context in :method:`get_context_data`
        return self.get(request, *args, **kwargs)


class DinamicFile(View):
    """
    Abstract class for generating dynamic files:
      FileName and possibly other Dynamic data -> Some file
    """

    def __init__(self, *args, **kwargs):
        """
        Simply adds a null request attribute to the instance
        """
        super(DinamicFile, self).__init__(*args, **kwargs)

        self._name = ''
        self._ext = ''

        self._search_validator = '.'

        self.request = getattr(self, 'request', None)
        self.response = None
        self.saved = False

    def _sanitize_check(self, string, error_key=None):
        errors = ', '.join( set(re.sub( self._search_validator, '', string )) )
#         if errors:
#             error_msg = 'You searched for \' {0} \' , but \' {1} \' is/are not '\
#                         'allowed in this search.'.format(string, errors)
#             self._put_error(error_key, error_msg)
        return errors

    # ``self.filename`` is a read-write property:
    def _get_filename(self):
        filename = '{0}.{1}'.format(self._name, self._ext)
        return filename
    def _set_filename(self, filename):
        self._name, _, self._ext = filename.rpartition('.')
        self.response = HttpResponse()
    filename = property(_get_filename, _set_filename)

    def get(self, request, *args, **kwargs):
        self.filename = args[0]

        # Implement real get stuff in Subclasses!

        # The rest of get stuff is Subclass Responsibility

        # Return ``self.response``
        return self.response

    def post(self, request, *args, **kwargs):
        """
        The real parameters are in ``self.args`` and/or ``self.request.GET`` ->
        redirect to :method:``self.get``.
        So this should only be called from download forms.
        """
        return self.get(request, *args, **kwargs)

    def save(self, request=None, filename=None):
        if request: self.request = request
        if filename: self.filename = filename
        with open(self.filename, 'wb') as io_file:
            io_file.write(self.response.content)
        self.saved = True
        return self

