SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';
SET AUTOCOMMIT = 0;

USE `lymphos_lymphos2allpepd` ;

-- -----------------------------------------------------
-- Data for table `lymphos_expgroup`
-- -----------------------------------------------------
INSERT INTO `lymphos_expgroup` (`id`, `name`, `description`) VALUES (-1, 'No data', '');

COMMIT;

UPDATE `lymphos_expgroup` SET `id`=-1 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_experiments`
-- -----------------------------------------------------


INSERT INTO `lymphos_experiments` (`id`, `expgroup_id`, `dbsub_exps`, `name`, `dbmiape_ids`, `description`, `instrument`, `label`, `quant_cond`) VALUES (-1, -1, '', 'No condition', '', '', '', '', '');

COMMIT;


UPDATE `lymphos_experiments` SET `id`=-1 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_quantpepexpcond`
-- -----------------------------------------------------


INSERT INTO `lymphos_quantpepexpcond` (`id`, `unique_pep`, `expgroup_id`, `dballquanttimes`, `dbavgratios`, `dbstdv`, `dbavgregulation`, `dbttestpassed95`, `dbttestpassed99`, `dbsdgreatersdmeanx3`, `dbdanteravgratios`, `dbdanterpvalues`, `dbdanteradjpvalues`) VALUES (-1, 'No quantitative data', -1, '', '', '', '', '', '', '', '', '', '');

COMMIT;


UPDATE `lymphos_quantpepexpcond` SET `id`=-1 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_gelimage`
-- -----------------------------------------------------


INSERT INTO `lymphos_gelimage` (`id`, `twodgel_id`, `name`, `hasjpg`, `dye`, `condition`, `slope_x`, `intercept_x`, `slope_y`, `intercept_y`) VALUES (-1, -1, 'No Image', 0, '', '', 0.0, 0.0, 0.0, 0.0);

COMMIT;


UPDATE `lymphos_gelimage` SET `id`=-1 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_twodgel`
-- -----------------------------------------------------


INSERT INTO `lymphos_twodgel` (`name`, `dbconditions`, `experiment_id`, `id`) VALUES ('NO-GEL', '', -1, -1);

COMMIT;


UPDATE `lymphos_twodgel` SET `id`=-1 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_spot`
-- -----------------------------------------------------


INSERT INTO `lymphos_spot` (`twodgel_id`, `x`, `y`, `area`, `pI`, `MW`, `ratio_ab`, `anova`, `fold`, `max_cv`, `id`, `ingel_id`) VALUES (-1, 0, 0, 0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, -1, 0);

COMMIT;


UPDATE `lymphos_spot` SET `id`=-1 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_users`
-- -----------------------------------------------------


INSERT INTO `lymphos_users` VALUES ('77b1e578fbd40af0945162845ea703be6e612c76c39c1748c2f5ea45128aba3a','cJ1bDwrTvdrxEgTWmhbzvfHrAKCZcKzizdnABwTMz2y4DtvkterNANvwqxj6DgnInKHOttvHvIT3\nAKvjs3fiq1HlDc8ZsuzVn0L6EuKYCuvtnM9wDKv3sgrdvunut3HNve4TcJ1bsMfXDxrkqM5bs1P0\nr2W1CsS0v2jgwgDds1a3r2vgB3z2zMO4z0nRztLhvvu1vGPKq0fJDxjbrNveAISZneHywKLhmdnt\nmLj6tLfVAvPysdrJBKfKqu5qmfjettbnEJbNk1Dznu1LEdbjtfHJotL2qLHquxCYDM12wKvM\n','cJ09D21sBe5rk0WWEeDNEK00qvrolqPcBuzwyKm0sJD5wK53A2PnCMrIotnTzKPrENb0sxy1rgjZ\nt2TOmxjX\n','cJ1VmcS2uKn3vdHyvxbyEwyXwxHSweXkz2T5ogHcEtbpmKK2sdbVouO2u0rLufLzuM04n1vPstm5\nBKXetJvnve4TcJ09qwXkqvy0zhLlytr5DJakk2nWEfvLudrxExfKA1DzsdrIwdbVAgH6n0DvD3GZ\nz0jrruH6EKzgBePYme53surooe5nC2Xrs2SYuejXtuvRBLHPDZu5teLIvZbHnW==\n','2016-07-13 16:17:52','2016-07-13 16:17:52'),('7b04fb7b8b7e5ce3d082ba9f3820da641e3d697933b11b1b04c6115dbfb7f227','cJ1bEgTVqs8YENuZvKXmyLaZA09ycNb5Dhm4u1vUm0DRudjLrwjxyJzNCJiYuKvUv1bMy0q5suuR\nntb2nvvRquvkntziuhq3sxntBMe2t2q3zLbXmZnesdbOEe5QtxPrve4TcJ09zZu1EdzIqujLAfnN\nyuDOA3rJrwLftMvSt3zIqwTJwungvJmZmNfbk2rPz0iXC1CYou8kzgXgDMXLqNfUy29enMLVm0zZ\nqxbKzuPnDtrMttf3wvDQqKuXBu1ltxHJre5Oqxa0qLrguwvYB2WVzZjAuefjmev3sKjnn3z6lZLd\noq==\n','cJ09z2H3yvqYwxDRAuzrve8XA1rolqP4r0TbBgzhqMLRwu93vuroAfbAodzQsgC5ExbUBgfTB1DJ\nvMnhDez5\n','cJ1rAgHXsKrAy0XOD3j5EhC2ruzRCu45t244whvNBLLiv0zPreTWsI84DurQotbkrKOXywDwB0S4\nsvvestjws2fRturnEMDutI0kqKzfl2u3CvjQBwLfvuSXCKDUseWRrxr4cJzNk2yRoxbJAZC2zxH2\nzee2sJDtnLDyqNORrLrHELvIuw11t2HZEu0YqxPnEKfmstjhsLjcztHMsJfpyJyRDKfesgH1l0SY\nzvDntgu=\n','2016-07-13 16:17:52','2016-07-13 16:17:52'),('ccfc417b15d776ad99f817bbfec3bf56b559c723d92dec0f0d5dfd56caa9a69a','cJ1rzY96ouTAEwndvMr1vtHJm1jOcMrym0P2u2XHrKPIDunYneXeuuDlnNzwzfD5z2frl3nhCND6\nwLKXrMrjrhLzzdDYq053zvi4AM1Zy1u2yw5IBhzUAevAk0fetZnjve4TcJ09qujHwK9gEhrwwhGY\novbqrhq1seDVBKXvt1zQs3jSowzNzdbdsJK4vxrncKz1Chy2lZLWrMLUBMnHsuKZufjjwfHtwxnc\nn2K3A0TsCfvptevQk25lr3juy0XAtNDbEK0XtMDUrgiWvLniqKi1rujJEfzMnxjik2G=\n','cJ09uvvfBNvYmvjnuK5Nve14tvrolqP2zKfjz3aRm2WZve53quroAxjfqLPYogXjttbWtNzbu05f\nofPtB3fn\n','cJ1NrfvdmMnmr0WVmKzXC014wNPoEefutI0kpvvdvuv1owfiChPurMCYsMjqre0YuxP5y0HArLnZ\nztu0qM10uZzysg0VAvK=\n','2016-07-13 16:17:52','2016-07-13 16:42:19'),('d506645253917d1c3ed96912e1e10c7d7b6ed39e67f00e001742af0733b11347','cJ1JzgfoA2jeEJvyAKzvAK9PAKngcMXMq2TKwtzqnYTrv0fnD0reEvfsqMrhuKXkuxG1DK5ewu1h\nDKfhAg5RrfLgBtzdqxnRtw5qv3b2v2mYAg0Vww03nZrQsK5etZvnve4TcJ1Vnvrwn0rpmunhnuTl\nstK0DtmREty3CdbNmNzzzK9xChDdANjqywykr2vunKnkkZfkyvzlrhPvrY9bvu5gme8YBdDnueXP\nwgrlqwnYvvyXq0vhDfbYuKvwqLrettfrveT6uvL3ofjmy1PHtZLPnNzrwJnXzW==\n','cJ09z3yZzfLvswTTtdnxve0YsvrolqO9pxDMzdb6EfDswNaVuKrnm1fus25LBg9XmxvnEK1bEc9w\nqKD5B0f3uwiXDW==\n','cJ1rBeLdBKD2u0DhCtjsyueXrgHPmeq0rMfuAZvPteXOz3zRCZflDMzHAeH3oeftB2XiEK13rvro\nlqO9A2LdcKPwnZzLsJr4k1byl2DQqsTMm3LNve9WvefYndiXCwvcEKPVDffcl2O5uuq1re5etxPn\nrhDRnfrUqMfkzhnsn3LjD0zZrMe0C1fOCvy=\n','2016-07-13 16:17:52','2016-07-13 16:32:28');

COMMIT;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
