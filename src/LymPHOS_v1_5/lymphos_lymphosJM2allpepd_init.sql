SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';
SET AUTOCOMMIT = 0;

USE `lymphos_lymphosJM2allpepd` ;

-- -----------------------------------------------------
-- Data for table `lymphos_expgroup`
-- -----------------------------------------------------
INSERT INTO `lymphos_expgroup` (`id`, `name`, `description`) VALUES (-1, 'No data', '');

COMMIT;

UPDATE `lymphos_expgroup` SET `id`=-1 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_experiments`
-- -----------------------------------------------------


INSERT INTO `lymphos_experiments` (`id`, `expgroup_id`, `dbsub_exps`, `name`, `miape_id`, `description`, `instrument`, `label`, `quant_cond`) VALUES (-1, -1, '', 'No condition', '', '', '', '', '');

COMMIT;


UPDATE `lymphos_experiments` SET `id`=-1 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_quantpepexpcond`
-- -----------------------------------------------------


INSERT INTO `lymphos_quantpepexpcond` (`id`, `unique_pep`, `expgroup_id`, `dballquanttimes`, `dbavgratios`, `dbstdv`, `dbavgregulation`, `dbttestpassed95`, `dbttestpassed99`, `dbsdgreatersdmeanx3`, `dbdanteravgratios`, `dbdanterpvalues`, `dbdanteradjpvalues`) VALUES (-1, 'No quantitative data', -1, '', '', '', '', '', '', '', '', '', '');

COMMIT;


UPDATE `lymphos_quantpepexpcond` SET `id`=-1 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_gelimage`
-- -----------------------------------------------------


INSERT INTO `lymphos_gelimage` (`id`, `twodgel_id`, `name`, `hasjpg`, `dye`, `condition`, `slope_x`, `intercept_x`, `slope_y`, `intercept_y`) VALUES (-1, -1, 'No Image', 0, '', '', 0.0, 0.0, 0.0, 0.0);

COMMIT;


UPDATE `lymphos_gelimage` SET `id`=-1 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_twodgel`
-- -----------------------------------------------------


INSERT INTO `lymphos_twodgel` (`name`, `dbconditions`, `experiment_id`, `id`) VALUES ('NO-GEL', '', -1, -1);

COMMIT;


UPDATE `lymphos_twodgel` SET `id`=-1 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_spot`
-- -----------------------------------------------------


INSERT INTO `lymphos_spot` (`twodgel_id`, `x`, `y`, `area`, `pI`, `MW`, `ratio_ab`, `anova`, `fold`, `max_cv`, `id`, `ingel_id`) VALUES (-1, 0, 0, 0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, -1, 0);

COMMIT;


UPDATE `lymphos_spot` SET `id`=-1 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_users`
-- -----------------------------------------------------


INSERT INTO `lymphos_users` (`id`, `mail`, `passwd`, `permissions`, `singup_date`, `last_logging`) VALUES (1, 'a@a.a', 'a', 1, '2011/12/22', '2011/12/22');

COMMIT;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
