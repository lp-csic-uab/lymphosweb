SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `lymphos_lymphosJM2allpep` ;
CREATE SCHEMA IF NOT EXISTS `lymphos_lymphosJM2allpep` DEFAULT CHARACTER SET latin1 ;
USE `lymphos_lymphosJM2allpep` ;

-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_expgroup`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_expgroup` (
  `id` INT NOT NULL ,
  `name` MEDIUMTEXT NOT NULL ,
  `description` MEDIUMTEXT NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_experiments`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_experiments` (
  `id` INT NOT NULL ,
  `expgroup_id` INT NOT NULL DEFAULT 0 ,
  `dbsub_exps` VARCHAR(50) NOT NULL DEFAULT '' ,
  `name` VARCHAR(100) NOT NULL DEFAULT '' ,
  `miape_id` VARCHAR(50) NOT NULL DEFAULT '' ,
  `description` VARCHAR(180) NOT NULL DEFAULT '' ,
  `instrument` VARCHAR(150) NOT NULL DEFAULT '' ,
  `label` VARCHAR(10) NOT NULL DEFAULT '' ,
  `quant_cond` MEDIUMTEXT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `miapes_id_fk1` (`miape_id` ASC) ,
  INDEX `exp_group_id_fk1` (`expgroup_id` ASC) ,
  CONSTRAINT `expgroup_id_fk1`
    FOREIGN KEY (`expgroup_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_expgroup` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_proteins`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_proteins` (
  `ac` VARCHAR(25) NOT NULL DEFAULT '' ,
  `phospho` TINYINT(1) NOT NULL DEFAULT 1 ,
  `is_decoy` TINYINT(1) NOT NULL DEFAULT 0 ,
  `up_id` VARCHAR(25) NOT NULL DEFAULT '' ,
  `up_ver` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `name` VARCHAR(300) NOT NULL DEFAULT '' ,
  `function` MEDIUMTEXT NOT NULL ,
  `location` VARCHAR(300) NOT NULL DEFAULT '' ,
  `seq` MEDIUMTEXT NOT NULL ,
  `up_psites` MEDIUMTEXT NOT NULL ,
  `score_lcms` FLOAT NOT NULL DEFAULT 0.0 ,
  `chr` varchar(5) NOT NULL DEFAULT '',
  PRIMARY KEY (`ac`) ,
  INDEX `phospho_indx_2` (`phospho` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_quantpepexpcond`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_quantpepexpcond` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `unique_pep` VARCHAR(80) BINARY NOT NULL DEFAULT '' ,
  `expgroup_id` INT NOT NULL DEFAULT 0 ,
  `dballquanttimes` MEDIUMTEXT NOT NULL ,
  `dbavgratios` MEDIUMTEXT NOT NULL ,
  `dbstdv` MEDIUMTEXT NOT NULL ,
  `dbavgregulation` VARCHAR(200) NOT NULL DEFAULT '' ,
  PRIMARY KEY (`id`) ,
  INDEX `expgroup_id_fk2` (`expgroup_id` ASC) ,
  INDEX `quantpepexpcond_uniquepep` (`unique_pep` ASC) ,
  CONSTRAINT `expgroup_id_fk2`
    FOREIGN KEY (`expgroup_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_expgroup` (`id` ))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_nophospho_stats`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_nophospho_stats` (
  `quanttime` SMALLINT NOT NULL ,
  `mu` FLOAT NOT NULL DEFAULT 0.0 ,
  `sigma` FLOAT NOT NULL DEFAULT 0.0 ,
  `n_spectra` INT UNSIGNED NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`quanttime`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_gelimage`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_gelimage` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `twodgel_id` INT NOT NULL DEFAULT 0 ,
  `name` VARCHAR(100) NOT NULL DEFAULT '' ,
  `dye` VARCHAR(10) NOT NULL DEFAULT '' ,
  `condition` VARCHAR(50) NOT NULL DEFAULT '' ,
  `hasjpg` TINYINT(1) NOT NULL DEFAULT 0 ,
  `slope_x` FLOAT NOT NULL DEFAULT 0.0 ,
  `intercept_x` FLOAT NOT NULL DEFAULT 0.0 ,
  `slope_y` FLOAT NOT NULL DEFAULT 0.0 ,
  `intercept_y` FLOAT NOT NULL DEFAULT 0.0 ,
  PRIMARY KEY (`id`) ,
  INDEX `twodgel_id_fk2` (`twodgel_id` ASC) ,
  CONSTRAINT `twodgel_id_fk2`
    FOREIGN KEY (`twodgel_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_twodgel` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_twodgel`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_twodgel` (
  `name` VARCHAR(15) NOT NULL DEFAULT '' ,
  `dbconditions` MEDIUMTEXT NOT NULL ,
  `experiment_id` INT NOT NULL DEFAULT 0 ,
  `gelimage_id` INT NOT NULL DEFAULT 0 ,
  `id` INT NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) ,
  INDEX `experiment_id_fk2` (`experiment_id` ASC) ,
  INDEX `gelimage_id_fk1` (`gelimage_id` ASC) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) ,
  CONSTRAINT `experiment_id_fk2`
    FOREIGN KEY (`experiment_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_experiments` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `image_id_fk1`
    FOREIGN KEY (`gelimage_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_gelimage` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_spot`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_spot` (
  `twodgel_id` INT NOT NULL DEFAULT 0 ,
  `x` INT NOT NULL DEFAULT 0 ,
  `y` INT NOT NULL DEFAULT 0 ,
  `area` INT NOT NULL DEFAULT 0 ,
  `pI` FLOAT NOT NULL DEFAULT 0.0 ,
  `MW` FLOAT NOT NULL DEFAULT 0.0 ,
  `ratio_ab` FLOAT NOT NULL DEFAULT -1.0 ,
  `anova` FLOAT NOT NULL DEFAULT 0.0 ,
  `fold` FLOAT NOT NULL DEFAULT 0.0 ,
  `max_cv` FLOAT NOT NULL DEFAULT 0.0 ,
  `id` INT NOT NULL AUTO_INCREMENT ,
  `ingel_id` INT NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  INDEX `twodgel_id_fk1` (`twodgel_id` ASC) ,
  CONSTRAINT `twodgel_id_fk1`
    FOREIGN KEY (`twodgel_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_twodgel` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_spots2proteins`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_spots2proteins` (
  `spot_id` INT NOT NULL ,
  `protein_id` VARCHAR(25) NOT NULL ,
  `score_gelpmf` FLOAT NOT NULL DEFAULT 0.0 ,
  `id` INT NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `spot_protein` (`spot_id` ASC, `protein_id` ASC) ,
  INDEX `spot_fk3` (`spot_id` ASC) ,
  INDEX `protein_fk2` (`protein_id` ASC) ,
  CONSTRAINT `spot_fk3`
    FOREIGN KEY (`spot_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_spot` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `protein_fk2`
    FOREIGN KEY (`protein_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_proteins` (`ac` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_spotextra`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_spotextra` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `gelimage_id` INT NOT NULL DEFAULT 0 ,
  `spot_id` INT NOT NULL DEFAULT 0 ,
  `volume` FLOAT NOT NULL DEFAULT 0.0 ,
  `norm_volume` FLOAT NOT NULL DEFAULT 0.0 ,
  `background` FLOAT NOT NULL DEFAULT 0.0 ,
  `peak_height` FLOAT NOT NULL DEFAULT 0.0 ,
  PRIMARY KEY (`id`) ,
  INDEX `gelimage_id_fk2` (`gelimage_id` ASC) ,
  INDEX `spot_id_fk2` (`spot_id` ASC) ,
  CONSTRAINT `gelimage_id_fk2`
    FOREIGN KEY (`gelimage_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_gelimage` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `spot_id_fk2`
    FOREIGN KEY (`spot_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_spot` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_data`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_data` (
  `experiment_id` INT NOT NULL ,
  `id` INT NOT NULL AUTO_INCREMENT ,
  `phospho` TINYINT(1) NOT NULL DEFAULT 1 ,
  `pep_orig` VARCHAR(200) NOT NULL ,
  `peptide` VARCHAR(80) BINARY NOT NULL ,
  `dbpsites` VARCHAR(20) NOT NULL ,
  `dbascores` VARCHAR(85) NOT NULL ,
  `pep_score` FLOAT NOT NULL ,
  `raw_file` VARCHAR(200) NOT NULL ,
  `out_file` VARCHAR(100) NOT NULL ,
  `scan_i` INT UNSIGNED NOT NULL ,
  `scan_f` INT UNSIGNED NOT NULL ,
  `n_prots` SMALLINT UNSIGNED NOT NULL ,
  `ms_n` TINYINT UNSIGNED NOT NULL ,
  `rt` INT UNSIGNED NOT NULL ,
  `spot_id` INT NOT NULL DEFAULT 0 ,
  `mass` FLOAT NOT NULL ,
  `deltamass` FLOAT NOT NULL ,
  `ions` VARCHAR(10) NOT NULL ,
  `charge` TINYINT NOT NULL ,
  `xcorr` FLOAT NOT NULL ,
  `deltacn` FLOAT NOT NULL ,
  `d` FLOAT NOT NULL ,
  `omssa` FLOAT NOT NULL ,
  `phenyx` FLOAT NOT NULL ,
  `peaks` FLOAT NOT NULL ,
  `easyprot` FLOAT NOT NULL ,
  `qdata` MEDIUMTEXT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `experiment_id_fk1` (`experiment_id` ASC) ,
  INDEX `spot_id_fk1` (`spot_id` ASC) ,
  INDEX `phospho_indx` (`phospho` ASC) ,
  CONSTRAINT `experiment_id_fk1`
    FOREIGN KEY (`experiment_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_experiments` (`id` ),
  CONSTRAINT `spot_id_fk1`
    FOREIGN KEY (`spot_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_spot` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_data_proteins`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_data_proteins` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `data_id` INT NOT NULL ,
  `proteins_id` VARCHAR(25) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `data_id_protein_id` (`data_id` ASC, `proteins_id` ASC) ,
  INDEX `data_id_fk2` (`data_id` ASC) ,
  INDEX `proteins_id_fk1` (`proteins_id` ASC) ,
  CONSTRAINT `proteins_id_fk1`
    FOREIGN KEY (`proteins_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_proteins` (`ac` ),
  CONSTRAINT `data_id_fk2`
    FOREIGN KEY (`data_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_data` (`id` ))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_spectra`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_spectra` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `data_id` INT NOT NULL ,
  `ms_n` TINYINT UNSIGNED NOT NULL ,
  `mass` MEDIUMTEXT NOT NULL ,
  `intensity` MEDIUMTEXT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `data_id_fk1` (`data_id` ASC) ,
  CONSTRAINT `data_id_fk1`
    FOREIGN KEY (`data_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_data` (`id` ))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_dataquant`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_dataquant` (
  `data_id` INT NOT NULL ,
  `id` INT NOT NULL AUTO_INCREMENT ,
  `dbquanttimes` VARCHAR(100) NOT NULL DEFAULT '' ,
  `dbratios` VARCHAR(100) NOT NULL DEFAULT '' ,
  `dbstdv` VARCHAR(100) NOT NULL DEFAULT '' ,
  `dbregulation` VARCHAR(100) NOT NULL DEFAULT '' ,
  `quantpepexpcond_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `data_id_fk3` (`data_id` ASC) ,
  INDEX `quantpepexpcond_id_fk1` (`quantpepexpcond_id` ASC) ,
  UNIQUE INDEX `data_id_UNIQUE` (`data_id` ASC) ,
  CONSTRAINT `data_id_fk3`
    FOREIGN KEY (`data_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_data` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `quantpepexpcond_id_fk1`
    FOREIGN KEY (`quantpepexpcond_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_quantpepexpcond` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_peptidecache`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_peptidecache` (
  `peptide` VARCHAR(80) BINARY NOT NULL ,
  `phospho` TINYINT(1) NOT NULL DEFAULT 1 ,
  `dbpsites` VARCHAR(20) NOT NULL ,
  `dbascores` VARCHAR(85) NOT NULL ,
  `n_prots` SMALLINT UNSIGNED NOT NULL ,
  `dbproteins` MEDIUMTEXT NOT NULL ,
  `quantitative` TINYINT(1) NOT NULL DEFAULT 0 ,
  `n_scans` SMALLINT UNSIGNED NOT NULL ,
  `gel` TINYINT(1) NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`peptide`) ,
  INDEX `phospho_indx_3` (`phospho` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_miapes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_miapes` (
  `id` VARCHAR(3) NOT NULL ,
  `xml` LONGTEXT NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_experiments_miapes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_experiments_miapes` (
  `experiments_id` INT NOT NULL ,
  `id` INT NOT NULL AUTO_INCREMENT ,
  `miapes_id` VARCHAR(3) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `experiments_id_fk3` (`experiments_id` ASC) ,
  INDEX `miapes_id_fk1` (`miapes_id` ASC) ,
  CONSTRAINT `experiments_id_fk3`
    FOREIGN KEY (`experiments_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_experiments` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `miapes_id_fk1`
    FOREIGN KEY (`miapes_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphos_miapes` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_users` (
  `mail` VARCHAR(240) NOT NULL ,
  `passwd` VARCHAR(24) NOT NULL ,
  `permissions` TINYINT UNSIGNED NOT NULL ,
  `singup_date` DATETIME NOT NULL ,
  `last_logging` DATETIME NOT NULL ,
  PRIMARY KEY (`mail`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_eventtype`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphost_eventtype` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(15) NOT NULL DEFAULT '' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphost_events`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphost_events` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `eventtype_id` INT UNSIGNED NOT NULL ,
  `date` DATETIME NOT NULL ,
  `value` TEXT NOT NULL ,
  `subevents` INT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  INDEX `eventtype_id_fk1` (`eventtype_id` ASC) ,
  CONSTRAINT `eventtype_id_fk1`
    FOREIGN KEY (`eventtype_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphost_eventtype` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lymphos_lymphosJM2allpep`.`lymphos_events_subevents`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lymphos_lymphosJM2allpep`.`lymphos_events_subevents` (
  `from_events_id` INT UNSIGNED NOT NULL ,
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `to_events_id` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `from_events_id_to_events_id` (`from_events_id` ASC, `to_events_id` ASC) ,
  INDEX `to_events_id_fk1` (`to_events_id` ASC) ,
  INDEX `from_events_id_fk1` (`from_events_id` ASC) ,
  CONSTRAINT `to_events_id_fk1`
    FOREIGN KEY (`to_events_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphost_events` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `from_events_id_fk1`
    FOREIGN KEY (`from_events_id` )
    REFERENCES `lymphos_lymphosJM2allpep`.`lymphost_events` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;




-- -----------------------------------------------------
-- Data for table `lymphos_lymphosJM2allpep`.`lymphos_expgroup`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
USE `lymphos_lymphosJM2allpep`;
INSERT INTO `lymphos_lymphosJM2allpep`.`lymphos_expgroup` (`id`, `name`, `description`) VALUES (0, 'No data', '');

COMMIT;

USE `lymphos_lymphosJM2allpep`;
UPDATE `lymphos_lymphosJM2allpep`.`lymphos_expgroup` SET `id`=0 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_lymphosJM2allpep`.`lymphos_experiments`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
USE `lymphos_lymphosJM2allpep`;
INSERT INTO `lymphos_lymphosJM2allpep`.`lymphos_experiments` (`id`, `expgroup_id`, `dbsub_exps`, `name`, `miape_id`, `description`, `instrument`, `label`, `quant_cond`) VALUES (0, 0, '', 'No condition', '', '', '', '', '');

COMMIT;

USE `lymphos_lymphosJM2allpep`;
UPDATE `lymphos_lymphosJM2allpep`.`lymphos_experiments` SET `id`=0 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_lymphosJM2allpep`.`lymphos_quantpepexpcond`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
USE `lymphos_lymphosJM2allpep`;
INSERT INTO `lymphos_lymphosJM2allpep`.`lymphos_quantpepexpcond` (`id`, `unique_pep`, `expgroup_id`, `dballquanttimes`, `dbavgratios`, `dbstdv`, `dbavgregulation`) VALUES (0, 'No cuantitative data', 0, '', '', '', '');

COMMIT;

USE `lymphos_lymphosJM2allpep`;
UPDATE `lymphos_lymphosJM2allpep`.`lymphos_quantpepexpcond` SET `id`=0 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_lymphosJM2allpep`.`lymphos_gelimage`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
USE `lymphos_lymphosJM2allpep`;
INSERT INTO `lymphos_lymphosJM2allpep`.`lymphos_gelimage` (`id`, `twodgel_id`, `name`, `dye`, `condition`, `slope_x`, `intercept_x`, `slope_y`, `intercept_y`) VALUES (0, 0, 'No Image', '', '', 0.0, 0.0, 0.0, 0.0);

COMMIT;

USE `lymphos_lymphosJM2allpep`;
UPDATE `lymphos_lymphosJM2allpep`.`lymphos_gelimage` SET `id`=0 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_lymphosJM2allpep`.`lymphos_twodgel`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
USE `lymphos_lymphosJM2allpep`;
INSERT INTO `lymphos_lymphosJM2allpep`.`lymphos_twodgel` (`name`, `dbconditions`, `experiment_id`, `gelimage_id`, `id`) VALUES ('NO-GEL', '', 0, 0, 0);

COMMIT;

USE `lymphos_lymphosJM2allpep`;
UPDATE `lymphos_lymphosJM2allpep`.`lymphos_twodgel` SET `id`=0 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_lymphosJM2allpep`.`lymphos_spot`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
USE `lymphos_lymphosJM2allpep`;
INSERT INTO `lymphos_lymphosJM2allpep`.`lymphos_spot` (`twodgel_id`, `x`, `y`, `area`, `pI`, `MW`, `ratio_ab`, `anova`, `fold`, `max_cv`, `id`, `ingel_id`) VALUES (0, 0, 0, 0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0, 0);

COMMIT;

USE `lymphos_lymphosJM2allpep`;
UPDATE `lymphos_lymphosJM2allpep`.`lymphos_spot` SET `id`=0 WHERE `id`='1';

-- -----------------------------------------------------
-- Data for table `lymphos_lymphosJM2allpep`.`lymphos_users`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
USE `lymphos_lymphosJM2allpep`;
INSERT INTO `lymphos_lymphosJM2allpep`.`lymphos_users` (`mail`, `passwd`, `permissions`, `singup_date`, `last_logging`) VALUES ('a@a.a', 'a', 1, '2011/12/22', '2011/12/22');

COMMIT;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
