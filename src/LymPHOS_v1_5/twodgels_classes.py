# -*- coding: utf-8 -*-
"""
:synopsis:   2D Gels classes for LymPHOS_v1_5 project views

:created:    2011-01-14

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    1.5 dev
:updated:    2014-05-16
"""

#===============================================================================
# Imports
#===============================================================================
from django.http import HttpResponseRedirect
from django.conf import settings

try:
    import Image #The deployment server uses PIL, so try this first
except ImportError:
    from PIL import Image #If PIL (phyton-imaging) is not pressent try importing from PILLOW

import os
from collections import defaultdict

#Application imports:
from basic_classes import SearchResultPage, DinamicFile
from lymphos.models import TwoDGel


#===============================================================================
# 2D Gels View classes definitions
#===============================================================================
class TwoDViewPage(SearchResultPage):
    
    result_template = 'twod_view.html'
    paginated = True
    search_default = ''
    twod_args = None
    
    def __init__(self, *args, **kwargs):
        '''
        Constructor
        '''
        
        #Pseudo_Constants:
        self._ARGS = ('pep', 'ac', 'map', 'spot', 'spectra', 'format', 
                      'extract', 'data', 'zoom')
        self._ARG2GEL = {'pep': 'spot__data__peptide',
                         'ac': ('spot__data__proteins__ac',   #OR query
                                'spot__proteins__ac'),        #
                         'map': 'name',
                         'spot': 'spot__id',
                         'spectra': 'spot__data__spectra__id',
                         }
        self._ARG2SPOT = {'pep': 'data__peptide',
                          'ac': ('data__proteins__ac',          #OR query
                                 'proteins__ac',),              #
                          'spot': 'id',
                          'spectra': 'data__spectra__id',
                          }
        
        #__init__ from Superclass:
        super(TwoDViewPage, self).__init__(*args, **kwargs)
        
        self._search_validator = r"""[\w\&\=]"""
        self.itemsxpage = 2
        self.zoom = None
        
        if self.twod_args: self.s_search = self.twod_args
    
    def set_s_search(self, rs_search):
        self._o_s_search = rs_search
        errors = self._sanitize_check(self._o_s_search, '2D Gels Search Error')
        if not errors:
            self._s_search, errors = self._args2dict(rs_search)
            if not errors:
                self.search()
                if self.results: self.context_format()
    s_search = property(SearchResultPage.get_s_search, set_s_search)
    
    def _args2dict(self, args):
        '''
        From an argument string (args), of the form "arg1=val1&arg2=val2",
        generate an argument dictionary (args_d), where: key = argument_name ,
        value = argument_value
        Return the argument dictionary and the parsing errors found.
        '''
        args_d = dict()
        errors = False
        if args:
            args = args.split('&')
            for arg in args:
                arg_tkns = arg.split('=')
                arg_name = arg_tkns[0]
                if arg_name in self._ARGS:
                    if len(arg_tkns) == 2:
                        args_d[arg_name] = arg_tkns[1]
                    elif len(arg_tkns) == 1:
                        args_d[arg_name] =  None
                    else:
                        self._put_error("2D Gels Search Error", 
                                        "' {0} ' is not a valid value for "\
                                        "' {1} '.".format(''.join(arg_tkns[1:]), 
                                                          arg_name))
                        errors = True
                else:
                    self._put_error("2D Gels Search Error", 
                                    "' {0} ' is not a valid parameter/argument "\
                                    "name.".format(arg_name))
                    errors = True
        return args_d, errors
    
    def _argsd2queryd(self, args2query):
        queryd = dict()
        for arg_name, qry_filterk in args2query.iteritems():
            arg_value = self._s_search.get(arg_name, None)
            if arg_value: #arg_value is None if arg_name is not in args2query or if a value was not passed in the URL. In those cases we don't use it for queries
                queryd[qry_filterk] = arg_value
        return queryd
    
    def search(self, rs_search=None):
        '''
        Search for 2D Gels that matches the arguments assigned to 
        self._s_search through self.s_search
        '''
        if rs_search:                   #Set the string search of the object
            self.s_search = rs_search
        else:                                               
            #Create an argument(arg_name)->field-filter(qry_filterk)->arg_value dictionary
            qry_filter_d = self._argsd2queryd(self._ARG2GEL)
            #Do the searches
            self._searches.st_twodgel(qry_filter_d, 'qtwodview')
            qtwodview = self._searches.get_query('qtwodview')
            if qtwodview:
                self.results = qtwodview
                super(TwoDViewPage, self).search() #Basic pagination stuff
            else:
                self.results = None
                self._put_error("2D Gels Search Error", 
                                "No results found for 2D Gels matching these "\
                                "arguments: {0}".format(self._o_s_search))
        return self.results
    
    def get_context_data(self, **kwargs):
        """
        """
        # Call the base implementation first to get a context:
        context = super(TwoDViewPage, self).get_context_data(**kwargs)
        # Return if there is no search result data to add:
        if not self.results:
            return context
        
        # Prepare spot filter to get spots to be marked in view:
        spot_filter_d = self._argsd2queryd(self._ARG2SPOT)
        
        # Process the zoom parameter, if present:
        zoom_value = self._s_search.get('zoom', None)
        if zoom_value:
            self.zoom = float(zoom_value)
        else:
            self.zoom = None
        
        for gel in self.results.object_list:
            #Set the images of the gel to load&show when page loads (gel.showimgs):
            gel.showimgs = list()
            conditions = gel.conditions
            gel.n_showimgs = gel.nconditions
#            gel.showimgs.append(gel.gelimage)
#            conditions.remove(gel.gelimage.condition)
            for condition in conditions:
                selected = gel.rel_gelimages.filter(condition=condition, 
                                                    hasjpg=True)[0]
                gel.showimgs.append(selected)
            #Adjust gel-images to desired zoom level:
            full_imgname = os.path.join(settings.STATIC_DOC_ROOT, 'images', 
                                         'gels', selected.file_name())
            gelimage = Image.open(full_imgname)
            width, _ = gelimage.size
            if not self.zoom:
                #Adjust initial gel-images scale to a maximum width of 385px
#                scaling = 1             #TEST
#                gel.overflow = 'scroll' #
                scaling = 385.0 / width
                gel.overflow = 'hidden'
                gel.zoom = scaling * 100.0
            else:
                #Adjust initial gel-images scale to self.zoom and decide if the
                #image-div viewport needs scroll-bars:
                scaling = self.zoom / 100.0
                gel.zoom = self.zoom
                if width * scaling > 385.0:
                    gel.overflow = 'scroll'
                else:
                    gel.overflow = 'hidden'
            #Get IDs of the spots to be marked in this gel:
            if spot_filter_d:
                spot_query = self._searches.st_complex_common(gel.spot_set, 
                                                              f_dfilter=
                                                              spot_filter_d)
                gel.spots_mrkd = set(spot.id for spot in spot_query)
            else:
                gel.spots_mrkd = set()
            #Get peptide, spectra and protein processed non-redundant
            #information for all spots in this gel, and associate this with 
            #the spot. That is because objects in a query from backward 
            #relationship (through *_set: spot_set) not conserving the related 
            #objects state if altered
            gel.spots = gel.spot_set.all() 
            for spot in gel.spots:
                #Adapt spot coordinates to gel-images scale and spot size (11x11 px):
                spot.x = int(round(spot.x * scaling)) - 5
                spot.y = int(round(spot.y * scaling)) - 5
                #If available, get non-redundant peptide name and all associated spectra IDs:
                pep_data = defaultdict(list)
                for data in spot.data_set.all():
                    data_spectra = [spec[0] for spec in
                                    data.spectra_set.values_list('id')]
                    pep_data[data.peptide].extend(data_spectra)         
                #Get peptide data from peptidecache table (non-redundant peptides):
                self._searches.st_peptidecache(peptide__in = pep_data.keys())  
                spot.peptides = self._searches.get_query() #Associate peptides found  #Alternative, if models were imported: spot.peptides = PeptideCache.objects.filter(peptide__in = pep_names)
                #Get proteins from every peptide identified by LC-MS in the spot:
                lcms_prot_acs = set()
                for pep in spot.peptides:
                    lcms_prot_acs.update(pep.l_prots)
                    pep.spectra = pep_data[pep.peptide] #Moreover, associate spectra IDs found in the spot with the corresponding peptide
                spot.lcms_prots = sorted(lcms_prot_acs) #Associate non-redundant proteins found
        
        # Update the context with search results:
        context['viewer_title'] = 'Test Title'
        context['twodgels'] = self.results
        context['n_twodgels'] = self.results.paginator.count
        return context


class TwoDDataPage(TwoDViewPage): #TASK: Implement
    
    def __init__(self, template='', extra_context=None, result_template='', 
                 irs_search=None, paginated=False, twod_args=None):
        '''
        Constructor
        '''
        
        #Pseudo_Constants:
        self._ARG2TMPLT = {'pep': '',
                           'ac': '',
                           'spot': '',
                           'spectra': '',
                           'format': '',
                           }
        
        #__init__ from Superclass:
        super(TwoDDataPage, self).__init__(self, template, extra_context, 
                                  result_template, irs_search, paginated,
                                  twod_args)
    
    def context_format(self, results=None):
        '''
        '''
        if results:
            self.results = results
        
        return


class TwoDGelImage(DinamicFile):
    
    def __init__(self, *args, **kwargs):
        super(TwoDGelImage, self).__init__(*args, **kwargs)
        #
        self.zoom = 100.0
    
#    def _set_filename(self, filename):
#        super(TwoDGelImage, self)._set_filename(filename)
#        if self._name:
#            self._name, _, zoom = self._name.partition('-')
#            if zoom: self.zoom = float(zoom)
#    filename = property(DinamicFile._get_filename, _set_filename)
    
    def get(self, request, *args, **kwargs):
        super(TwoDGelImage, self).get(request, *args, **kwargs)
        
        # Get the requested zoom level as a %:
        self.zoom = float( self.request.GET.get('zoom', self.zoom) )
        
        if self.zoom != 100.0:
            # Load the original image:
            full_filename = os.path.join(settings.STATIC_DOC_ROOT, 'images', 
                                         'gels', self.filename)
            gelimage = Image.open(full_filename)
            # Rescale image:
            width, height = gelimage.size
            scale = self.zoom / 100.0
            new_size = ( int(round(width*scale)), int(round(height*scale)) )
            gelimage = gelimage.resize(new_size, Image.ANTIALIAS)
            # Serve rescaled image:
            self.response['Content-Type'] = 'image/jpeg'
            gelimage.save(self.response, "JPEG")
            self.zoom = 100.0 #X-NOTE: Reset zoom to avoid render a new image with an old zoom level from the previous one
            return self.response
        else:
            # Let the the httpd for static content serve the original image:
            return HttpResponseRedirect('/files/images/gels/{0}'.format(self.filename))

