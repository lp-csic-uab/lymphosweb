
---

**WARNING!**: This is the *Old* source-code repository for the web-app at [www.LymPHOS.org](https://www.LymPHOS.org).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/lymphosweb/) located at https://sourceforge.net/p/lp-csic-uab/lymphosweb/**  

---
  
  
# LymPHOS2

[**LymPHOS2 Web-App**](https://www.LymPHOS.org) from [LP CSIC/UAB](http://proteomica.uab.cat) is a web-based DataBase Application containing peptidic and protein sequences and spectrometric information on the PhosphoProteome of human T-Lymphocytes.  
  
  
---

**WARNING!**: This is the *Old* source-code repository for the web-app at [www.LymPHOS.org](https://www.LymPHOS.org).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/lymphosweb/) located at https://sourceforge.net/p/lp-csic-uab/lymphosweb/**  

---
  
